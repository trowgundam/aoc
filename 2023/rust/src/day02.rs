fn main() {
    let lines = include_str!("../../files/day02.txt")
        .lines()
        .map(|line| line.to_owned())
        .collect::<Vec<_>>();

    println!("Part1: {}", part1(&lines));
    println!("Part2: {}", part2(&lines));
}

#[derive(Debug)]
enum Color {
    Red,
    Green,
    Blue,
}

impl Color {
    fn get_color(line: &str) -> Color {
        if line.ends_with("red") {
            Color::Red
        } else if line.ends_with("green") {
            Color::Green
        } else if line.ends_with("blue") {
            Color::Blue
        } else {
            Color::Red
        }
    }
}

#[derive(Default, Debug)]
struct GameResult {
    pub red: i32,
    pub green: i32,
    pub blue: i32,
}
#[derive(Default, Debug)]
struct Game {
    pub id: i32,
    pub results: Vec<GameResult>,
}

const RED_MAX: i32 = 12;
const GREEN_MAX: i32 = 13;
const BLUE_MAX: i32 = 14;

impl Game {
    pub fn get_peaks(&self) -> GameResult {
        self.results
            .iter()
            .fold(GameResult::default(), |init, result| GameResult {
                red: init.red.max(result.red),
                green: init.green.max(result.green),
                blue: init.blue.max(result.blue),
            })
    }

    pub fn is_good(&self) -> bool {
        let peaks = self.get_peaks();
        peaks.red <= RED_MAX && peaks.green <= GREEN_MAX && peaks.blue <= BLUE_MAX
    }

    pub fn get_power(&self) -> i32 {
        let peaks = self.get_peaks();
        peaks.red * peaks.green * peaks.blue
    }
}

fn part1(input: &[String]) -> i32 {
    get_games_results(input)
        .into_iter()
        .filter(|game| game.is_good())
        .fold(0, |acc, g| acc + g.id)
}

fn part2(input: &[String]) -> i32 {
    get_games_results(input)
        .into_iter()
        .map(|g| g.get_power())
        .sum()
}

fn get_games_results(input: &[String]) -> Vec<Game> {
    input
        .iter()
        .map(|line| {
            let colon_pos = line.find(':').unwrap_or(5);
            let game_id = substr(line, 5, colon_pos - 5).parse::<i32>().unwrap_or(0);
            let results_str = substr(line, colon_pos + 2, line.len() - colon_pos - 2);
            let results = get_game_results(&results_str);

            Game {
                id: game_id,
                results,
            }
        })
        .collect::<Vec<_>>()
}

fn substr(str: &str, position: usize, length: usize) -> String {
    str.chars().skip(position).take(length).collect::<String>()
}

fn get_game_results(results: &str) -> Vec<GameResult> {
    results.split(';').map(get_game_result).collect::<Vec<_>>()
}

fn get_game_result(result: &str) -> GameResult {
    result
        .split(',')
        .map(|result| {
            let result = result.trim();
            let color = Color::get_color(result);
            let clen = match color {
                Color::Red => 4,
                Color::Green => 6,
                Color::Blue => 5,
            };
            (
                color,
                substr(result, 0, result.len() - clen)
                    .parse::<i32>()
                    .unwrap_or(0),
            )
        })
        .fold(GameResult::default(), |t, (c, v)| match c {
            Color::Red => GameResult {
                red: t.red + v,
                ..t
            },
            Color::Green => GameResult {
                green: t.green + v,
                ..t
            },
            Color::Blue => GameResult {
                blue: t.blue + v,
                ..t
            },
        })
}
