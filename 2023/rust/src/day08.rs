use std::{collections::HashMap, vec};

use itertools::Itertools;

fn main() {
    let sample1 = Input::new(include_str!("../../files/day08-test.txt"));
    let sample2 = Input::new(include_str!("../../files/day08-test2.txt"));
    let actual = Input::new(include_str!("../../files/day08.txt"));

    println!("Sample:");
    println!("Part1: {}", part1(&sample1));
    println!("Part1: {}", part2(&sample2));
    println!();
    println!("Actua:");
    println!("Part1: {}", part1(&actual));
    println!("Part2: {}", part2(&actual));
}

fn part1(input: &Input) -> usize {
    input.walk("AAA", |n| n == "ZZZ")
}

fn part2(input: &Input) -> usize {
    let mut input = input
        .map
        .keys()
        .filter(|node| node.ends_with('A'))
        .map(|start| input.walk(start, |n| n.ends_with('Z')))
        .flat_map(|length| {
            let mut f = prime_factors(length);
            f.sort();
            f.iter()
                .chunk_by(|f| *f)
                .into_iter()
                .map(|g| {
                    let base = *g.0;
                    let exponent = g.1.count();
                    (base, exponent)
                })
                .collect_vec()
        })
        .collect_vec();
    input.sort_by_key(|(base, _)| *base);
    input
        .iter()
        .chunk_by(|(b, _)| b)
        .into_iter()
        .map(|g| {
            let base = g.0;
            let exponent = g.1.map(|(_, e)| *e).max().unwrap();
            base.pow(u32::try_from(exponent).unwrap())
        })
        .product::<usize>()
}

fn prime_factors(n: usize) -> Vec<usize> {
    if n < 2 {
        return vec![];
    }

    let divisor = find_divisor(n, 2);
    if divisor == n {
        return vec![n];
    }

    let mut factors = vec![divisor];
    factors.extend(prime_factors(n / divisor));
    factors
}

fn find_divisor(n: usize, candidate: usize) -> usize {
    if (candidate * candidate) > n {
        return n;
    }

    if n % candidate == 0 {
        return candidate;
    }

    find_divisor(n, candidate + 1)
}

struct Input {
    pub directions: Vec<usize>,
    pub map: HashMap<String, Vec<String>>,
}

impl Input {
    pub fn new(input: &str) -> Input {
        let directions = input
            .lines()
            .take(1)
            .flat_map(|s| {
                s.chars().map(|c| match c {
                    'L' => 0usize,
                    'R' => 1usize,
                    c => panic!("Invalid Direction: {c}"),
                })
            })
            .collect::<Vec<_>>();

        let map = input
            .lines()
            .skip(2)
            .map(|l| {
                (
                    l[0..3].to_owned(),
                    vec![l[7..10].to_owned(), l[12..15].to_owned()],
                )
            })
            .collect::<HashMap<_, _>>();

        Input { directions, map }
    }

    pub fn walk<F>(&self, start: &str, is_end: F) -> usize
    where
        F: Fn(&str) -> bool,
    {
        let mut depth = 0usize;
        let mut node = start;
        while !is_end(node) {
            let dir = self.directions[depth % self.directions.len()];
            node = &self.map[node][dir];
            depth += 1;
        }

        depth
    }
}
