use std::{collections::HashMap, time::SystemTime};

fn main() {
    let lines = include_str!("../../files/day03.txt")
        .lines()
        .map(Card::new)
        .collect::<Vec<_>>();

    println!("Part1: {}", part1(&lines));
    let start = SystemTime::now();
    println!("Part2: {}", part2(&lines));
    let mid = SystemTime::now();
    println!("Part2: {}", part2_alt(&lines));
    let end = SystemTime::now();

    let norm_dur = mid.duration_since(start).unwrap().as_secs_f64();
    let alt_dur = end.duration_since(mid).unwrap().as_secs_f64();
    println!("Normal:   {}", norm_dur);
    println!("Alt:      {}", alt_dur);
    println!("Imporved: {}", (norm_dur / alt_dur) * 100f64);
}

fn part1(cards: &[Card]) -> u32 {
    cards.iter().map(|c| c.get_score()).sum()
}

fn part2(cards: &[Card]) -> usize {
    cards
        .iter()
        .map(|c| {
            let winning_count = u32::try_from(c.get_winning_numbers().len()).unwrap();
            let new_ids = (c.id + 1..c.id + winning_count + 1).collect::<Vec<_>>();
            1 + get_cards(cards, new_ids).len()
        })
        .sum()
}

fn part2_alt(cards: &[Card]) -> usize {
    let mut card_prizes: HashMap<u32, Vec<u32>> = HashMap::new();
    for card in cards.iter().rev() {
        let winning_count = u32::try_from(card.get_winning_numbers().len()).unwrap();
        let mut prizes = (card.id + 1..card.id + winning_count + 1).collect::<Vec<_>>();
        let prizes_prizes = prizes
            .iter()
            .flat_map(|i| card_prizes.get(i).unwrap())
            .collect::<Vec<_>>();
        prizes.extend(prizes_prizes);
        card_prizes.insert(card.id, prizes);
    }
    card_prizes.values().map(|p| p.len()).sum::<usize>() + card_prizes.len()
}

fn get_cards(cards: &[Card], ids: Vec<u32>) -> Vec<u32> {
    ids.iter()
        .map(|i| {
            *cards
                .iter()
                .filter(|c| c.id.eq(i))
                .collect::<Vec<_>>()
                .first()
                .unwrap()
        })
        .map(|c| {
            let winning_count = u32::try_from(c.get_winning_numbers().len()).unwrap();
            let new_ids = (c.id + 1..c.id + winning_count + 1).collect::<Vec<_>>();
            let mut results = vec![c.id];
            results.extend(get_cards(cards, new_ids));
            results
        })
        .fold(vec![], |acc, i| {
            let mut result = vec![];
            result.extend(acc);
            result.extend(i);
            result
        })
}

struct Card {
    pub id: u32,
    pub winning: Vec<u8>,
    pub haves: Vec<u8>,
}
impl Card {
    pub fn new(line: &str) -> Card {
        let items = line.split(':').map(str::trim).collect::<Vec<_>>();

        if items.len() < 2 {
            panic!("Invalid Input: {line}");
        }

        let header = items[0];
        let data = items[1];

        let id = header[5..].trim().parse::<u32>().unwrap();
        let data = data
            .split('|')
            .map(str::trim)
            .map(|d| {
                let cleaned = d.replace("  ", " ");
                cleaned
                    .split(' ')
                    .map(str::trim)
                    .map(|d| d.parse::<u8>().unwrap())
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        if data.len() < 2 {
            panic!("Invalid Input: {line}");
        }

        Card {
            id,
            winning: data.first().unwrap().to_owned(),
            haves: data.last().unwrap().to_owned(),
        }
    }

    pub fn get_winning_numbers(&self) -> Vec<u8> {
        self.haves
            .clone()
            .into_iter()
            .filter(|v| self.winning.contains(v))
            .collect()
    }

    pub fn get_score(&self) -> u32 {
        match self.get_winning_numbers().len() {
            0 => 0,
            c => 2u32.pow(u32::try_from(c).unwrap_or(1) - 1),
        }
    }
}
