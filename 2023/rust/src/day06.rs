fn main() {
    let input = include_str!("../../files/day06-test.txt");
    println!("Sample");
    println!("Part1: {}", part1(input));
    println!("Part2: {}", part2(input));

    let input = include_str!("../../files/day06.txt");
    println!("Actual");
    println!("Part1: {}", part1(input));
    println!("Part2: {}", part2(input));
}

fn part1(input: &str) -> usize {
    let input = input.lines().take(2).map(process_line).collect::<Vec<_>>();
    let data = input[0].iter().zip(input[1].iter()).collect::<Vec<_>>();
    data.into_iter().map(process_race).product::<usize>()
}

fn part2(input: &str) -> usize {
    let input = input
        .lines()
        .take(2)
        .map(|s| s.replace(' ', ""))
        .flat_map(|s| {
            s.split(':')
                .skip(1)
                .map(|i| i.parse::<usize>().unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();
    process_race((&input[0], &input[1]))
}

fn process_line(line: &str) -> Vec<usize> {
    line.split_ascii_whitespace()
        .filter(|i| !i.is_empty())
        .skip(1)
        .map(|i| i.parse::<usize>().unwrap())
        .collect()
}

fn process_race((time, distance): (&usize, &usize)) -> usize {
    (0..time + 1)
        .filter(|v| {
            let v = v * (time - v);
            distance.lt(&v)
        })
        .count()
}
