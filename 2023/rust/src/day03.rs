fn main() {
    let lines = include_str!("../../files/day03.txt")
        .lines()
        .map(|line| line.to_owned())
        .map(|line| SchematicLine::new(&line))
        .collect::<Vec<_>>();

    println!("Part1: {}", part1(lines.clone()));
    println!("Part2: {}", part2(lines.clone()));

    // println!("Schmatic:");
    // for line in lines {
    //     println!("{}", line.line);
    //     for part in line.parts {
    //         println!("{},{} - {}", part.start, part.size, part.value);
    //     }
    //     println!("{:?}", line.gears);
    // }
}

fn part1(input: Vec<SchematicLine>) -> i32 {
    let mut parts = vec![];
    for idx in 0..input.len() {
        let prev = if idx > 0 {
            Some(&input[idx - 1].line)
        } else {
            None
        };
        let curr = &input[idx];
        let next = if (idx + 1) < input.len() {
            Some(&input[idx + 1].line)
        } else {
            None
        };

        // println!("Prev: {}", prev.unwrap_or(&"".to_string()));
        // println!("Curr: {}", curr.line);
        // println!("Next: {}", next.unwrap_or(&"".to_string()));
        for part in curr.parts.clone() {
            // println!("{},{} - {}", part.start, part.size, part.value);
            if is_part_number(&part, &prev, &curr.line, &next) {
                // println!("Good Part");
                parts.push(part);
            }
        }
        // println!();
    }

    parts.iter().fold(0, |acc, val| acc + val.value)
}

fn part2(input: Vec<SchematicLine>) -> i32 {
    let mut ratios = vec![];
    for idx in 0..input.len() {
        if input[idx].gears.is_empty() {
            continue;
        }

        let mut prev = if idx > 0 {
            input[idx - 1]
                .parts
                .iter()
                .map(|pn| {
                    (
                        pn.value,
                        if pn.start > 0 { pn.start - 1 } else { 0 },
                        pn.start + pn.size,
                    )
                })
                .collect::<Vec<_>>()
        } else {
            vec![]
        };
        let mut next = if (idx + 1) < input.len() {
            input[idx + 1]
                .parts
                .iter()
                .map(|pn| {
                    (
                        pn.value,
                        if pn.start > 0 { pn.start - 1 } else { 0 },
                        pn.start + pn.size,
                    )
                })
                .collect::<Vec<_>>()
        } else {
            vec![]
        };

        let mut numbers = input[idx]
            .parts
            .iter()
            .map(|pn| {
                (
                    pn.value,
                    if pn.start > 0 { pn.start - 1 } else { 0 },
                    pn.start + pn.size,
                )
            })
            .collect::<Vec<_>>();
        numbers.append(&mut prev);
        numbers.append(&mut next);

        for gear in input[idx].gears.clone() {
            let parts = numbers
                .clone()
                .iter()
                .filter_map(|pn| {
                    if pn.1 <= gear && gear <= pn.2 {
                        Some(pn.0)
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();
            if parts.len() == 2 {
                ratios.push(parts[0] * parts[1]);
            }
        }
    }

    ratios.iter().sum::<i32>()
}

fn is_symbol(char: char) -> bool {
    char != '.' && !char.is_ascii_digit()
}
fn is_part_number(
    part: &PartNumber,
    prev: &Option<&String>,
    line: &str,
    next: &Option<&String>,
) -> bool {
    let asp = if part.start > 0 { part.start - 1 } else { 0 };
    let al = part.size + if part.start > 0 { 1 } else { 0 } + 1;
    // println!(
    //     "Part: {},{} - Search: {},{}",
    //     part.start, part.size, asp, al,
    // );

    if substr(line, asp, al).contains(is_symbol) {
        return true;
    }

    if let Some(prev_line) = prev {
        if substr(prev_line, asp, al).contains(is_symbol) {
            return true;
        }
    }

    if let Some(next_line) = next {
        return substr(next_line, asp, al).contains(is_symbol);
    }

    false
}

fn substr(str: &str, start: usize, size: usize) -> String {
    str.chars().skip(start).take(size).collect()
}

#[derive(Clone, Debug)]
struct SchematicLine {
    pub line: String,
    pub parts: Vec<PartNumber>,
    pub gears: Vec<usize>,
}
impl SchematicLine {
    pub fn new(line: &str) -> SchematicLine {
        SchematicLine {
            line: line.to_string(),
            parts: get_numbers(line),
            gears: find_all(line, '*'),
        }
    }
}
#[derive(Clone, Debug)]
struct PartNumber {
    pub value: i32,
    pub start: usize,
    pub size: usize,
}

fn get_numbers(line: &str) -> Vec<PartNumber> {
    let mut numbers = vec![];
    let mut pos = 0;
    while let Some(p) = line
        .chars()
        .skip(pos)
        .collect::<String>()
        .find(|c: char| c.is_ascii_digit())
    {
        let value = line
            .chars()
            .skip(pos + p)
            .take_while(|c| c.is_ascii_digit())
            .collect::<String>();
        numbers.push(PartNumber {
            value: value.parse().unwrap(),
            start: pos + p,
            size: value.len(),
        });
        pos += p + value.len();
    }

    numbers
}

fn find_all(str: &str, char: char) -> Vec<usize> {
    let mut results: Vec<usize> = vec![];
    let mut pos: usize = 0;
    while let Some(idx) = str.chars().skip(pos).collect::<String>().find(char) {
        results.push(idx + pos);
        pos += idx + 1;
    }

    results
}
