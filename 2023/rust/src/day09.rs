fn main() {
    let input = include_str!("../../files/day09-test.txt")
        .lines()
        .map(map_input)
        .collect::<Vec<_>>();

    println!("Sample:");
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
    println!();

    let input = include_str!("../../files/day09.txt")
        .lines()
        .map(map_input)
        .collect::<Vec<_>>();

    println!("Actual:");
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
    println!();
}

fn map_input(line: &str) -> Vec<i32> {
    line.split_ascii_whitespace()
        .map(|v| v.parse::<i32>().unwrap())
        .collect()
}

fn part1(sets: &[Vec<i32>]) -> i32 {
    sets.iter().map(|s| predict(s)).sum::<i32>()
}

fn part2(sets: &[Vec<i32>]) -> i32 {
    sets.iter()
        .map(|s| s.iter().rev().copied().collect::<Vec<_>>())
        .map(|s| predict(&s))
        .sum::<i32>()
}

fn predict(set: &[i32]) -> i32 {
    generate_history(set)
        .iter()
        .filter_map(|h| h.last())
        .sum::<i32>()
}

fn generate_history(set: &[i32]) -> Vec<Vec<i32>> {
    if set.iter().all(|v| 0.eq(v)) {
        return vec![set.to_vec()];
    }

    let mut hd = set.first();
    let mut tl = set.iter().skip(1).collect::<Vec<_>>();
    let mut result: Vec<i32> = vec![];
    while hd.is_some() {
        let prev = hd.unwrap();
        if let Some(next) = tl.first() {
            let v = *next - prev;
            result.push(v);
            hd = Some(*next);
            tl = tl.into_iter().skip(1).collect::<Vec<_>>();
        } else {
            break;
        }
    }

    let mut ret = vec![set.to_vec()];
    ret.extend(generate_history(&result));
    ret
}
