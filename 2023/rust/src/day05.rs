use std::{cmp::Ordering, collections::HashMap};

fn main() {
    let input = include_str!("../../files/day05-test.txt").to_owned();
    println!("sample");
    process_file(input);

    let input = include_str!("../../files/day05.txt").to_owned();
    println!("actual");
    process_file(input);
}

#[cfg(windows)]
const LINE_ENDING: &str = "\r\n";
#[cfg(not(windows))]
const LINE_ENDING: &str = "\n";

fn process_file(input: String) {
    let input = input
        .split(&(LINE_ENDING.to_owned() + LINE_ENDING))
        .map(|s| s.trim())
        .collect::<Vec<_>>();

    let seeds = input
        .iter()
        .clone()
        .take(1)
        .collect::<Vec<_>>()
        .first()
        .unwrap()
        .to_owned()
        .to_owned();

    let seeds = get_seeds(seeds);
    let mut parsed = input.into_iter().skip(1).map(Map::new).collect::<Vec<_>>();
    let max = parsed
        .iter()
        .flat_map(|m| m.map.iter())
        .flat_map(|r| vec![r.src + r.range, r.dest + r.range])
        .max()
        .unwrap();
    for map in parsed.iter_mut() {
        let r = map.map.last().unwrap();
        let fr = Range {
            dest: r.src + r.range,
            src: r.src + r.range,
            range: max - r.src - r.range,
        };
        map.map.push(fr);
    }

    let parsed_map = parsed
        .iter()
        .map(|m| (m.from, m))
        .collect::<HashMap<_, _>>();
    let mut lowest = usize::MAX;
    for seed in seeds {
        println!("start {:?}", seed);
        let mut remaining = seed.length;
        let mut start = seed.start;
        while remaining > 0 {
            let (start_location, consumed) = walk(start, remaining, "seed", &parsed_map);

            remaining -= consumed;
            start += consumed;
            if consumed > 1 {
                println!("consumed {consumed}");
            }
            if start_location < lowest {
                lowest = start_location;
            }
        }
        println!("finished {:?}", seed);
    }

    println!("{lowest}");
}

fn walk(value: usize, range: usize, name: &str, map: &HashMap<&str, &Map>) -> (usize, usize) {
    let item = match map.get(name) {
        None => return (value, range),
        Some(m) => m,
    };

    if let Some(range_item) = find(value, item) {
        let diff = value - range_item.src;
        let new_value = range_item.dest + diff;
        return walk(new_value, range.min(range_item.range - diff), item.to, map);
    }

    walk(value, 1, item.to, map)
}

fn find<'a>(value: usize, item: &&'a Map) -> Option<&'a Range> {
    match item.map.binary_search_by(|x| {
        if value < x.src {
            Ordering::Greater
        } else if value >= x.src + x.range {
            Ordering::Less
        } else {
            Ordering::Equal
        }
    }) {
        Ok(r) => Some(&item.map[r]),
        Err(_) => None,
    }
}

fn get_seeds(seeds: &str) -> Vec<SeedRange> {
    let seeds = seeds
        .split(": ")
        .skip(1)
        .collect::<Vec<_>>()
        .first()
        .unwrap()
        .split(' ')
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<_>>();

    let mut result = vec![];
    for i in (0..seeds.len()).step_by(2) {
        result.push(SeedRange {
            start: seeds[i],
            length: seeds[i + 1],
        });
    }
    result
}

#[derive(Debug, Clone)]
struct SeedRange {
    pub start: usize,
    pub length: usize,
}

#[derive(Debug, Clone)]
struct Range {
    pub dest: usize,
    pub src: usize,
    pub range: usize,
}
impl Range {
    pub fn new(line: &str) -> Range {
        let items = line
            .split(' ')
            .map(|i| i.parse::<usize>().unwrap())
            .collect::<Vec<_>>();
        Range {
            dest: items[0],
            src: items[1],
            range: items[2],
        }
    }
}

#[derive(Debug, Clone)]
struct Map<'a> {
    from: &'a str,
    to: &'a str,
    map: Vec<Range>,
}
impl Map<'static> {
    pub fn new(line: &str) -> Map {
        let contents = line.split(LINE_ENDING).filter(|l| !l.is_empty());
        let hd = contents
            .clone()
            .take(1)
            .collect::<Vec<_>>()
            .first()
            .unwrap()
            .split(' ')
            .take(1)
            .collect::<Vec<_>>()
            .first()
            .unwrap()
            .split('-')
            .collect::<Vec<_>>();
        let mut map = contents.skip(1).map(Range::new).collect::<Vec<_>>();
        map.sort_by_key(|r| r.src);

        let mut result = vec![];
        let mut start = 0;
        for range in map {
            if range.src > start {
                result.push(Range {
                    src: start,
                    dest: start,
                    range: range.src - start,
                });
            }
            start = range.src + range.range;
            result.push(range);
        }
        Map {
            from: hd[0],
            to: hd[2],
            map: result,
        }
    }
}
