use std::cmp::Ordering;

use itertools::Itertools;

fn main() {
    let input = include_str!("../../files/day07-test.txt")
        .lines()
        .map(Hand::new)
        .collect::<Vec<_>>();

    println!("Sample:");
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
    println!();

    let input = include_str!("../../files/day07.txt")
        .lines()
        .map(Hand::new)
        .collect::<Vec<_>>();

    println!("Actual:");
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

fn part1(hands: &[Hand]) -> usize {
    hands
        .iter()
        .sorted_by(|a, b| part1_hand_compare(a, b))
        .zip(1..hands.len() + 1)
        .map(|(h, r)| h.bid * r)
        .sum::<usize>()
}

fn part2(hands: &[Hand]) -> usize {
    hands
        .iter()
        .sorted_by(|a, b| part2_hand_compare(a, b))
        .zip(1..hands.len() + 1)
        .map(|(h, r)| h.bid * r)
        .sum::<usize>()
}

fn part1_hand_compare(h1: &Hand, h2: &Hand) -> Ordering {
    hand_compare(h1, h2, &false)
}

fn part2_hand_compare(h1: &Hand, h2: &Hand) -> Ordering {
    hand_compare(h1, h2, &true)
}

fn hand_compare(h1: &Hand, h2: &Hand, part2: &bool) -> Ordering {
    let r1 = if *part2 {
        &h1.part2_rank
    } else {
        &h1.part1_rank
    };
    let r2 = if *part2 {
        &h2.part2_rank
    } else {
        &h2.part1_rank
    };

    match Ord::cmp(&r1, &r2) {
        Ordering::Equal => {
            for (c1, c2) in h1
                .cards
                .iter()
                .map(|c| Hand::get_card_value(c, part2))
                .zip(h2.cards.iter().map(|c| Hand::get_card_value(c, part2)))
            {
                match c1.cmp(&c2) {
                    Ordering::Equal => {}
                    o => {
                        return o;
                    }
                }
            }
            Ordering::Equal
        }
        o => o,
    }
}

struct Hand {
    pub cards: Vec<char>,
    pub bid: usize,
    pub part1_rank: HandRank,
    pub part2_rank: HandRank,
}

impl Hand {
    pub fn new(line: &str) -> Hand {
        let data = line.split_ascii_whitespace().take(2).collect::<Vec<_>>();
        Hand {
            cards: data[0].chars().collect(),
            bid: data[1].parse().unwrap(),
            part1_rank: Hand::get_hand_rank(data[0], &false),
            part2_rank: Hand::get_hand_rank(data[0], &true),
        }
    }

    fn get_hand_rank(cards: &str, part2: &bool) -> HandRank {
        let mut hand = cards
            .chars()
            .map(|c| Hand::get_card_value(&c, part2))
            .collect::<Vec<_>>();
        hand.sort();

        let jokers = hand.iter().filter(|c| 1.eq(*c)).count();
        if jokers == 5 {
            return HandRank::FiveKind;
        }
        let hand = hand
            .into_iter()
            .filter(|c| 1.ne(c))
            .collect::<Vec<_>>()
            .clone();
        let hand = hand
            .into_iter()
            .chunk_by(|c| *c)
            .into_iter()
            .map(|i| i.1.count())
            .sorted_by(Ord::cmp)
            .rev()
            .collect::<Vec<_>>();

        let first = hand[0];
        let second = if hand.len() >= 2 { hand[1] } else { 0 };
        match (first, second, jokers) {
            (5, _, _) => HandRank::FiveKind,
            (4, _, 1) => HandRank::FiveKind,
            (4, _, _) => HandRank::FourKind,
            (3, _, 2) => HandRank::FiveKind,
            (3, _, 1) => HandRank::FourKind,
            (3, 2, _) => HandRank::FullHouse,
            (3, _, _) => HandRank::ThreeKind,
            (2, _, 3) => HandRank::FiveKind,
            (2, _, 2) => HandRank::FourKind,
            (2, 2, 1) => HandRank::FullHouse,
            (2, _, 1) => HandRank::ThreeKind,
            (2, 2, _) => HandRank::TwoPair,
            (2, _, _) => HandRank::OnePair,
            (1, _, 4) => HandRank::FiveKind,
            (1, _, 3) => HandRank::FourKind,
            (1, _, 2) => HandRank::ThreeKind,
            (1, _, 1) => HandRank::OnePair,
            _ => HandRank::HighCard,
        }
    }

    pub fn get_card_value(card: &char, part2: &bool) -> u8 {
        match card {
            '2' => 2,
            '3' => 3,
            '4' => 4,
            '5' => 5,
            '6' => 6,
            '7' => 7,
            '8' => 8,
            '9' => 9,
            'T' => 10,
            'J' => {
                if *part2 {
                    1
                } else {
                    11
                }
            }
            'Q' => 12,
            'K' => 13,
            'A' => 14,
            _ => panic!("Invalid Card: {card}"),
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
enum HandRank {
    FiveKind = 6,
    FourKind = 5,
    FullHouse = 4,
    ThreeKind = 3,
    TwoPair = 2,
    OnePair = 1,
    HighCard = 0,
}
