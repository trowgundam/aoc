fn main() {
    let input = Input::new(include_str!("../../files/day11-test.txt"));
    println!("Sample:");
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
    println!();

    let input = Input::new(include_str!("../../files/day11.txt"));
    println!("Actual:");
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}

fn part1(input: &Input) -> usize {
    input.calculate_distance(2)
}

fn part2(input: &Input) -> usize {
    input.calculate_distance(1000000)
}

struct Input {
    galaxies: Vec<(usize, usize)>,
    empty_rows: Vec<usize>,
    empty_cols: Vec<usize>,
}

impl Input {
    pub fn new(data: &str) -> Input {
        let cols: Vec<Vec<char>> = data
            .lines()
            .map(|l| l.chars().collect::<Vec<_>>())
            .collect();

        Input {
            galaxies: data
                .lines()
                .enumerate()
                .flat_map(|(y, l)| {
                    l.chars().enumerate().flat_map(move |(x, c)| match c {
                        '#' => Some((x, y)),
                        _ => None,
                    })
                })
                .collect(),
            empty_rows: data
                .lines()
                .enumerate()
                .filter_map(|(r, l)| if l.contains('#') { None } else { Some(r) })
                .collect(),
            empty_cols: transpose(cols)
                .into_iter()
                .enumerate()
                .filter_map(|(c, l)| if l.contains(&'#') { None } else { Some(c) })
                .collect(),
        }
    }

    pub fn calculate_distance(&self, factor: usize) -> usize {
        let expanded: Vec<_> = self
            .galaxies
            .iter()
            .map(|(gx, gy)| {
                let dx = self.empty_cols.iter().filter(|c| gx > c).count();
                let dy = self.empty_rows.iter().filter(|r| gy > r).count();
                (gx + (dx * factor) - dx, gy + (dy * factor) - dy)
            })
            .collect();
        let pairs: Vec<_> = expanded
            .iter()
            .enumerate()
            .flat_map(|(i, g)| {
                if i == (self.galaxies.len() - 1) {
                    vec![]
                } else {
                    expanded[i + 1..].iter().map(|g2| (g, g2)).collect()
                }
            })
            .collect();
        pairs
            .iter()
            .map(|((x1, y1), (x2, y2))| {
                let dx = if x1 > x2 { x1 - x2 } else { x2 - x1 };
                let dy = if y1 > y2 { y1 - y2 } else { y2 - y1 };
                dx + dy
            })
            .sum::<usize>()
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        println!("Input:");
        let rows = self
            .empty_rows
            .iter()
            .map(|r| r.to_string())
            .collect::<Vec<_>>()
            .join(", ");
        let cols = self
            .empty_cols
            .iter()
            .map(|r| r.to_string())
            .collect::<Vec<_>>()
            .join(", ");
        println!("Empty Rows:    [{rows}]");
        println!("Empty Columns: [{cols}]");
        println!("Galaxies:");
        for (gx, gy) in self.galaxies.iter() {
            println!("({gx}, {gy})");
        }
    }
}

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|n| n.into_iter()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}
