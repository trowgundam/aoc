fn main() {
    let lines = include_str!("../../files/day01.txt")
        .lines()
        .map(|line| line.to_owned())
        .collect::<Vec<_>>();

    println!("Part1: {}", part1(&lines));
    println!("Part2: {}", part2(&lines));
}

fn part2(input: &[String]) -> i32 {
    input
        .iter()
        .map(|line| {
            let mut digits = vec![];
            for idx in 0..line.len() {
                let subval = line.chars().skip(idx).collect::<String>();
                if let Some(v) = get_digit(&subval) {
                    digits.push(v)
                }
            }
            let first = digits.first().unwrap();
            let last = digits.last().unwrap();
            format!("{}{}", first, last).parse::<i32>().unwrap_or(0)
        })
        .sum::<i32>()
}

fn part1(input: &[String]) -> i32 {
    input
        .iter()
        .map(|line| {
            let digits = line
                .chars()
                .filter(|c| c.is_ascii_digit())
                .collect::<Vec<_>>();

            let first = *digits.first().unwrap_or(&' ');
            let last = *digits.last().unwrap_or(&' ');
            vec![first, last]
                .into_iter()
                .collect::<String>()
                .parse::<i32>()
                .unwrap_or(0)
        })
        .sum::<i32>()
}

fn get_digit(val: &str) -> Option<i32> {
    let char = val.chars().next().unwrap();
    if char.is_ascii_digit() {
        return match vec![char].into_iter().collect::<String>().parse::<i32>() {
            Ok(v) => Some(v),
            Err(_) => None,
        };
    }

    if val.len() < 3 {
        return None;
    }

    match &val[..3] {
        "one" => return Some(1),
        "two" => return Some(2),
        "six" => return Some(6),
        _ => {}
    }

    if val.len() < 4 {
        return None;
    }

    match &val[..4] {
        "four" => return Some(4),
        "five" => return Some(5),
        "nine" => return Some(9),
        "zero" => return Some(0),
        _ => {}
    }

    if val.len() < 5 {
        return None;
    }

    match &val[..5] {
        "three" => Some(3),
        "seven" => Some(7),
        "eight" => Some(8),
        _ => None,
    }
}
