use core::panic;

use colored::{ColoredString, Colorize};

fn main() {
    let map = process_input(include_str!("../../files/day10.txt"));
    let pipe = walk_pipe(&map);

    println!("Part 1: {}", part1(&pipe));
    println!("Part 2: {}", part2(&map, &pipe));
}

fn part1(pipe: &[(usize, usize)]) -> usize {
    pipe.len() / 2
}

fn part2(map: &[Vec<PipePiece>], pipe: &[(usize, usize)]) -> usize {
    let (start, start_piece) = get_start(map);
    let mut map: Vec<Vec<_>> = map
        .iter()
        .enumerate()
        .map(|(y, r)| {
            r.iter()
                .enumerate()
                .map(move |(x, p)| {
                    if pipe.contains(&(x, y)) {
                        p
                    } else {
                        &PipePiece::Ground
                    }
                })
                .collect()
        })
        .collect();
    map[start.1][start.0] = &start_piece;

    let map: Vec<Vec<_>> = map
        .into_iter()
        .flat_map(|row| {
            let mut row1 = vec![];
            let mut row2 = vec![];
            row.iter().for_each(|c| {
                let (exp1, exp2) = expand_pipe(c);
                row1.extend(exp1);
                row2.extend(exp2);
            });
            vec![row1, row2]
        })
        .collect();
    let mut map: Vec<Vec<_>> = map
        .iter()
        .enumerate()
        .map(|(y, row)| {
            row.iter()
                .enumerate()
                .map(|(x, p)| {
                    if y == 0 || y == map.len() - 1 || x == 0 || x == row.len() - 1 {
                        if let PipePiece::Ground = p {
                            &PipePiece::Flooded
                        } else {
                            p
                        }
                    } else {
                        p
                    }
                })
                .collect()
        })
        .collect();

    let mut filled = true;
    while filled {
        filled = false;
        for (y, row) in map.clone().iter().enumerate() {
            for (x, p) in row.iter().enumerate() {
                if let PipePiece::Ground = p {
                    if let PipePiece::Flooded = map[y - 1][x] {
                        filled = true;
                        map[y][x] = &PipePiece::Flooded;
                    }
                    if let PipePiece::Flooded = map[y + 1][x] {
                        filled = true;
                        map[y][x] = &PipePiece::Flooded;
                    }
                    if let PipePiece::Flooded = map[y][x - 1] {
                        filled = true;
                        map[y][x] = &PipePiece::Flooded;
                    }
                    if let PipePiece::Flooded = map[y][x + 1] {
                        filled = true;
                        map[y][x] = &PipePiece::Flooded;
                    }
                }
            }
        }
    }

    let map: Vec<Vec<_>> = map
        .iter()
        .enumerate()
        .filter_map(|(y, row)| {
            if y % 2 == 0 {
                Some(
                    row.iter()
                        .enumerate()
                        .filter_map(|(x, p)| if x % 2 == 0 { Some(p) } else { None })
                        .collect(),
                )
            } else {
                None
            }
        })
        .collect();

    for row in map.clone() {
        for col in row {
            print!("{}", char_of_pipe_piece(col));
        }
        println!();
    }
    map.iter()
        .flatten()
        .filter(|p| matches!(p, PipePiece::Ground))
        .count()
}

fn process_input(input: &str) -> Vec<Vec<PipePiece>> {
    input
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| match c {
                    '.' => PipePiece::Ground,
                    'S' => PipePiece::Start,
                    'F' => PipePiece::NorthWest,
                    '-' => PipePiece::WestEast,
                    '7' => PipePiece::NorthEast,
                    '|' => PipePiece::NorthSouth,
                    'J' => PipePiece::SouthEast,
                    'L' => PipePiece::SouthWest,
                    _ => panic!("Invalid Map Piece: {c}"),
                })
                .collect()
        })
        .collect()
}

fn walk_pipe(map: &[Vec<PipePiece>]) -> Vec<(usize, usize)> {
    let (start, start_piece) = get_start(map);
    let (mod1, _) = get_mod(&start_piece);
    let next = translate(start, mod1);
    walk(map, vec![start], start, next)
}
fn translate((x, y): (usize, usize), dir: Direction) -> (usize, usize) {
    match dir {
        Direction::North => (x, y - 1),
        Direction::West => (x - 1, y),
        Direction::South => (x, y + 1),
        Direction::East => (x + 1, y),
    }
}
fn walk(
    map: &[Vec<PipePiece>],
    mut pipe: Vec<(usize, usize)>,
    prev: (usize, usize),
    curr: (usize, usize),
) -> Vec<(usize, usize)> {
    let piece = &map[curr.1][curr.0];
    if let PipePiece::Start = piece {
        return pipe;
    }
    pipe.push(curr);
    let mods = get_mod(piece);
    let poss1 = translate(curr, mods.0);
    let poss2 = translate(curr, mods.1);
    let next = if poss1 == prev { poss2 } else { poss1 };
    walk(map, pipe, curr, next)
}

fn get_start(map: &[Vec<PipePiece>]) -> ((usize, usize), PipePiece) {
    let translate_start = |x: &usize, y: &usize| {
        let is_good = |dx: usize, dy: usize| {
            let piece = &map[dy][dx];
            if dy < *y {
                match piece {
                    PipePiece::NorthWest | PipePiece::NorthEast | PipePiece::NorthSouth => {
                        return true
                    }
                    _ => return false,
                }
            }
            if dy > *y {
                match piece {
                    PipePiece::SouthWest | PipePiece::SouthEast | PipePiece::NorthSouth => {
                        return true
                    }
                    _ => return false,
                }
            }
            if dx < *x {
                match piece {
                    PipePiece::NorthWest | PipePiece::SouthWest | PipePiece::WestEast => {
                        return true
                    }
                    _ => return false,
                }
            }
            if dx > *x {
                match piece {
                    PipePiece::NorthEast | PipePiece::SouthEast | PipePiece::WestEast => {
                        return true
                    }
                    _ => return false,
                }
            }
            false
        };
        let surroundings = (
            is_good(*x, y - 1),
            is_good(x + 1, *y),
            is_good(*x, y + 1),
            is_good(x - 1, *y),
        );
        match surroundings {
            (true, true, false, false) => PipePiece::SouthWest,
            (true, false, true, false) => PipePiece::NorthSouth,
            (true, false, false, true) => PipePiece::SouthEast,
            (false, true, true, false) => PipePiece::NorthWest,
            (false, true, false, true) => PipePiece::WestEast,
            (false, false, true, true) => PipePiece::NorthEast,
            _ => panic!("Invalid Start"),
        }
    };
    for (y, row) in map.iter().enumerate() {
        for (x, piece) in row.iter().enumerate() {
            if let PipePiece::Start = piece {
                return ((x, y), translate_start(&x, &y));
            }
        }
    }

    panic!("Could not find start");
}

fn get_mod(piece: &PipePiece) -> (Direction, Direction) {
    match piece {
        PipePiece::NorthWest => (Direction::South, Direction::East),
        PipePiece::WestEast => (Direction::West, Direction::East),
        PipePiece::NorthEast => (Direction::South, Direction::West),
        PipePiece::NorthSouth => (Direction::North, Direction::South),
        PipePiece::SouthEast => (Direction::North, Direction::West),
        PipePiece::SouthWest => (Direction::North, Direction::East),
        _ => panic!("Invalid Pipe Piece"),
    }
}
fn char_of_pipe_piece(p: &PipePiece) -> ColoredString {
    match p {
        PipePiece::Ground => " ".clear(),
        PipePiece::Start => "@".yellow(),
        PipePiece::NorthSouth => "┃".green(),
        PipePiece::NorthWest => "┏".green(),
        PipePiece::WestEast => "━".green(),
        PipePiece::NorthEast => "┓".green(),
        PipePiece::SouthEast => "┛".green(),
        PipePiece::SouthWest => "┗".green(),
        PipePiece::Flooded => "█".blue(),
    }
}
fn expand_pipe(p: &PipePiece) -> (Vec<PipePiece>, Vec<PipePiece>) {
    match p {
        PipePiece::Ground => (
            vec![PipePiece::Ground, PipePiece::Ground],
            vec![PipePiece::Ground, PipePiece::Ground],
        ),
        PipePiece::NorthSouth => (
            vec![PipePiece::NorthSouth, PipePiece::Ground],
            vec![PipePiece::NorthSouth, PipePiece::Ground],
        ),
        PipePiece::NorthWest => (
            vec![PipePiece::NorthWest, PipePiece::WestEast],
            vec![PipePiece::NorthSouth, PipePiece::Ground],
        ),
        PipePiece::WestEast => (
            vec![PipePiece::WestEast, PipePiece::WestEast],
            vec![PipePiece::Ground, PipePiece::Ground],
        ),
        PipePiece::NorthEast => (
            vec![PipePiece::NorthEast, PipePiece::Ground],
            vec![PipePiece::NorthSouth, PipePiece::Ground],
        ),
        PipePiece::SouthEast => (
            vec![PipePiece::SouthEast, PipePiece::Ground],
            vec![PipePiece::Ground, PipePiece::Ground],
        ),
        PipePiece::SouthWest => (
            vec![PipePiece::SouthWest, PipePiece::WestEast],
            vec![PipePiece::Ground, PipePiece::Ground],
        ),
        PipePiece::Flooded => (
            vec![PipePiece::Flooded, PipePiece::Flooded],
            vec![PipePiece::Flooded, PipePiece::Flooded],
        ),
        PipePiece::Start => panic!("Invalid Pipe to expand"),
    }
}

enum Direction {
    North,
    West,
    South,
    East,
}

enum PipePiece {
    Ground,
    Start,
    NorthWest,
    WestEast,
    NorthEast,
    NorthSouth,
    SouthEast,
    SouthWest,
    Flooded,
}
