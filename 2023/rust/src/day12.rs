use std::collections::HashMap;

fn main() {
    let input: Vec<_> = include_str!("../../files/day12-test.txt")
        .lines()
        .map(Data::new)
        .collect();

    println!("Sample:");
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));

    let input: Vec<_> = include_str!("../../files/day12.txt")
        .lines()
        .map(Data::new)
        .collect();

    println!("Actual:");
    println!("Part 1: {}", part1(&input));
    println!("Part 2: {}", part2(&input));
}

fn part1(input: &[Data]) -> usize {
    input.iter().map(process).sum::<usize>()
}

fn part2(input: &[Data]) -> usize {
    input
        .iter()
        .map(|d| {
            let s: Vec<_> =
                d.0.iter()
                    .copied()
                    .chain(vec![None])
                    .chain(d.0.clone())
                    .chain(vec![None])
                    .chain(d.0.clone())
                    .chain(vec![None])
                    .chain(d.0.clone())
                    .chain(vec![None])
                    .chain(d.0.clone())
                    .collect();
            let c: Vec<_> = d.1.clone().repeat(5);
            Data(s, c)
        })
        .map(|d| process(&d))
        .sum::<usize>()
}

fn process(data: &Data) -> usize {
    let status = &data.0;
    let counts = &data.1;
    let mut cache = HashMap::with_capacity(1024);

    process_int(status, counts, &mut cache)
}

#[allow(dead_code)]
fn convert_data_to_vec(data: &str) -> Vec<Option<bool>> {
    data.chars()
        .map(|c| match c {
            '?' => None,
            '#' => Some(false),
            '.' => Some(true),
            _ => panic!("Invalid Char: {c}"),
        })
        .collect()
}

#[allow(dead_code)]
fn convert_data_to_string(data: &[Option<bool>]) -> String {
    data.iter()
        .map(|v| match v {
            None => '?',
            Some(false) => '#',
            Some(true) => '.',
        })
        .collect()
}

fn process_int(
    status: &[Option<bool>],
    counts: &[usize],
    cache: &mut HashMap<(Vec<Option<bool>>, Vec<usize>), usize>,
) -> usize {
    // let s = convert_data_to_string(status);
    // let c: String = counts
    //     .iter()
    //     .map(usize::to_string)
    //     .fold(
    //         "".to_string(),
    //         |a, c| {
    //             if a.is_empty() {
    //                 c
    //             } else {
    //                 a + ", " + &c
    //             }
    //         },
    //     );
    // println!("process_int(\"{s}\", [ {c} ], {})", cache.len());
    if let Some(r) = cache.get(&(status.to_vec(), counts.to_vec())) {
        // println!("cached {r}");
        return *r;
    }

    let result = match (status.split_first(), counts.split_first()) {
        (None, None) => 1,
        (None, _) => 0,
        (Some((Some(false), _)), None) => 0,
        (Some((Some(true), ss)), _) => process_int(ss, counts, cache),
        (Some((Some(false), _)), Some((c, cs))) => match splitat(status, *c) {
            None => 0,
            Some((prefix, suffix)) => {
                if prefix
                    .iter()
                    .all(|v| v.is_none() || matches!(v, Some(false)))
                {
                    match suffix.split_first() {
                        Some((Some(false), _)) => 0,
                        Some((None, ss)) => process_int(
                            &[Some(true)].iter().chain(ss).copied().collect::<Vec<_>>(),
                            counts,
                            cache,
                        ),
                        _ => process_int(&suffix, cs, cache),
                    }
                } else {
                    0
                }
            }
        },
        (Some((None, ss)), _) => {
            process_int(
                &[Some(true)].iter().chain(ss).copied().collect::<Vec<_>>(),
                counts,
                cache,
            ) + process_int(
                &[Some(false)].iter().chain(ss).copied().collect::<Vec<_>>(),
                counts,
                cache,
            )
        }
    };
    cache.insert((status.to_vec(), counts.to_vec()), result);
    // println!("calculated {result}");
    result
}

type OptBoolVec = Vec<Option<bool>>;

fn splitat(iter: &[Option<bool>], n: usize) -> Option<(OptBoolVec, OptBoolVec)> {
    let left: Vec<_> = iter.iter().take(n).copied().collect();
    let right: Vec<_> = iter.iter().skip(n).copied().collect();
    if left.len() < n {
        None
    } else {
        Some((left, right))
    }
}

struct Data(Vec<Option<bool>>, Vec<usize>);
impl Data {
    pub fn new(text: &str) -> Data {
        let input: Vec<_> = text.split_ascii_whitespace().take(2).collect();
        Data(
            input[0]
                .chars()
                .map(|c| match c {
                    '?' => None,
                    '#' => Some(false),
                    '.' => Some(true),
                    _ => panic!("Invalid Input"),
                })
                .collect(),
            input[1]
                .split(',')
                .map(|s| s.parse::<usize>().unwrap())
                .collect(),
        )
    }
}
