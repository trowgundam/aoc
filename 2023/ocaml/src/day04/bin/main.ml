open Core
open In_channel

let list_contains l v =
  match List.find l ~f:(fun t -> t = v) with
  | None -> false
  | Some _ -> true
;;

let card_value winning haves =
  match List.count haves ~f:(list_contains winning) with
  | 0 -> 0, 0
  | p -> p, Int.pow 2 @@ (p - 1)
;;

let process_card line =
  match String.split line ~on:':' |> List.map ~f:String.strip with
  | [ header; body ] ->
    let card_id = String.slice header 4 0 |> String.strip |> int_of_string in
    let split_nums nums =
      String.substr_replace_all nums ~pattern:"  " ~with_:" "
      |> String.split ~on:' '
      |> List.map ~f:String.strip
      |> List.map ~f:int_of_string
    in
    (match String.split body ~on:'|' |> List.map ~f:String.strip with
     | [ winning; haves ] ->
       let winning_nums = split_nums winning in
       let have_nums = split_nums haves in
       let win_count, score = card_value winning_nums have_nums in
       card_id, win_count, score
     | _ -> failwith "Invalid Input")
  | _ -> failwith "Invalid Input"
;;

let part1 lines =
  List.map lines ~f:process_card
  |> List.fold ~init:0 ~f:(fun acc (_, _, score) -> acc + score)
;;

let range s e =
  let rec rangei st et r = if st <= et then rangei (st + 1) et @@ r @ [ st ] else r in
  rangei (s + 1) e []
;;

let get_card_cards id card_set =
  let find_card i = List.find card_set ~f:(fun (card_id, _, _) -> i = card_id) in
  let rec cards i =
    match find_card i with
    | None -> []
    | Some (_, count, _) ->
      let my_cards =
        range i (i + count) |> List.map ~f:cards |> List.fold ~init:[] ~f:( @ )
      in
      [ i ] @ my_cards
  in
  match find_card id with
  | None -> []
  | Some (_, count, _) ->
    range id (id + count) |> List.map ~f:cards |> List.fold ~init:[] ~f:( @ )
;;

let part2 lines =
  let cards = List.map lines ~f:process_card in
  let adj_cards =
    List.map cards ~f:(fun (id, c, s) -> id, c, s, get_card_cards id cards)
  in
  List.map adj_cards ~f:(fun (_, _, _, c) -> 1 + List.length c)
  |> List.fold ~init:0 ~f:( + )
;;

let ( ++ ) f1 f2 p = f1 p, f2 p

let () =
  let part1, part2 = input_lines stdin |> part1 ++ part2 in
  printf "Part1: %d\nPart2: %d\n" part1 part2
;;
