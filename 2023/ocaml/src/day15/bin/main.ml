open Core

let process_input (input : string) =
  String.split input ~on:','
  |> List.map ~f:String.strip
  |> List.filter ~f:(fun s -> not @@ String.is_empty s)
;;

let process_line (line : string) =
  String.to_list line |> List.fold ~init:0 ~f:(fun a c -> (a + Char.to_int c) * 17 % 256)
;;

let part1 (input : string list) =
  List.fold input ~init:0 ~f:(fun a l -> a + process_line l)
;;

let part2 (input : string list) =
  let convert_line line =
    let equal = String.contains line '=' in
    let items =
      String.split_on_chars line ~on:[ '-'; '=' ]
      |> List.filter ~f:(fun i -> not @@ String.is_empty i)
    in
    let label = List.nth_exn items 0 in
    label, label |> process_line, equal, List.nth items 1 |> Option.map ~f:int_of_string
  in
  let boxes = Hashtbl.create ~size:256 (module Int) in
  let process_item (label, box_num, equal, focal) =
    let box = Hashtbl.find_or_add boxes box_num ~default:(fun () -> []) in
    let new_box =
      if equal
      then
        if List.find box ~f:(fun (l, _) -> String.equal label l) |> Option.is_none
        then box @ [ label, Option.value_exn focal ]
        else
          List.map box ~f:(fun (l, f) ->
            if String.equal label l then l, Option.value_exn focal else l, f)
      else List.filter box ~f:(fun (l, _) -> not @@ String.equal label l)
    in
    Hashtbl.set boxes ~key:box_num ~data:new_box
  in
  let input = List.map input ~f:convert_line in
  List.iter input ~f:process_item;
  let process_box (box, lens) =
    List.foldi lens ~init:0 ~f:(fun p a (_, f) -> a + ((box + 1) * (p + 1) * f))
  in
  Hashtbl.to_alist boxes |> List.fold ~init:0 ~f:(fun a b -> a + process_box b)
;;

let () =
  printf "Sample:\n";
  let input = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n";
  print_endline "";
  printf "Actual:\n";
  let input = In_channel.read_all "../input.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n"
;;
