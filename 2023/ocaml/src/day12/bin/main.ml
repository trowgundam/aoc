open Core

type status =
  | Unknown
  | Good
  | Broken

let int_to_status v =
  match v with
  | 0 -> Unknown
  | 1 -> Good
  | 2 -> Broken
  | _ -> Unknown
;;

let status_to_int status =
  match status with
  | Unknown -> 0
  | Good -> 1
  | Broken -> 2
;;

let comparator_status s1 s2 = compare_int (status_to_int s1) (status_to_int s2)

let compare_status (s1 : status) (s2 : status) =
  let get_int (s : status) =
    match s with
    | Unknown -> 0
    | Good -> 1
    | Broken -> 2
  in
  get_int s1 = get_int s2
;;

type record =
  { status : status list
  ; counts : int list
  }

let process_line (line : string) =
  let process_char (c : char) =
    match c with
    | '?' -> Unknown
    | '.' -> Good
    | '#' -> Broken
    | _ -> failwith @@ sprintf "Invalid Char: %c" c
  in
  let data = String.split_on_chars line ~on:[ ' ' ] in
  { status = List.nth_exn data 0 |> String.to_list |> List.map ~f:process_char
  ; counts =
      List.nth_exn data 1
      |> String.split_on_chars ~on:[ ',' ]
      |> List.map ~f:int_of_string
  }
;;

module Record = struct
  module T = struct
    type t = status list * int list

    let compare_status = compare_list comparator_status
    let compare_counts = compare_list compare_int

    let compare (x1, y1) (x2, y2) =
      match compare_status x1 x2 with
      | 0 -> compare_counts y1 y2
      | b -> b
    ;;

    let sexp_of_t_status r = List.map r ~f:status_to_int |> List.sexp_of_t Int.sexp_of_t
    let sexp_of_t_counts = List.sexp_of_t Int.sexp_of_t
    let sexp_of_t (s, c) = Tuple2.sexp_of_t sexp_of_t_status sexp_of_t_counts (s, c)
    let t_of_sexp_status r = List.t_of_sexp Int.t_of_sexp r |> List.map ~f:int_to_status
    let t_of_sexp_counts = List.t_of_sexp Int.t_of_sexp
    let t_of_sexp = Tuple2.t_of_sexp t_of_sexp_status t_of_sexp_counts
    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

let check_record ((_, record) : int * record) =
  let ht = Hashtbl.create ~size:1024 (module Record) in
  let splitat lst n =
    let l, r = List.split_n lst n in
    if List.length l <> n then None else Some (l, r)
  in
  let rec process (status : status list) (counts : int list) =
    let result =
      match Hashtbl.find ht (status, counts) with
      | Some x -> x
      | None ->
        (match status, counts with
         | [], [] -> 1
         | [], _ -> 0
         | Broken :: _, [] -> 0
         | Good :: ss, _ -> process ss counts
         | Broken :: _, c :: cs ->
           (match splitat status c with
            | None -> 0
            | Some (prefix, status) ->
              if List.for_all
                   prefix
                   ~f:(List.mem [ Broken; Unknown ] ~equal:compare_status)
              then (
                match status with
                | Broken :: _ -> 0
                | Unknown :: ss -> process (Good :: ss) cs
                | _ -> process status cs)
              else 0)
         | Unknown :: ss, _ -> process (Broken :: ss) counts + process (Good :: ss) counts)
    in
    let _ = Hashtbl.add ht ~key:(status, counts) ~data:result in
    result
  in
  (* printf "%s " (string_of_int (idx + 1) |> String.pad_left ~len:4); *)
  (* print_record record; *)
  let result = process record.status record.counts in
  (* printf "result = %d\n" result; *)
  result
;;

let part1 (records : record list) =
  (* let records = List.take (List.drop records 1) 1 in *)
  records
  |> List.zip_exn (List.range 0 (List.length records))
  |> List.map ~f:check_record
  |> List.fold ~init:0 ~f:( + )
;;

let part2 (records : record list) =
  (* let records = List.take (List.drop records 0) 1 in *)
  let expand (record : record) =
    { status =
        List.range 0 5
        |> List.map ~f:(fun _ -> [ Unknown ] @ record.status)
        |> List.join
        |> List.tl_exn
    ; counts = List.range 0 5 |> List.map ~f:(fun _ -> record.counts) |> List.join
    }
  in
  List.map records ~f:expand
  |> List.zip_exn (List.range 0 (List.length records))
  |> List.map ~f:check_record
  |> List.fold ~init:0 ~f:( + )
;;

let () =
  let input = In_channel.read_lines "../input-sample.txt" |> List.map ~f:process_line in
  printf "Sample:\n";
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n";
  print_endline "";
  let input = In_channel.read_lines "../input.txt" |> List.map ~f:process_line in
  printf "Actual:\n";
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n"
;;
