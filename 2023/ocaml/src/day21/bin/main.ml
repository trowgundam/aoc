open Core

type mc =
  | Start
  | Garden
  | Rock

type mc_s =
  | GardenS of int option ref
  | RockS

let is_rock mc =
  match mc with
  | Rock -> true
  | _ -> false
;;

let process_input input =
  String.split_lines input
  |> List.map ~f:(fun l ->
    String.to_array l
    |> Array.map ~f:(fun c ->
      match c with
      | 'S' -> Start
      | '.' -> Garden
      | '#' -> Rock
      | _ -> failwith @@ sprintf "Invalid char: %c" c))
  |> Array.of_list
;;

let find_start input =
  let start_y =
    fst
    @@ Array.findi_exn input ~f:(fun _ r ->
      Array.find r ~f:(fun mc ->
        match mc with
        | Start -> true
        | _ -> false)
      |> Option.is_some)
  in
  let start_x =
    fst
    @@ Array.findi_exn input.(start_y) ~f:(fun _ mc ->
      match mc with
      | Start -> true
      | _ -> false)
  in
  start_x, start_y
;;

let compare_coord = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare

let part1 input steps =
  let start_x, start_y = find_start input in
  let height, width = Array.length input, Array.length input.(0) in
  List.range 0 steps
  |> List.fold
       ~init:[ start_x, start_y ]
       ~f:(fun positions _ ->
         List.map positions ~f:(fun (x, y) ->
           [ 0, -1; 1, 0; 0, 1; -1, 0 ]
           |> List.map ~f:(fun (dx, dy) -> x + dx, y + dy)
           |> List.filter ~f:(fun (x, y) -> not @@ is_rock input.(y).(x))
           |> List.filter ~f:(fun (x, y) ->
             Int.between x ~low:0 ~high:(width - 1)
             && Int.between y ~low:0 ~high:(height - 1)))
         |> List.join
         |> List.dedup_and_sort ~compare:compare_coord)
  |> List.dedup_and_sort ~compare:compare_coord
  |> List.length
;;

let part2 input steps =
  let height, width = Array.length input, Array.length input.(0) in
  let start_x, start_y = find_start input in
  let input =
    Array.map
      input
      ~f:
        (Array.map ~f:(fun mc ->
           match mc with
           | Start | Garden -> GardenS (ref None)
           | Rock -> RockS))
  in
  let rec read_distances queue =
    match Queue.dequeue queue with
    | Some (dist, (x, y)) ->
      (match input.(y).(x) with
       | GardenS rc when Option.is_none !rc ->
         rc := Some dist;
         [ 0, -1; 1, 0; 0, 1; -1, 0 ]
         |> List.map ~f:(fun (dx, dy) -> x + dx, y + dy)
         |> List.filter ~f:(fun (x, y) ->
           Int.between x ~low:0 ~high:(width - 1)
           && Int.between y ~low:0 ~high:(height - 1))
         |> List.iter ~f:(fun (x, y) ->
           match input.(y).(x) with
           | GardenS rc when Option.is_none !rc -> Queue.enqueue queue (dist + 1, (x, y))
           | _ -> ());
         read_distances queue
       | _ -> read_distances queue)
    | _ -> ()
  in
  let queue = Queue.create () in
  Queue.enqueue queue (0, (start_x, start_y));
  read_distances queue;
  let strictly_in_corner x y =
    Int.min (width - 1 - y) y + x < (width - 1) / 2
    || Int.max (width - 1 - y) y + x > 3 * (width - 1) / 2
  in
  let in_corner x y =
    Int.min (width - 1 - y) y + x <= (width - 1) / 2
    || Int.max (width - 1 - y) y + x >= 3 * (width - 1) / 2
  in
  let whole_even =
    Array.to_list input
    |> Array.concat
    |> Array.count ~f:(fun mc ->
      match mc with
      | GardenS d when Option.is_some !d -> (Option.value_exn @@ !d) % 2 = 0
      | _ -> false)
  in
  let whole_odd =
    Array.to_list input
    |> Array.concat
    |> Array.count ~f:(fun mc ->
      match mc with
      | GardenS d when Option.is_some !d -> (Option.value_exn @@ !d) % 2 = 1
      | _ -> false)
  in
  let center, other =
    if steps % 2 = 0 then whole_even, whole_odd else whole_odd, whole_even
  in
  let middle = start_x in
  let q = (steps - middle) / width in
  (* let r = (steps - middle) % width in *)
  let c =
    Sequence.init Int.max_value ~f:(fun n -> 2 + (n * 2))
    |> Sequence.take_while ~f:(fun k -> k < q)
    |> Sequence.fold ~init:0 ~f:( + )
  in
  let o =
    Sequence.init Int.max_value ~f:(fun n -> 1 + (n * 2))
    |> Sequence.take_while ~f:(fun k -> k < q)
    |> Sequence.fold ~init:0 ~f:( + )
  in
  let s =
    Array.mapi input ~f:(fun y row -> Array.mapi row ~f:(fun x mc -> (x, y), mc))
    |> Array.to_list
    |> Array.concat
    |> Array.map ~f:(fun ((x, y), mc) ->
      match mc with
      | GardenS d when Option.is_some !d ->
        let dist = Option.value_exn !d in
        if dist % 2 = q % 2
        then if in_corner x y then q else 0
        else if strictly_in_corner x y
        then (3 * (q - 1)) + 2
        else (4 * (q - 1)) + 4
      | _ -> 0)
    |> Array.fold ~init:0 ~f:( + )
  in
  (center * (1 + (4 * c))) + (other * 4 * o) + s
;;

let () =
  print_endline "Sample:";
  let input = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 input 6 |> sprintf "Part 1: %d" |> print_endline;
  print_endline "";
  print_endline "Actual:";
  let input = In_channel.read_all "../input.txt" |> process_input in
  part1 input 64 |> sprintf "Part 1: %d" |> print_endline;
  part2 input 26501365 |> sprintf "Part 2: %d" |> print_endline
;;
