open Core

let process_input input =
  let process_line line =
    let data = String.split_on_chars line ~on:[ ' ' ] in
    ( List.nth_exn data 0 |> String.to_list |> List.hd_exn
    , List.nth_exn data 1 |> int_of_string
    , List.nth_exn data 2 )
  in
  String.split_lines input |> List.map ~f:process_line
;;

let dig input =
  let rec dig' input (x, y) rst =
    match input with
    | [] -> rst
    | (d, c, _) :: tl ->
      let next =
        match d with
        | 'U' -> x, y - c
        | 'R' -> x + c, y
        | 'D' -> x, y + c
        | 'L' -> x - c, y
        | _ -> failwith @@ sprintf "Invalid Direction: %c" d
      in
      dig' tl next (rst @ [ next ])
  in
  dig' input (0, 0) []
;;

let calc_area input =
  let coord = dig input in
  (* print_endline "Coordinates:"; *)
  (* printf "%d\n" (List.length coord); *)
  (* List.iter coord ~f:(fun (x, y) -> sprintf "(%d, %d)" x y |> print_endline); *)
  let fx, fy = List.hd_exn coord in
  let rec calc (x1, y1) input area =
    match input with
    | [] -> area + ((x1 * fy) - (y1 * fx))
    | (x2, y2) :: tl ->
      let area = area + ((x1 * y2) - (y1 * x2)) in
      calc (x2, y2) tl area
  in
  let area =
    match coord with
    | [] -> failwith "Invalid Coordinates"
    | hd :: tl -> calc hd tl 0
  in
  let area = abs area / 2 in
  (* let edges = (List.length coord / 2) + 1 in *)
  (* let edges = *)
  (*   snd *)
  (*   @@ List.fold *)
  (*        coord *)
  (*        ~init:((0, 0), 0) *)
  (*        ~f:(fun ((x1, y1), acc) (x2, y2) -> (x2, y2), acc + abs (x2 - x1 + y2 - y1)) *)
  (* in *)
  let edges = List.fold input ~init:0 ~f:(fun a (_, c, _) -> a + c) in
  let edges = (edges / 2) + 1 in
  (* printf "area = %d; edges = %d\n" area edges; *)
  area + edges
;;

let part1 input = calc_area input

let part2 input =
  let process_color_code cc =
    let cc = String.drop_suffix (String.drop_prefix cc 2) 1 in
    let dist, dir = String.drop_suffix cc 1, String.drop_prefix cc 5 in
    let dir =
      match dir with
      | "0" -> 'R'
      | "1" -> 'D'
      | "2" -> 'L'
      | "3" -> 'U'
      | _ -> failwith @@ sprintf "Invalid Direction: %s" dir
    in
    let dist = Int.of_string @@ "0x" ^ dist in
    dir, dist, ""
  in
  let _, _, input = List.unzip3 input in
  let input = List.map input ~f:process_color_code in
  calc_area input
;;

let () =
  print_endline "Sample:";
  let input = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n";
  print_endline "";
  print_endline "Actual:";
  let input = In_channel.read_all "../input.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n"
;;
