open Core

let rec get_numbers data results pos =
  let context = String.slice data pos @@ String.length data in
  match String.find context ~f:Char.is_digit with
  | None -> results
  | Some d ->
    (match String.index context d with
     | None -> results
     | Some idx ->
       let value =
         String.slice context idx @@ String.length context
         |> String.take_while ~f:Char.is_digit
       in
       let adjusted_pos = pos + idx in
       let new_pos = adjusted_pos + String.length value in
       get_numbers
         data
         (results @ [ int_of_string value, adjusted_pos, new_pos - 1 ])
         new_pos)
;;

let is_symbol c =
  match c with
  | '.' -> false
  | '0' .. '9' -> false
  | _ -> true
;;

let is_partnumber (_, start_pos, end_pos) prev line next =
  if start_pos > 0 && (is_symbol @@ String.nget line @@ (start_pos - 1))
  then true
  else if end_pos + 1 < String.length line
          && (is_symbol @@ String.nget line @@ (end_pos + 1))
  then true
  else (
    let check_line v s e =
      let asp = max 0 @@ (s - 1) in
      let aep = min (e + 2) (String.length v) in
      let vs = String.slice v asp aep in
      match String.find vs ~f:is_symbol with
      | None -> false
      | Some _ -> true
    in
    if (not @@ String.is_empty prev) && check_line prev start_pos end_pos
    then true
    else (not @@ String.is_empty next) && check_line next start_pos end_pos)
;;

let rec process_schematic_line line prev next schematic results =
  let numbers = get_numbers line [] 0 in
  let valid_numbers = List.filter numbers ~f:(fun n -> is_partnumber n prev line next) in
  match schematic with
  | [] ->
    if String.is_empty next
    then results @ valid_numbers
    else process_schematic_line next line "" [] (results @ valid_numbers)
  | hd :: tl -> process_schematic_line next line hd tl (results @ valid_numbers)
;;

let part1 (data : string list) =
  match data with
  | line :: next :: tl -> process_schematic_line line "" next tl []
  | line :: _ -> process_schematic_line line "" "" [] []
  | [] -> failwith "Invalid Input"
;;

let is_gear line pos prev next =
  let numbers = get_numbers line [] 0 @ get_numbers prev [] 0 @ get_numbers next [] 0 in
  let valid_numbers (_, s, e) =
    let valid p = Int.between ~low:s ~high:e p in
    valid pos || (valid @@ (pos - 1)) || (valid @@ (pos + 1))
  in
  let rel_numbers =
    List.filter numbers ~f:valid_numbers |> List.map ~f:(fun (v, _, _) -> v)
  in
  let length = List.length rel_numbers in
  length = 2, rel_numbers
;;

let process_line_gears (prev, _) (line, curr_gears) (next, _) =
  List.map curr_gears ~f:(fun gp -> is_gear line gp prev next)
  |> List.filter ~f:(fun (v, _) -> v)
  |> List.map ~f:(fun (_, n) -> List.fold n ~init:1 ~f:( * ))
  |> List.fold ~init:0 ~f:( + )
;;

let rec process_gear_linesi prev (line, gears) (next, next_gears) data results =
  let line_sum = process_line_gears prev (line, gears) (next, next_gears) in
  match data with
  | [] ->
    if String.is_empty next
    then results + line_sum
    else
      process_gear_linesi (line, gears) (next, next_gears) ("", []) [] (results + line_sum)
  | hd :: tl ->
    process_gear_linesi (line, gears) (next, next_gears) hd tl (results + line_sum)
;;

let process_gear_lines data =
  match data with
  | line :: next :: tl -> process_gear_linesi ("", []) line next tl 0
  | line :: _ -> process_gear_linesi ("", []) line ("", []) [] 0
  | [] -> failwith "Invalid Input"
;;

let part2 (data : string list) =
  match
    List.map data ~f:(String.substr_index_all ~may_overlap:false ~pattern:"*")
    |> List.zip data
  with
  | Ok x -> process_gear_lines x
  | _ -> failwith "Invalid Input"
;;

(*
   let print_number ((number, start_pos, end_pos) : int * int * int) =
   Printf.printf "%d - %d to %d\n" number start_pos end_pos;;
   let print_line_info ((line, numbers) : string * (int * int * int) list) =
   Printf.printf "%s\n" line
   ; List.iter numbers ~f:print_number;;
*)

let () =
  let numbers = In_channel.input_lines In_channel.stdin in
  let part1 =
    part1 numbers |> List.map ~f:(fun (v, _, _) -> v) |> List.fold ~init:0 ~f:( + )
  in
  let part2 = part2 numbers in
  Printf.printf "Part1: %d\nPart2: %d\n" part1 part2
;;
