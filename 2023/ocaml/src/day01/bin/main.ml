open Core

let part1_map = [ "1", 1; "2", 2; "3", 3; "4", 4; "5", 5; "6", 6; "7", 7; "8", 8; "9", 9 ]

let part2_map =
  part1_map
  @ [ "one", 1
    ; "two", 2
    ; "three", 3
    ; "four", 4
    ; "five", 5
    ; "six", 6
    ; "seven", 7
    ; "eight", 8
    ; "nine", 9
    ]
;;

let get_digit (p, v) s = if String.is_prefix ~prefix:p s then Some v else None
let get_digit m c = List.find_map m ~f:(fun mv -> get_digit mv c)

let rec get_first m line =
  match get_digit m line with
  | Some v -> v
  | None -> String.suffix line (String.length line - 1) |> get_first m
;;

let rec get_last_i m line idx =
  if idx > String.length line
  then 0
  else (
    let suffix = String.suffix line idx in
    match get_digit m suffix with
    | Some v -> v
    | None -> get_last_i m line (idx + 1))
;;

let get_last m line = get_last_i m line 1

let get_line_value m line =
  let first = get_first m line in
  let last = get_last m line in
  (first * 10) + last
;;

let get_part1_value = get_line_value part1_map
let get_part2_value = get_line_value part2_map
let part1 lines = List.map lines ~f:get_part1_value |> List.fold ~init:0 ~f:( + )
let part2 lines = List.map lines ~f:get_part2_value |> List.fold ~init:0 ~f:( + )

let () =
  let lines = In_channel.input_lines In_channel.stdin in
  part1 lines |> Printf.printf "Part1: %d\n";
  part2 lines |> Printf.printf "Part1: %d\n"
;;
