open Core

let red_count = 12
let green_count = 13
let blue_count = 14

type color =
  | Red
  | Green
  | Blue

let process_result result =
  let color =
    if String.is_suffix result ~suffix:"red"
    then Red
    else if String.is_suffix result ~suffix:"blue"
    then Blue
    else if String.is_suffix result ~suffix:"green"
    then Green
    else Red
  in
  let length =
    match color with
    | Red -> String.slice result 0 (String.length result - 4)
    | Green -> String.slice result 0 (String.length result - 6)
    | Blue -> String.slice result 0 (String.length result - 5)
  in
  let v = String.strip length in
  color, int_of_string v
;;

let summarize_set ((_, tr), (_, tg), (_, tb)) (c, v) =
  let red =
    match c with
    | Red -> max tr v
    | _ -> tr
  in
  let green =
    match c with
    | Green -> max tg v
    | _ -> tg
  in
  let blue =
    match c with
    | Blue -> max tb v
    | _ -> tb
  in
  (Red, red), (Green, green), (Blue, blue)
;;

let process_result_set results =
  String.split ~on:',' results
  |> List.map ~f:String.strip
  |> List.map ~f:process_result
  |> List.fold ~init:((Red, 0), (Green, 0), (Blue, 0)) ~f:summarize_set
;;

let fold_game_peaks ((_, tr), (_, tg), (_, tb)) ((_, r), (_, g), (_, b)) =
  (Red, max tr r), (Green, max tg g), (Blue, max tb b)
;;

let process_game line =
  let colon_pos =
    match String.index line ':' with
    | None -> 5
    | Some c -> c
  in
  let game_id = String.slice line 5 colon_pos |> int_of_string in
  let results =
    String.slice line (colon_pos + 1) (String.length line)
    |> String.split ~on:';'
    |> List.map ~f:String.strip
    |> List.map ~f:process_result_set
    |> List.fold ~init:((Red, 0), (Green, 0), (Blue, 0)) ~f:fold_game_peaks
  in
  game_id, results
;;

let is_possible ((_, r), (_, g), (_, b)) =
  r <= red_count && g <= green_count && b <= blue_count
;;

let part1 lines =
  let game_ids = List.map lines ~f:process_game in
  let filtered = List.filter game_ids ~f:(fun (_, r) -> is_possible r) in
  List.fold filtered ~init:0 ~f:(fun t (i, _) -> t + i)
;;

let get_power ((_, r), (_, g), (_, b)) = r * g * b

let part2 lines =
  let game_ids = List.map lines ~f:process_game in
  let game_powers = List.map game_ids ~f:(fun (_, c) -> get_power c) in
  List.fold game_powers ~init:0 ~f:( + )
;;

let () =
  let lines = In_channel.input_lines In_channel.stdin in
  let part1 = part1 lines in
  let part2 = part2 lines in
  Out_channel.printf "Part1: %d\n" part1;
  Out_channel.printf "Part2: %d\n" part2
;;
