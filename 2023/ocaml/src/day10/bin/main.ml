open Core

type pipe_piece =
  | Ground
  | Start
  | NorthSouth
  | SouthEast
  | WestEast
  | SouthWest
  | NorthWest
  | NorthEast
  | NotInclosed

let pipe_piece_of_char (c : char) =
  match c with
  | '.' -> Ground
  | 'S' -> Start
  | '|' -> NorthSouth
  | 'F' -> SouthEast
  | '-' -> WestEast
  | '7' -> SouthWest
  | 'J' -> NorthWest
  | 'L' -> NorthEast
  | _ -> failwith "Unknown character"
;;

let char_of_pipe_piece (p : pipe_piece) =
  match p with
  | Ground -> " "
  | Start -> "@"
  | NorthSouth -> "┃"
  | SouthEast -> "┏"
  | WestEast -> "━"
  | SouthWest -> "┓"
  | NorthWest -> "┛"
  | NorthEast -> "┗"
  | NotInclosed -> "█"
;;

let expand_pipe (p : pipe_piece) =
  match p with
  | Ground -> [ Ground; Ground ], [ Ground; Ground ]
  | Start -> failwith "Can't handle Start"
  | NorthSouth -> [ NorthSouth; Ground ], [ NorthSouth; Ground ]
  | SouthEast -> [ SouthEast; WestEast ], [ NorthSouth; Ground ]
  | WestEast -> [ WestEast; WestEast ], [ Ground; Ground ]
  | SouthWest -> [ SouthWest; Ground ], [ NorthSouth; Ground ]
  | NorthWest -> [ NorthWest; Ground ], [ Ground; Ground ]
  | NorthEast -> [ NorthEast; WestEast ], [ Ground; Ground ]
  | NotInclosed -> failwith "Can't handle NotInclosed"
;;

type direction =
  | North
  | East
  | South
  | West

let get_new_pos
  (pipe : pipe_piece)
  ((prev_x, prev_y) : int * int)
  ((curr_x, curr_y) : int * int)
  =
  let (pos1_x, pos1_y), (pos2_x, pos2_y) =
    match pipe with
    | NorthSouth -> (curr_x, curr_y - 1), (curr_x, curr_y + 1)
    | SouthEast -> (curr_x, curr_y + 1), (curr_x + 1, curr_y)
    | WestEast -> (curr_x - 1, curr_y), (curr_x + 1, curr_y)
    | SouthWest -> (curr_x, curr_y + 1), (curr_x - 1, curr_y)
    | NorthWest -> (curr_x, curr_y - 1), (curr_x - 1, curr_y)
    | NorthEast -> (curr_x, curr_y - 1), (curr_x + 1, curr_y)
    | _ -> failwith @@ "No movement for Pipe Piece"
  in
  Out_channel.flush Out_channel.stdout;
  if prev_x = pos1_x && prev_y = pos1_y then pos2_x, pos2_y else pos1_x, pos1_y
;;

let process_input (lines : string list) =
  let process_line (line : string) =
    String.to_array line |> Array.map ~f:pipe_piece_of_char
  in
  List.map lines ~f:process_line |> Array.of_list
;;

let find_start (map : pipe_piece array array) =
  let is_start (p : pipe_piece) =
    match p with
    | Start -> true
    | _ -> false
  in
  let search_row (row : pipe_piece array) =
    match Array.findi row ~f:(fun _ p -> is_start p) with
    | None -> false
    | Some _ -> true
  in
  let y, row = Array.findi_exn map ~f:(fun _ r -> search_row r) in
  let x, _ = Array.findi_exn row ~f:(fun _ p -> is_start p) in
  x, y
;;

let get_piece (map : pipe_piece array array) ((x, y) : int * int) = map.(y).(x)

let get_start_banches (map : pipe_piece array array) ((start_x, start_y) : int * int) =
  let validate_piece (x, y) =
    if x < 0 || y < 0
    then None
    else if y > Array.length map || x > Array.length map.(0)
    then None
    else Some (x, y)
  in
  let translate_pos (pos : (int * int) option) =
    match pos with
    | None -> None
    | Some pos -> Some (get_piece map pos)
  in
  let is_good ((idx, piece) : int * pipe_piece option) =
    match piece with
    | None -> false
    | Some piece ->
      (match idx with
       | 0 ->
         (match piece with
          | NorthSouth | SouthEast | SouthWest -> true
          | _ -> false)
       | 1 ->
         (match piece with
          | WestEast | SouthWest | NorthWest -> true
          | _ -> false)
       | 2 ->
         (match piece with
          | NorthSouth | NorthWest | NorthEast -> true
          | _ -> false)
       | 3 ->
         (match piece with
          | SouthEast | WestEast | NorthEast -> true
          | _ -> false)
       | _ -> false)
  in
  let pieces =
    [ start_x, start_y - 1
    ; start_x + 1, start_y
    ; start_x, start_y + 1
    ; start_x - 1, start_y
    ]
  in
  let map_pieces =
    List.map pieces ~f:validate_piece
    |> List.map ~f:translate_pos
    |> List.zip_exn (List.range ~stop:`inclusive 0 3)
    |> List.filter ~f:is_good
  in
  let (idx0, _), (idx1, _) = List.nth_exn map_pieces 0, List.nth_exn map_pieces 1 in
  List.nth_exn pieces idx0, List.nth_exn pieces idx1
;;

let part1 (map : pipe_piece array array) =
  let rec walk b1_prev b1_curr b2_prev b2_curr d =
    let is_same (x1, y1) (x2, y2) = x1 = x2 && y1 = y2 in
    if is_same b1_curr b2_curr
    then d
    else (
      let get_piece = get_piece map in
      let b1_piece = get_piece b1_curr in
      let b2_piece = get_piece b2_curr in
      let b1_next = get_new_pos b1_piece b1_prev b1_curr in
      let b2_next = get_new_pos b2_piece b2_prev b2_curr in
      walk b1_curr b1_next b2_curr b2_next (d + 1))
  in
  let start = find_start map in
  let branch1, branch2 = get_start_banches map start in
  walk start branch1 start branch2 1
;;

let get_start_pipe_piece
  ((start_x, start_y) : int * int)
  ((b1_x, b1_y) : int * int)
  ((b2_x, b2_y) : int * int)
  =
  let b1_dir =
    if b1_x = start_x && b1_y = start_y - 1
    then North
    else if b1_x = start_x + 1 && b1_y = start_y
    then East
    else if b1_x = start_x && b1_y = start_y + 1
    then South
    else if b1_x = start_x - 1 && b1_y = start_y
    then West
    else failwith "Invalid Start"
  in
  let b2_dir =
    if b2_x = start_x && b2_y = start_y - 1
    then North
    else if b2_x = start_x + 1 && b2_y = start_y
    then East
    else if b2_x = start_x && b2_y = start_y + 1
    then South
    else if b2_x = start_x - 1 && b2_y = start_y
    then West
    else failwith "Invalid Start"
  in
  match b1_dir, b2_dir with
  | North, East -> NorthEast
  | North, South -> NorthSouth
  | North, West -> NorthWest
  | East, South -> SouthEast
  | East, West -> WestEast
  | East, North -> NorthEast
  | South, West -> SouthWest
  | South, North -> NorthSouth
  | South, East -> SouthEast
  | West, North -> NorthEast
  | West, East -> WestEast
  | West, South -> SouthWest
  | _ -> failwith "Invalid Start"
;;

let get_filtered_map (map : pipe_piece array array) =
  let rec walk b1_prev b1_curr b2_prev b2_curr m =
    let is_same (x1, y1) (x2, y2) = x1 = x2 && y1 = y2 in
    if is_same b1_curr b2_curr
    then m @ [ b1_curr; b2_curr ]
    else (
      let get_piece = get_piece map in
      let b1_piece = get_piece b1_curr in
      let b2_piece = get_piece b2_curr in
      let b1_next = get_new_pos b1_piece b1_prev b1_curr in
      let b2_next = get_new_pos b2_piece b2_prev b2_curr in
      walk b1_curr b1_next b2_curr b2_next @@ m @ [ b1_curr; b2_curr ])
  in
  let start_x, start_y = find_start map in
  let branch1, branch2 = get_start_banches map (start_x, start_y) in
  let start_piece = get_start_pipe_piece (start_x, start_y) branch1 branch2 in
  let pipe =
    walk (start_x, start_y) branch1 (start_x, start_y) branch2 [ start_x, start_y ]
  in
  let is_pipe x y =
    match List.find pipe ~f:(fun (dx, dy) -> dx = x && dy = y) with
    | None -> false
    | Some _ -> true
  in
  let filter_row (y : int) (row : pipe_piece array) =
    Array.mapi row ~f:(fun x p -> if is_pipe x y then p else Ground)
  in
  let map = Array.mapi map ~f:filter_row in
  map.(start_y).(start_x) <- start_piece;
  map
;;

(* let show_map m = *)
(*   let show mp = printf "%s" @@ char_of_pipe_piece mp in *)
(*   let show r = *)
(*     Array.iter r ~f:(fun mp -> show mp); *)
(*     printf "\n" *)
(*   in *)
(*   let show = Array.iter ~f:show in *)
(*   show m *)
(* ;; *)

let flood_fill_prep (map : pipe_piece array array) =
  let height = Array.length map in
  let width = Array.length map.(0) in
  let edge (pipe : pipe_piece) =
    match pipe with
    | Ground -> NotInclosed
    | _ -> pipe
  in
  List.range 0 width |> List.iter ~f:(fun x -> map.(0).(x) <- edge map.(0).(x));
  List.range 0 width
  |> List.iter ~f:(fun x -> map.(height - 1).(x) <- edge map.(height - 1).(x));
  List.range 1 (height - 1) |> List.iter ~f:(fun y -> map.(y).(0) <- edge map.(y).(0));
  List.range 1 (height - 1)
  |> List.iter ~f:(fun y -> map.(y).(width - 1) <- edge map.(y).(width - 1));
  map
;;

let compare_map m1 m2 =
  let compare (p1 : pipe_piece) (p2 : pipe_piece) =
    let p1, p2 = char_of_pipe_piece p1, char_of_pipe_piece p2 in
    compare_string p1 p2
  in
  let compare = compare_array compare in
  let compare = compare_array compare in
  compare m1 m2
;;

let flood_fill (map : pipe_piece array array) =
  let rec flood_fill (map : pipe_piece array array) =
    let check p =
      match p with
      | NotInclosed -> true
      | _ -> false
    in
    let fill_piece (y : int) (x : int) (pipe : pipe_piece) =
      match pipe with
      | Ground ->
        if check map.(y - 1).(x)
           || check map.(y).(x - 1)
           || check map.(y + 1).(x)
           || check map.(y).(x + 1)
        then NotInclosed
        else pipe
      | _ -> pipe
    in
    let fill_piece y = Array.mapi ~f:(fill_piece y) in
    let fill_piece = Array.mapi ~f:fill_piece in
    let my_map = fill_piece map in
    if compare_map my_map map = 0 then my_map else flood_fill my_map
  in
  let map = flood_fill_prep map in
  flood_fill map
;;

let expand_map (map : pipe_piece array array) =
  let expand (row : pipe_piece array) =
    let interim = Array.map row ~f:expand_pipe |> List.of_array in
    let row1, row2 = List.unzip interim in
    let row1, row2 = List.join row1 |> Array.of_list, List.join row2 |> Array.of_list in
    [ row1 ] @ [ row2 ]
  in
  let expand (map : pipe_piece array array) =
    Array.map map ~f:expand |> List.of_array |> List.join |> Array.of_list
  in
  expand map
;;

let contract_map (map : pipe_piece array array) =
  let contract (x : int) (pipe : pipe_piece) = if x % 2 = 1 then None else Some pipe in
  let contract (y : int) (row : pipe_piece array) =
    if y % 2 = 1 then None else Some (Array.filter_mapi row ~f:contract)
  in
  let contract (map : pipe_piece array array) = Array.filter_mapi map ~f:contract in
  contract map
;;

let part2 (map : pipe_piece array array) =
  get_filtered_map map
  |> expand_map
  |> flood_fill
  |> contract_map
  |> Array.map ~f:List.of_array
  |> List.of_array
  |> List.join
  |> List.filter ~f:(fun p ->
    match p with
    | Ground -> true
    | _ -> false)
  |> List.length
;;

let () =
  let input = In_channel.read_lines "../input-sample3.txt" |> process_input in
  printf "Sample:\n";
  printf "Part1: %d\n" @@ part1 input;
  printf "Part2: %d\n" @@ part2 input;
  Out_channel.flush Out_channel.stdout;
  let input = In_channel.read_lines "../input.txt" |> process_input in
  printf "\nActual:\n";
  printf "Part1: %d\n" @@ part1 input;
  printf "Part2: %d\n" @@ part2 input;
  Out_channel.flush Out_channel.stdout
;;
