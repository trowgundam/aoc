open Core

let process_input input =
  String.split_lines input
  |> List.map ~f:(fun l ->
    String.to_array l |> Array.map ~f:(fun c -> String.of_char c |> int_of_string))
  |> Array.of_list
;;

module Path = struct
  module T = struct
    type t = (int * int) * (int * (int * int)) list * int

    let compare_pos = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
    let compare_hist = List.compare (Tuple2.compare ~cmp1:Int.compare ~cmp2:compare_pos)
    let compare = Tuple3.compare ~cmp1:compare_pos ~cmp2:compare_hist ~cmp3:Int.compare
    let sexp_of_t_pos = Tuple2.sexp_of_t Int.sexp_of_t Int.sexp_of_t
    let sexp_of_t_hist = List.sexp_of_t (Tuple2.sexp_of_t Int.sexp_of_t sexp_of_t_pos)
    let sexp_of_t = Tuple3.sexp_of_t sexp_of_t_pos sexp_of_t_hist Int.sexp_of_t
    let t_of_sexp_pos = Tuple2.t_of_sexp Int.t_of_sexp Int.t_of_sexp
    let t_of_sexp_hist = List.t_of_sexp (Tuple2.t_of_sexp Int.t_of_sexp t_of_sexp_pos)
    let t_of_sexp = Tuple3.t_of_sexp t_of_sexp_pos t_of_sexp_hist Int.t_of_sexp
    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

module Seen = struct
  module T = struct
    type t = (int * int) * int * int

    let compare =
      Tuple3.compare
        ~cmp1:(Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare)
        ~cmp2:Int.compare
        ~cmp3:Int.compare
    ;;

    let sexp_of_t =
      Tuple3.sexp_of_t
        (Tuple2.sexp_of_t Int.sexp_of_t Int.sexp_of_t)
        Int.sexp_of_t
        Int.sexp_of_t
    ;;

    let t_of_sexp =
      Tuple3.t_of_sexp
        (Tuple2.t_of_sexp Int.t_of_sexp Int.t_of_sexp)
        Int.t_of_sexp
        Int.t_of_sexp
    ;;

    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

let solve (map : int array array) can_move get_moves is_at_end =
  let height = Array.length map in
  let width = Array.length map.(0) in
  (* let dp = *)
  (*   List.range 0 height *)
  (*   |> List.map ~f:(fun _ -> *)
  (*     List.range 0 width *)
  (*     |> List.map ~f:(fun _ -> Array.create ~len:4 Int.max_value) *)
  (*     |> Array.of_list) *)
  (*   |> Array.of_list *)
  (* in *)
  (* dp.(0).(0).(0) <- Int.min_value; *)
  (* dp.(0).(0).(1) <- Int.max_value; *)
  (* dp.(0).(0).(2) <- Int.max_value; *)
  (* dp.(0).(0).(3) <- Int.min_value; *)
  (* let last ~count lst = *)
  (*   let ll = List.length lst in *)
  (*   if ll <= count then lst else List.drop lst (ll - count) *)
  (* in *)
  let seen = Hash_set.create (module Seen) in
  let queue = Hash_set.create (module Path) in
  Hash_set.add queue ((0, 0), [], 0);
  let should_stop () =
    let items = Hash_set.filter queue ~f:is_at_end in
    match
      Hash_set.min_elt items ~compare:(fun (_, _, h1) (_, _, h2) -> Int.compare h1 h2)
    with
    | None -> true
    | Some _ -> false
  in
  while (not @@ Hash_set.is_empty queue) && should_stop () do
    let _, _, h =
      match
        Hash_set.min_elt queue ~compare:(fun (_, _, h1) (_, _, h2) -> Int.compare h1 h2)
      with
      | None -> failwith "Impossible"
      | Some m -> m
    in
    let min = Hash_set.filter queue ~f:(fun (_, _, hq) -> hq = h) in
    let min =
      match
        Hash_set.min_elt min ~compare:(fun ((x1, y1), _, _) ((x2, y2), _, _) ->
          Int.compare (width - x1 + height - y1) (width - x2 + height - y2))
      with
      | None -> failwith "Impossible"
      | Some m -> m
    in
    Hash_set.remove queue min;
    let (x, y), history, heat = min in
    get_moves x y
    |> List.filter ~f:(fun (d, p) -> can_move p d history)
    |> List.iter ~f:(fun (d, (nx, ny)) ->
      let new_heat = heat + map.(ny).(nx) in
      (* if new_heat < dp.(ny).(nx).(d) then dp.(ny).(nx).(d) <- new_heat else (); *)
      let move_dir =
        List.rev history |> List.take_while ~f:(fun (dd, _) -> dd = d) |> List.length
      in
      let move_dir = move_dir + 1 in
      if not @@ Hash_set.mem seen ((nx, ny), d, move_dir)
      then (
        (* sprintf *)
        (*   "(%d, %d) dist = %d; heat = %d" *)
        (*   nx *)
        (*   ny *)
        (*   (width - nx - 1 + height - ny - 1) *)
        (*   new_heat *)
        (* |> print_endline; *)
        Hash_set.add queue ((nx, ny), history @ [ d, (nx, ny) ], new_heat);
        Hash_set.add seen ((nx, ny), d, move_dir))
      else ())
  done;
  let finished = Hash_set.filter queue ~f:is_at_end in
  let finished =
    Hash_set.min_elt finished ~compare:(fun (_, _, h1) (_, _, h2) -> Int.compare h1 h2)
  in
  match finished with
  | None -> failwith "No result"
  | Some (_, _, h) ->
    (* | Some (_, hist, h) -> *)
    (* let hist = fst (List.unzip hist) in *)
    (* let hist = *)
    (*   List.group hist ~break:( <> ) *)
    (*   |> List.map ~f:(fun d -> sprintf "d %d - c %d" (List.hd_exn d) (List.length d)) *)
    (*   |> String.concat ~sep:"; " *)
    (*   |> sprintf "[ %s ]" *)
    (* in *)
    (* printf "Heat %d - Path %s\n" h hist; *)
    h
;;

let part1 (map : int array array) =
  let height = Array.length map in
  let width = Array.length map.(0) in
  let is_inbounds (x, y) =
    Int.between x ~low:0 ~high:(width - 1) && Int.between y ~low:0 ~high:(height - 1)
  in
  let can_move (x, y) d (hist : (int * (int * int)) list) =
    let is_inbounds = is_inbounds (x, y) in
    let not_walked =
      not
      @@ List.mem
           hist
           (d, (x, y))
           ~equal:(fun (_, (x1, y1)) (_, (x2, y2)) ->
             Tuple2.equal ~eq1:Int.equal ~eq2:Int.equal (x1, y1) (x2, y2))
    in
    let ds = List.rev hist |> List.take_while ~f:(fun (dd, _) -> d = dd) |> List.length in
    let ds = ds + 1 in
    let three_d = ds <= 3 in
    is_inbounds && not_walked && three_d
  in
  let get_moves x y = [ 0, (x, y - 1); 1, (x + 1, y); 2, (x, y + 1); 3, (x - 1, y) ] in
  let is_at_end ((x, y), _, _) = x = width - 1 && y = height - 1 in
  solve map can_move get_moves is_at_end
;;

let part2 (map : int array array) =
  let height = Array.length map in
  let width = Array.length map.(0) in
  let is_inbounds (x, y) =
    Int.between x ~low:0 ~high:(width - 1) && Int.between y ~low:0 ~high:(height - 1)
  in
  let can_move (x, y) d (hist : (int * (int * int)) list) =
    let is_inbounds = is_inbounds (x, y) in
    let not_walked =
      not
      @@ List.mem
           hist
           (d, (x, y))
           ~equal:(fun (_, (x1, y1)) (_, (x2, y2)) ->
             Tuple2.equal ~eq1:Int.equal ~eq2:Int.equal (x1, y1) (x2, y2))
    in
    (* let hds = fst (List.unzip hist) in *)
    let ds = fst (List.take (List.rev hist) 10 |> List.unzip) in
    (* printf *)
    (*   "%s\n" *)
    (*   (List.map ds ~f:string_of_int |> String.concat ~sep:"; " |> sprintf "[ %s ]"); *)
    let ds = List.group ds ~break:( <> ) in
    (* printf *)
    (*   "%s\n" *)
    (*   (List.map ds ~f:(fun sl -> *)
    (*      List.map sl ~f:string_of_int |> String.concat ~sep:"; " |> sprintf "[ %s ]") *)
    (*    |> String.concat ~sep:"; " *)
    (*    |> sprintf "[ %s ]"); *)
    let ds = List.hd ds in
    let can_move =
      match ds with
      | None -> true
      | Some c ->
        let cl = List.length c in
        if List.mem c d ~equal:Int.equal
        then
          if x = width - 1 && y = height - 1
          then Int.between ~low:4 ~high:9 cl
          else cl < 10
        else Int.between ~low:4 ~high:10 cl
    in
    (* let cd = *)
    (*   match ds with *)
    (*   | None -> "''" *)
    (*   | Some cd -> List.hd_exn cd |> string_of_int *)
    (* in *)
    (* sprintf *)
    (*   "(%d, %d) d %d dsl %s ds %d" *)
    (*   x *)
    (*   y *)
    (*   d *)
    (*   cd *)
    (*   (List.length (Option.value_or_thunk ds ~default:(fun () -> []))) *)
    (* |> print_endline; *)
    (* sprintf *)
    (*   "is_inbounds = %b - not_walked = %b - can_move = %b" *)
    (*   is_inbounds *)
    (*   not_walked *)
    (*   can_move *)
    (* |> print_endline; *)
    is_inbounds && not_walked && can_move
  in
  let get_moves x y = [ 0, (x, y - 1); 1, (x + 1, y); 2, (x, y + 1); 3, (x - 1, y) ] in
  let is_at_end ((x, y), hist, _) =
    if x = width - 1 && y = height - 1
    then (
      let hist = fst (List.unzip hist) in
      let hist = List.group hist ~break:( <> ) in
      let hist = List.map hist ~f:List.length in
      let hist = List.filter hist ~f:(fun l -> not @@ Int.between ~low:4 ~high:10 l) in
      List.length hist = 0)
    else false
  in
  solve map can_move get_moves is_at_end
;;

let () =
  print_endline "Sample:";
  let map = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 map |> sprintf "Part 1: %d" |> print_endline;
  part2 map |> sprintf "Part 2: %d" |> print_endline;
  print_endline "";
  print_endline "Actual:";
  let map = In_channel.read_all "../input.txt" |> process_input in
  part1 map |> sprintf "Part 1: %d" |> print_endline;
  part2 map |> sprintf "Part 2: %d" |> print_endline
;;
