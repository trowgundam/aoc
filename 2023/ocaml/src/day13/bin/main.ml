open Core

let partition l ~on =
  let rec partition' rst l =
    match List.split_while l ~f:(fun l -> not @@ on l) with
    | [], [] -> rst
    | [], _ :: tl -> partition' rst tl
    | hd, [] -> rst @ [ hd ]
    | hd, _ :: tl -> partition' (rst @ [ hd ]) tl
  in
  partition' [] l
;;

let process_input input =
  let convert c =
    match c with
    | '.' -> 0
    | '#' -> 1
    | _ -> failwith @@ sprintf "Invalid char: %c" c
  in
  partition input ~on:String.is_empty
  |> List.map ~f:(List.map ~f:(fun l -> String.to_list l |> List.map ~f:convert))
;;

(* let print_pattern (pattern : int list list) = *)
(*   let convert c = *)
(*     match c with *)
(*     | 0 -> '.' *)
(*     | 1 -> '$' *)
(*     | _ -> ' ' *)
(*   in *)
(*   let string_of_int_list l = List.map l ~f:convert |> String.of_char_list in *)
(*   List.map pattern ~f:string_of_int_list |> List.iter ~f:print_endline *)
(* ;; *)

let compare_pattern = compare_list (compare_list compare_int)

module Pattern = struct
  module T = struct
    type t = int list list * int list list

    let compare = compare_list @@ compare_list compare_int
    let compare = Tuple2.compare ~cmp1:compare ~cmp2:compare
    let sexp_of_t = List.sexp_of_t @@ List.sexp_of_t Int.sexp_of_t
    let sexp_of_t = Tuple2.sexp_of_t sexp_of_t sexp_of_t
    let t_of_sexp = List.t_of_sexp @@ List.t_of_sexp Int.t_of_sexp
    let t_of_sexp = Tuple2.t_of_sexp t_of_sexp t_of_sexp
    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

let solve ht (pat : int list list) (ir, ic) =
  let compare l1 l2 =
    let l1_len = List.length l1 in
    let l2_len = List.length l2 in
    let len = min l1_len l2_len in
    let l1, l2 = List.take (List.rev l1) len, List.take l2 len in
    match Hashtbl.find ht (l1, l2) with
    | Some r -> r
    | None ->
      let result =
        if l1_len = 0 then -1 else if l2_len = 0 then 1 else compare_pattern l1 l2
      in
      let _ = Hashtbl.add ht ~key:(l1, l2) ~data:result in
      result
  in
  let rec solve' (r_top, r_btm) ig =
    let tp_len = List.length r_top in
    if compare r_top r_btm = 0 && tp_len <> ig
    then tp_len
    else (
      match r_btm with
      | [] -> 0
      | hd :: tl -> solve' (r_top @ [ hd ], tl) ig)
  in
  let rows = solve' (List.split_n pat 1) ir in
  let cols = solve' (List.split_n (List.transpose_exn pat) 1) ic in
  rows, cols
;;

let part1 input =
  List.map input ~f:(fun p ->
    let ht = Hashtbl.create ~size:0 (module Pattern) in
    solve ht p (0, 0))
  |> List.fold ~init:0 ~f:(fun a (r, c) -> a + (r * 100) + c)
;;

let part2 input =
  List.map input ~f:(fun p ->
    let not v = if v = 0 then 1 else 0 in
    let ht = Hashtbl.create ~size:0 (module Pattern) in
    let old_r, old_c = solve ht p (0, 0) in
    let row_length = List.length (List.hd_exn p) in
    let col_height = List.length p in
    let variations = col_height * row_length in
    let variations =
      Sequence.range 0 variations
      |> Sequence.map ~f:(fun v ->
        List.mapi p ~f:(fun y r ->
          List.mapi r ~f:(fun x c ->
            if v / row_length = y && v % row_length = x then not c else c)))
    in
    Sequence.fold_until
      variations
      ~init:0
      ~f:(fun a p ->
        let r, c = solve ht p (old_r, old_c) in
        let result = (r * 100) + c in
        if result > 0 then Stop result else Continue a)
      ~finish:(fun v -> v))
  |> List.fold ~init:0 ~f:( + )
;;

let () =
  print_endline "Sample";
  let input = In_channel.read_lines "../input-sample.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n";
  print_endline "\nActual:";
  let input = In_channel.read_lines "../input.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n"
;;
