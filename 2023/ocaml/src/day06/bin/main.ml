open Core

let get_ways (t, d) =
  Sequence.range 1 t
  |> Sequence.map ~f:(fun dt -> dt * (t - dt))
  |> Sequence.filter ~f:(fun dd -> dd > d)
  |> Sequence.length
;;

let part1 lines =
  let process_lines lines =
    let process_line line =
      match
        String.split_on_chars line ~on:[ ' ' ]
        |> List.filter ~f:(fun s -> not @@ String.is_empty s)
        |> List.tl
      with
      | Some t -> List.map t ~f:int_of_string
      | None -> failwith "Invalid line"
    in
    match lines with
    | time :: distance :: _ -> List.zip_exn (process_line time) (process_line distance)
    | _ -> failwith "Invalid Input"
  in
  process_lines lines |> List.map ~f:get_ways |> List.fold ~init:1 ~f:( * )
;;

let part2 lines =
  let process_lines lines =
    let process_line line =
      match
        String.filter line ~f:(fun c -> not @@ Char.equal ' ' c)
        |> String.split_on_chars ~on:[ ':' ]
      with
      | _ :: v :: _ -> int_of_string v
      | _ -> failwith "Invalid line"
    in
    match lines with
    | time :: distance :: _ -> process_line time, process_line distance
    | _ -> failwith "Invalid Input"
  in
  get_ways @@ process_lines lines
;;

let () =
  let sample = In_channel.read_lines "../input-sample.txt" in
  let actual = In_channel.read_lines "../input.txt" in
  printf "Sample:\n";
  part1 sample |> printf "Part1: %d\n";
  part2 sample |> printf "Part2: %d\n";
  printf "\nActual:\n";
  part1 actual |> printf "Part1: %d\n";
  part2 actual |> printf "Part2: %d\n"
;;
