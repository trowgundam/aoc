open Core

type coord =
  { x : int
  ; y : int
  ; z : int
  }

module Coord = struct
  module T = struct
    type t = coord

    let t_of_tuple3 (z, x, y) = { z; x; y }
    let tuple3_of_t t = t.z, t.x, t.y

    let compare t1 t2 =
      Tuple3.compare
        ~cmp1:Int.compare
        ~cmp2:Int.compare
        ~cmp3:Int.compare
        (tuple3_of_t t1)
        (tuple3_of_t t2)
    ;;

    let sexp_of_t t =
      Tuple3.sexp_of_t Int.sexp_of_t Int.sexp_of_t Int.sexp_of_t (tuple3_of_t t)
    ;;

    let t_of_sexp s =
      Tuple3.t_of_sexp Int.t_of_sexp Int.t_of_sexp Int.t_of_sexp s |> t_of_tuple3
    ;;
  end

  include T
  include Comparable.Make (T)
end

let sprint_coord c = sprintf "(%d, %d, %d)" c.x c.y c.z

module Block = struct
  module T = struct
    type t = coord * coord

    let compare = Tuple2.compare ~cmp1:Coord.compare ~cmp2:Coord.compare
    let sexp_of_t = Tuple2.sexp_of_t Coord.sexp_of_t Coord.sexp_of_t
    let t_of_sexp = Tuple2.t_of_sexp Coord.t_of_sexp Coord.t_of_sexp

    let get_coordinates (block : t) =
      let c1, c2 = block in
      let x_seq = Sequence.range ~stride:1 ~start:`inclusive ~stop:`inclusive c1.x c2.x in
      let y_seq = Sequence.range ~stride:1 ~start:`inclusive ~stop:`inclusive c1.y c2.y in
      let z_seq = Sequence.range ~stride:1 ~start:`inclusive ~stop:`inclusive c1.z c2.z in
      Sequence.map x_seq ~f:(fun x ->
        Sequence.map y_seq ~f:(fun y -> Sequence.map z_seq ~f:(fun z -> { x; y; z }))
        |> Sequence.join)
      |> Sequence.join
    ;;
  end

  include T
  include Comparable.Make (T)
end

let process_input input =
  let process_line line =
    let process_coord coord =
      let data = String.split coord ~on:',' |> List.map ~f:int_of_string in
      { x = List.nth_exn data 0; y = List.nth_exn data 1; z = List.nth_exn data 2 }
    in
    let data =
      String.split line ~on:'~'
      |> List.map ~f:process_coord
      |> List.sort ~compare:Coord.compare
    in
    List.nth_exn data 0, List.nth_exn data 1
  in
  String.split_lines input |> List.map ~f:process_line
;;

let intersection l1 l2 equal = Sequence.filter l1 ~f:(Sequence.mem l2 ~equal)
let blocks_intersect b1 b2 = not @@ Sequence.is_empty @@ intersection b1 b2 Coord.equal

let can_fall input =
  let input =
    List.sort input ~compare:Block.compare
    |> List.filter ~f:(fun (bs, _) -> bs.z > 1)
    |> List.map ~f:(fun i -> i, Block.get_coordinates i)
  in
  List.fold_until
    input
    ~init:None
    ~finish:(fun b -> b)
    ~f:(fun _ (b, _) ->
      let bs1, be1 = b in
      let filtered = List.filter input ~f:(fun ((bs2, _), _) -> bs2.z < bs1.z) in
      let bs1, be1 =
        { x = bs1.x; y = bs1.y; z = bs1.z - 1 }, { x = be1.x; y = be1.y; z = be1.z - 1 }
      in
      let c = Block.get_coordinates (bs1, be1) in
      match
        List.fold_until
          filtered
          ~init:None
          ~finish:(fun f -> f)
          ~f:(fun _ (_, c2) ->
            let intersection = intersection c c2 Coord.equal in
            if Sequence.length intersection = 0 then Stop (Some b) else Continue None)
      with
      | None -> Continue None
      | Some b -> Stop (Some b))
;;

let rec fall input =
  let input = List.sort input ~compare:Block.compare in
  match can_fall input with
  | None -> input
  | Some block ->
    let bs, be = block in
    let filtered = List.filter input ~f:(fun b -> not @@ Block.equal block b) in
    let vert = bs.z <> be.z in
    if vert
    then (
      printf "Found vertical %s-%s\n" (sprint_coord bs) (sprint_coord be);
      List.iter filtered ~f:(fun (bs, be) ->
        printf "%s-%s\n" (sprint_coord bs) (sprint_coord be)))
    else ();
    let new_block =
      List.range 1 bs.z
      |> List.fold_until
           ~init:block
           ~finish:(fun b -> b)
           ~f:(fun acc z ->
             let filtered =
               List.filter filtered ~f:(fun (bs2, _) -> bs2.z < z)
               |> List.map ~f:Block.get_coordinates
             in
             let new_block =
               { x = bs.x; y = bs.y; z = bs.z - z }, { x = be.x; y = be.y; z = be.z - z }
             in
             let ns, ne = new_block in
             if vert
             then printf "Checking New %s-%s\n" (sprint_coord ns) (sprint_coord ne)
             else ();
             if List.mem
                  filtered
                  (Block.get_coordinates new_block)
                  ~equal:blocks_intersect
             then Stop acc
             else Continue new_block)
    in
    fall (new_block :: filtered)
;;

let part1 input =
  print_endline "Before Fall:";
  List.iter input ~f:(fun (bs, be) ->
    printf "%s-%s\n" (sprint_coord bs) (sprint_coord be);
    Block.get_coordinates (bs, be)
    |> Sequence.iter ~f:(fun c -> print_endline (sprint_coord c)));
  print_endline "";
  print_endline "After Fall:";
  let input = fall input in
  List.iter input ~f:(fun (bs, be) ->
    printf "%s-%s\n" (sprint_coord bs) (sprint_coord be);
    Block.get_coordinates (bs, be)
    |> Sequence.iter ~f:(fun c -> print_endline (sprint_coord c)));
  let last_s, last_e = List.last_exn input in
  let last_c = Block.get_coordinates (last_s, last_e) in
  printf "last %s-%s\n" (sprint_coord last_s) (sprint_coord last_e);
  List.drop_last_exn input
  |> List.iter ~f:(fun b ->
    let bs, be = b in
    let b_c = Block.get_coordinates b in
    printf "%s-%s %b\n" (sprint_coord bs) (sprint_coord be) (blocks_intersect last_c b_c));
  (* List.iter input ~f:(fun (bs, be) -> *)
  (*   printf "(%d, %d, %d)-(%d, %d, %d)\n" bs.x bs.y bs.z be.x be.y be.z); *)
  0
;;

let () =
  (* let seq1, seq2 = Sequence.range 0 5, Sequence.range 4 10 in *)
  (* Sequence.iter seq1 ~f:(printf "%d\n"); *)
  (* Sequence.iter seq2 ~f:(printf "%d\n"); *)
  (* printf "%b\n" (not @@ Sequence.is_empty @@ intersection seq1 seq2 Int.equal); *)
  print_endline "Sample:";
  let input = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 input |> sprintf "Part 1: %d" |> print_endline
;;
