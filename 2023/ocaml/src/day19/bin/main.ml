open Core

type part_attribute =
  | Extreme
  | Musical
  | Aerodynamic
  | Shiny
  | None

let part_attribute_of_char c =
  match c with
  | 'x' -> Extreme
  | 'm' -> Musical
  | 'a' -> Aerodynamic
  | 's' -> Shiny
  | _ -> failwith @@ sprintf "Invalid Char: %c" c
;;

type rule_type =
  | LessThan of int
  | GreaterThan of int
  | Default

type result =
  | Accept
  | Reject
  | Rule of string

type rule =
  { attribute : part_attribute
  ; rule_type : rule_type
  ; result : result
  }

type part =
  { extreme : int
  ; musical : int
  ; aerodynamic : int
  ; shiny : int
  }

let process_input input =
  let process_rule line =
    let get_rule rule =
      let get_result v =
        if String.equal v "A"
        then Accept
        else if String.equal v "R"
        then Reject
        else Rule v
      in
      let rule_parts = String.split rule ~on:':' in
      if List.length rule_parts = 1
      then { attribute = None; rule_type = Default; result = get_result rule }
      else (
        let result = get_result @@ List.nth_exn rule_parts 1 in
        let rule = List.hd_exn rule_parts in
        let attribute = part_attribute_of_char @@ String.nget rule 0 in
        let rule_type =
          match String.nget rule 1 with
          | '<' -> LessThan (int_of_string @@ String.drop_prefix rule 2)
          | '>' -> GreaterThan (int_of_string @@ String.drop_prefix rule 2)
          | c -> failwith @@ sprintf "Invalid Character: %c" c
        in
        { attribute; rule_type; result })
    in
    let name = String.take_while line ~f:(fun c -> not @@ Char.equal '{' c) in
    let rules = String.drop_prefix line (String.length name + 1) in
    let rules = String.drop_suffix rules 1 in
    let rules = String.split rules ~on:',' |> List.map ~f:get_rule in
    name, rules
  in
  let process_part line =
    let line = String.drop_prefix (String.drop_suffix line 1) 1 in
    let data = String.split line ~on:',' in
    let data = List.map data ~f:(fun d -> String.drop_prefix d 2) in
    let data = List.map data ~f:int_of_string in
    { extreme = List.nth_exn data 0
    ; musical = List.nth_exn data 1
    ; aerodynamic = List.nth_exn data 2
    ; shiny = List.nth_exn data 3
    }
  in
  let input = String.split_lines input in
  let rules, parts = List.split_while input ~f:(fun l -> not @@ String.is_empty l) in
  let rules = List.map rules ~f:process_rule in
  let rules =
    match
      Hashtbl.of_alist
        ~growth_allowed:false
        ~size:(List.length rules)
        (module String)
        rules
    with
    | `Ok rt -> rt
    | _ -> failwith "Invalid Rules Set"
  in
  let parts = List.tl_exn parts in
  let parts = List.map parts ~f:process_part in
  rules, parts
;;

let rec process_part rules rule part =
  let rule_list =
    match Hashtbl.find rules rule with
    | None -> failwith @@ sprintf "Invalid Rule: %s" rule
    | Some r -> r
  in
  let get_attribute a =
    match a with
    | Extreme -> part.extreme
    | Musical -> part.musical
    | Aerodynamic -> part.aerodynamic
    | Shiny -> part.shiny
    | _ -> failwith "Invalid Part Attribute"
  in
  let result =
    List.fold_until
      rule_list
      ~init:Reject
      ~f:(fun a r ->
        match r.rule_type with
        | GreaterThan v ->
          if get_attribute r.attribute > v then Stop r.result else Continue a
        | LessThan v ->
          if get_attribute r.attribute < v then Stop r.result else Continue a
        | Default -> Stop r.result)
      ~finish:(fun a -> a)
  in
  match result with
  | Rule r -> process_part rules r part
  | r -> r
;;

let part1 input =
  let rules, parts = input in
  let results =
    List.map parts ~f:(fun p -> process_part rules "in" p, p)
    |> List.filter ~f:(fun (r, _) ->
      match r with
      | Accept -> true
      | _ -> false)
  in
  let accepted_parts = snd @@ List.unzip results in
  List.map accepted_parts ~f:(fun p -> p.extreme + p.musical + p.aerodynamic + p.shiny)
  |> List.fold ~init:0 ~f:( + )
;;

let part2 ((rules, _) : (string, rule list) Base.Hashtbl.t * part list) =
  let gt s e v =
    if Int.between v ~low:s ~high:e
    then v + 1, e, s, v
    else if v < s
    then s, e, 0, 0
    else 0, 0, s, e
  in
  let lt s e v =
    if Int.between v ~low:s ~high:e
    then s, v - 1, v, e
    else if v > s
    then s, e, 0, 0
    else 0, 0, s, e
  in
  let rec evaluate_rules sets accepted =
    let sets =
      List.map sets ~f:(fun (rule, ranges) ->
        let rule =
          match Hashtbl.find rules rule with
          | None -> failwith @@ sprintf "Invalid Rule: %s" rule
          | Some r -> r
        in
        List.folding_map
          rule
          ~init:[ "", ranges ]
          ~f:(fun acc rule ->
            let _, ((xs, xe), (ms, me), (ads, ade), (ss, se)) = List.hd_exn acc in
            let set, remaining =
              match rule.rule_type with
              | GreaterThan v ->
                (match rule.attribute with
                 | Extreme ->
                   let xs1, xe1, xs2, xe2 = gt xs xe v in
                   ( ((xs1, xe1), (ms, me), (ads, ade), (ss, se))
                   , ((xs2, xe2), (ms, me), (ads, ade), (ss, se)) )
                 | Musical ->
                   let ms1, me1, ms2, me2 = gt ms me v in
                   ( ((xs, xe), (ms1, me1), (ads, ade), (ss, se))
                   , ((xs, xe), (ms2, me2), (ads, ade), (ss, se)) )
                 | Aerodynamic ->
                   let as1, ae1, as2, ae2 = gt ads ade v in
                   ( ((xs, xe), (ms, me), (as1, ae1), (ss, se))
                   , ((xs, xe), (ms, me), (as2, ae2), (ss, se)) )
                 | Shiny ->
                   let ss1, se1, ss2, se2 = gt ss se v in
                   ( ((xs, xe), (ms, me), (ads, ade), (ss1, se1))
                   , ((xs, xe), (ms, me), (ads, ade), (ss2, se2)) )
                 | None -> failwith "Invalid Attribute")
              | LessThan v ->
                (match rule.attribute with
                 | Extreme ->
                   let xs1, xe1, xs2, xe2 = lt xs xe v in
                   ( ((xs1, xe1), (ms, me), (ads, ade), (ss, se))
                   , ((xs2, xe2), (ms, me), (ads, ade), (ss, se)) )
                 | Musical ->
                   let ms1, me1, ms2, me2 = lt ms me v in
                   ( ((xs, xe), (ms1, me1), (ads, ade), (ss, se))
                   , ((xs, xe), (ms2, me2), (ads, ade), (ss, se)) )
                 | Aerodynamic ->
                   let as1, ae1, as2, ae2 = lt ads ade v in
                   ( ((xs, xe), (ms, me), (as1, ae1), (ss, se))
                   , ((xs, xe), (ms, me), (as2, ae2), (ss, se)) )
                 | Shiny ->
                   let ss1, se1, ss2, se2 = lt ss se v in
                   ( ((xs, xe), (ms, me), (ads, ade), (ss1, se1))
                   , ((xs, xe), (ms, me), (ads, ade), (ss2, se2)) )
                 | None -> failwith "Invalid Attribute")
              | Default ->
                ( ((xs, xe), (ms, me), (ads, ade), (ss, se))
                , ((0, 0), (0, 0), (0, 0), (0, 0)) )
            in
            ("", remaining) :: List.drop acc 1, (rule.result, set)))
    in
    let sets = List.join sets in
    let sets =
      List.filter sets ~f:(fun (_, ((xs, xe), (ms, me), (ads, ade), (ss, se))) ->
        if (xs = 0 && xe = 0)
           || (ms = 0 && me = 0)
           || (ads = 0 && ade = 0)
           || (ss = 0 && se = 0)
        then false
        else true)
    in
    let sets, a, _ =
      List.partition3_map sets ~f:(fun (r, s) ->
        match r with
        | Accept -> `Snd s
        | Reject -> `Trd s
        | Rule r -> `Fst (r, s))
    in
    if List.is_empty sets then accepted @ a else evaluate_rules sets (accepted @ a)
  in
  let accepted_sets =
    evaluate_rules [ "in", ((1, 4000), (1, 4000), (1, 4000), (1, 4000)) ] []
  in
  List.map accepted_sets ~f:(fun ((s1, e1), (s2, e2), (s3, e3), (s4, e4)) ->
    (e1 - s1 + 1) * (e2 - s2 + 1) * (e3 - s3 + 1) * (e4 - s4 + 1))
  |> List.fold ~init:0 ~f:( + )
;;

let () =
  print_endline "Sample:";
  let input = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 input |> sprintf "Part 1: %d" |> print_endline;
  part2 input |> sprintf "Part 2: %d" |> print_endline;
  print_endline "\nActual:";
  let input = In_channel.read_all "../input.txt" |> process_input in
  part1 input |> sprintf "Part 1: %d" |> print_endline;
  part2 input |> sprintf "Part 2: %d" |> print_endline
;;
