open Core

type cell =
  | Empty
  | VertSplitter
  | HorzSplitter
  | ForwardMirror
  | BackwardMirror

type direction =
  | North
  | East
  | South
  | West

let int_of_direction d =
  match d with
  | North -> 0
  | East -> 1
  | South -> 2
  | West -> 3
;;

(* let string_of_direction d = *)
(*   match d with *)
(*   | North -> "N" *)
(*   | East -> "E" *)
(*   | South -> "S" *)
(*   | West -> "W" *)
(* ;; *)

let direction_of_int d =
  match d with
  | 0 -> North
  | 1 -> East
  | 2 -> South
  | 3 -> West
  | _ -> failwith @@ sprintf "Invalid Value: %d" d
;;

let get_direction_change (dir : direction) =
  match dir with
  | North -> 0, -1
  | East -> 1, 0
  | South -> 0, 1
  | West -> -1, 0
;;

type ray =
  { pos : int * int
  ; direction : direction
  }

(* let string_of_ray ray = *)
(*   let x, y = ray.pos in *)
(*   string_of_direction ray.direction |> sprintf "{ (%d, %d); %s}" x y *)
(* ;; *)

module Ray = struct
  module T = struct
    type t = ray

    let destructure r = r.direction, r.pos
    let structure (dir, pos) = { direction = dir; pos }
    let compare_pos = Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare
    let compare_dir d1 d2 = Int.compare (int_of_direction d1) (int_of_direction d2)

    let compare r1 r2 =
      let r1, r2 = destructure r1, destructure r2 in
      Tuple2.compare ~cmp1:compare_dir ~cmp2:compare_pos r1 r2
    ;;

    let sexp_of_t_pos = Tuple2.sexp_of_t Int.sexp_of_t Int.sexp_of_t
    let sexp_of_t_dir d = int_of_direction d |> Int.sexp_of_t

    let sexp_of_t r =
      let d = destructure r in
      Tuple2.sexp_of_t sexp_of_t_dir sexp_of_t_pos d
    ;;

    let t_of_sexp_pos = Tuple2.t_of_sexp Int.t_of_sexp Int.t_of_sexp
    let t_of_sexp_dir d = Int.t_of_sexp d |> direction_of_int
    let t_of_sexp d = Tuple2.t_of_sexp t_of_sexp_dir t_of_sexp_pos d |> structure
    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

let process_input (input : string) =
  String.split_lines input
  |> List.map ~f:(fun s ->
    String.to_array s
    |> Array.map ~f:(fun c ->
      match c with
      | '.' -> Empty
      | '|' -> VertSplitter
      | '-' -> HorzSplitter
      | '/' -> ForwardMirror
      | '\\' -> BackwardMirror
      | _ -> failwith @@ sprintf "Invalid Character: %c" c))
  |> Array.of_list
;;

(* let print_result (result : bool array array) = *)
(*   Array.iter result ~f:(fun r -> *)
(*     Array.iter r ~f:(fun b -> printf "%c" (if b then '#' else '.')); *)
(*     printf "\n") *)
(* ;; *)

let cast (map : cell array array) (start : ray) =
  let height = Array.length map in
  let width = Array.length map.(0) in
  let cache = Hash_set.create (module Ray) in
  Hash_set.add cache start;
  let rec cast' (rays : ray list) (gen : int) =
    let cast'' (ray : ray) =
      let next (x, y) dir =
        let dx, dy = get_direction_change dir in
        x + dx, y + dy
      in
      let x, y = ray.pos in
      match map.(y).(x) with
      | Empty -> [ { direction = ray.direction; pos = next ray.pos ray.direction } ]
      | VertSplitter ->
        (match ray.direction with
         | West | East ->
           [ { direction = North; pos = next ray.pos North }
           ; { direction = South; pos = next ray.pos South }
           ]
         | _ -> [ { direction = ray.direction; pos = next ray.pos ray.direction } ])
      | HorzSplitter ->
        (match ray.direction with
         | North | South ->
           [ { direction = East; pos = next ray.pos East }
           ; { direction = West; pos = next ray.pos West }
           ]
         | _ -> [ { direction = ray.direction; pos = next ray.pos ray.direction } ])
      | ForwardMirror ->
        (match ray.direction with
         | North -> [ { direction = East; pos = next ray.pos East } ]
         | East -> [ { direction = North; pos = next ray.pos North } ]
         | South -> [ { direction = West; pos = next ray.pos West } ]
         | West -> [ { direction = South; pos = next ray.pos South } ])
      | BackwardMirror ->
        (match ray.direction with
         | North -> [ { direction = West; pos = next ray.pos West } ]
         | East -> [ { direction = South; pos = next ray.pos South } ]
         | South -> [ { direction = East; pos = next ray.pos East } ]
         | West -> [ { direction = North; pos = next ray.pos North } ])
    in
    let rays =
      List.map rays ~f:cast''
      |> List.join
      |> List.filter ~f:(fun ray ->
        let x, y = ray.pos in
        Int.between x ~low:0 ~high:(width - 1) && Int.between y ~low:0 ~high:(height - 1))
      |> List.filter ~f:(fun ray ->
        match Hash_set.strict_add cache ray with
        | Ok _ -> true
        | _ -> false)
    in
    if List.is_empty rays
    then (
      let result =
        Hash_set.to_list cache
        |> List.map ~f:(fun ray -> ray.pos)
        |> List.sort_and_group
             ~compare:(Tuple2.compare ~cmp1:Int.compare ~cmp2:Int.compare)
        |> List.map ~f:(fun l -> List.nth_exn l 0)
      in
      result |> List.length)
    else cast' rays (gen + 1)
  in
  cast' [ start ] 0
;;

let part1 (map : cell array array) = cast map { direction = East; pos = 0, 0 }

let part2 (map : cell array array) =
  let height = Array.length map in
  let width = Array.length map.(0) in
  [ List.range 0 width
    |> List.map ~f:(fun x ->
      [ { direction = South; pos = x, 0 }; { direction = North; pos = x, height - 1 } ])
  ; List.range 0 height
    |> List.map ~f:(fun y ->
      [ { direction = East; pos = 0, y }; { direction = West; pos = width - 1, y } ])
  ]
  |> List.map ~f:List.concat_no_order
  |> List.concat_no_order
  |> List.fold ~init:0 ~f:(fun max start -> Int.max max (cast map start))
;;

let () =
  print_endline "Sample:";
  let map = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 map |> printf "Part 1: %d\n";
  Out_channel.flush Out_channel.stdout;
  part2 map |> printf "Part 2: %d\n";
  print_endline "";
  print_endline "Actual:";
  let map = In_channel.read_all "../input.txt" |> process_input in
  part1 map |> printf "Part 1: %d\n";
  Out_channel.flush Out_channel.stdout;
  part2 map |> printf "Part 2: %d\n"
;;
