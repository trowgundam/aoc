open Core

let process_input (lines : string list) =
  List.map lines ~f:(fun l ->
    String.split_on_chars l ~on:[ ' ' ] |> List.map ~f:int_of_string)
;;

let rec generate_history (hist : int list) =
  let rec walk val1 val2 data =
    match data with
    | [] -> [ val2 - val1 ]
    | hd :: tl -> [ val2 - val1 ] @ walk val2 hd tl
  in
  if List.for_all hist ~f:(( = ) 0)
  then [ hist ]
  else (
    match hist with
    | val1 :: val2 :: tl ->
      let child = walk val1 val2 tl in
      [ hist ] @ generate_history child
    | _ -> [])
;;

let predict_set (set : int list) =
  let history = generate_history set in
  List.map history ~f:List.last_exn |> List.fold ~init:0 ~f:( + )
;;

let part1 (input : int list list) =
  List.map input ~f:predict_set |> List.fold ~init:0 ~f:( + )
;;

let part2 (input : int list list) =
  List.map input ~f:List.rev |> List.map ~f:predict_set |> List.fold ~init:0 ~f:( + )
;;

let () =
  let input = In_channel.read_lines "../input-sample.txt" |> process_input in
  printf "Sample:\n";
  part1 input |> printf "Part1: %d\n";
  part2 input |> printf "Part2: %d\n";
  Out_channel.flush Out_channel.stdout;
  let input = In_channel.read_lines "../input.txt" |> process_input in
  printf "\nActual:\n";
  part1 input |> printf "Part1: %d\n";
  part2 input |> printf "Part2: %d\n";
  Out_channel.flush Out_channel.stdout
;;
