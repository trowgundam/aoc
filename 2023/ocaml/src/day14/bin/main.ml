open Core

let process_input lines =
  let process_input' line =
    String.to_array line
    |> Array.map ~f:(fun c ->
      match c with
      | 'O' -> 2
      | '#' -> 1
      | '.' -> 0
      | _ -> failwith @@ sprintf "Invalid Char: %c" c)
  in
  List.map lines ~f:process_input' |> Array.of_list
;;

module TiltResult = struct
  module T = struct
    type t = int array array * int

    let compare = compare_array @@ compare_array compare_int
    let compare = Tuple2.compare ~cmp1:compare ~cmp2:compare_int
    let sexp_of_t = Array.sexp_of_t @@ Array.sexp_of_t Int.sexp_of_t
    let sexp_of_t = Tuple2.sexp_of_t sexp_of_t Int.sexp_of_t
    let t_of_sexp = Array.t_of_sexp @@ Array.t_of_sexp Int.t_of_sexp
    let t_of_sexp = Tuple2.t_of_sexp t_of_sexp Int.t_of_sexp
    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

module Board = struct
  module T = struct
    type t = int array array

    let compare = compare_array @@ compare_array compare_int
    let sexp_of_t = Array.sexp_of_t @@ Array.sexp_of_t Int.sexp_of_t
    let t_of_sexp = Array.t_of_sexp @@ Array.t_of_sexp Int.t_of_sexp
    let hash = Hashtbl.hash
  end

  include T
  include Comparable.Make (T)
end

(* let string_of_int_list lst = *)
(*   List.map lst ~f:string_of_int |> String.concat ~sep:"; " |> sprintf "[ %s ]" *)
(* ;; *)

(* let print_board (board : int array array) = *)
(*   let convert i = *)
(*     match i with *)
(*     | 0 -> '.' *)
(*     | 1 -> '#' *)
(*     | 2 -> 'O' *)
(*     | _ -> ' ' *)
(*   in *)
(*   Array.iter board ~f:(fun r -> *)
(*     Array.map r ~f:convert |> String.of_array |> printf "%s\n") *)
(* ;; *)

let tilt_vert (dir : int) (input : int array array) cache =
  match Hashtbl.find cache (input, dir) with
  | Some r -> r
  | None ->
    let result = Array.copy_matrix input in
    let height = Array.length result in
    let width = Array.length result.(0) in
    let get_seq y =
      if dir < 0
      then Sequence.range ~stride:(-1) ~start:`exclusive ~stop:`inclusive y 0
      else Sequence.range ~start:`exclusive y height
    in
    let seq =
      if dir < 0
      then Sequence.range 0 height
      else Sequence.range ~stride:(-1) ~start:`exclusive ~stop:`inclusive height 0
    in
    Sequence.iter seq ~f:(fun y ->
      if (dir < 0 && y = 0) || (dir > 0 && y = height - 1) || dir = 0
      then ()
      else
        Sequence.range 0 width
        |> Sequence.iter ~f:(fun x ->
          if result.(y).(x) = 2
          then (
            let check_seq = get_seq y in
            let new_row =
              Sequence.fold_until
                check_seq
                ~init:(-1)
                ~finish:(fun a -> a)
                ~f:(fun a py -> if result.(py).(x) = 0 then Continue py else Stop a)
            in
            if new_row >= 0
            then (
              result.(new_row).(x) <- result.(y).(x);
              result.(y).(x) <- 0)
            else ())
          else ()));
    let _ = Hashtbl.add cache ~key:(input, dir) ~data:result in
    result
;;

let tilt_horz (dir : int) (input : int array array) cache =
  match Hashtbl.find cache (input, dir) with
  | Some r -> r
  | None ->
    let result = Array.copy_matrix input in
    let height = Array.length result in
    let width = Array.length result.(0) in
    let get_seq x =
      if dir < 0
      then Sequence.range ~stride:(-1) ~start:`exclusive ~stop:`inclusive x 0
      else Sequence.range ~start:`exclusive x width
    in
    Sequence.range 0 height
    |> Sequence.iter ~f:(fun y ->
      let row =
        if dir < 0
        then Sequence.range 0 width
        else Sequence.range ~stride:(-1) ~start:`exclusive ~stop:`inclusive width 0
      in
      Sequence.iter row ~f:(fun x ->
        if (dir < 0 && x = 0) || (dir > 0 && x = width - 1) || dir = 0
        then ()
        else if result.(y).(x) = 2
        then (
          let check_seq = get_seq x in
          let new_col =
            Sequence.fold_until
              check_seq
              ~init:(-1)
              ~finish:(fun a -> a)
              ~f:(fun a px -> if result.(y).(px) = 0 then Continue px else Stop a)
          in
          if new_col >= 0
          then (
            result.(y).(new_col) <- result.(y).(x);
            result.(y).(x) <- 0)
          else ())
        else ()));
    let _ = Hashtbl.add cache ~key:(input, dir) ~data:result in
    result
;;

let calculate_weight (input : int array array) =
  let height = Array.length input in
  Array.foldi input ~init:0 ~f:(fun y acc row ->
    Array.fold row ~init:0 ~f:(fun acc v ->
      match v with
      | 2 -> acc + (height - y)
      | _ -> acc)
    + acc)
;;

let part1 (input : int array array) =
  let cache = Hashtbl.create ~size:1024 (module TiltResult) in
  tilt_vert (-1) input cache |> calculate_weight
;;

let part2 (input : int array array) =
  let cycle_count = 1000000000 in
  let result_cache = Hashtbl.create ~size:1024 (module Board) in
  let vert_cache = Hashtbl.create ~size:1024 (module TiltResult) in
  let horz_cache = Hashtbl.create ~size:1024 (module TiltResult) in
  let rec part2' input cycle =
    (* printf "Cycle %d\n" cycle; *)
    (* print_board input; *)
    (* print_endline ""; *)
    if cycle = cycle_count
    then input
    else (
      let part2'' input =
        let _ = Hashtbl.add result_cache ~key:input ~data:cycle in
        let input = tilt_vert (-1) input vert_cache in
        (* printf "North:\n"; *)
        (* print_board input; *)
        (* print_endline ""; *)
        let input = tilt_horz (-1) input horz_cache in
        (* printf "West:\n"; *)
        (* print_board input; *)
        (* print_endline ""; *)
        let input = tilt_vert 1 input vert_cache in
        (* printf "South:\n"; *)
        (* print_board input; *)
        (* print_endline ""; *)
        let input = tilt_horz 1 input horz_cache in
        (* printf "East:\n"; *)
        (* print_board input; *)
        (* print_endline ""; *)
        part2' input (cycle + 1)
      in
      match Hashtbl.find result_cache input with
      | None -> part2'' input
      | Some cyc ->
        let diff = cycle - cyc in
        let remainder = (cycle_count - cycle) % diff in
        let target = cyc + remainder in
        if remainder = 0
        then input
        else (
          match
            Hashtbl.to_alist result_cache |> List.find ~f:(fun (_, c) -> c = target)
          with
          | Some (b, _) ->
            (* printf "Actual Cycles: %d -> %d\n" cyc cycle; *)
            b
          | None ->
            let message =
              sprintf
                "cycle = %d; cyc = %d; diff = %d; remainder = %d; target = %d"
                cycle
                cyc
                diff
                remainder
                target
            in
            let message = "Cache failed: " ^ message in
            failwith message))
  in
  part2' input 0 |> calculate_weight
;;

let () =
  printf "Sample:\n";
  let input = In_channel.read_lines "../input-sample.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  Out_channel.flush Out_channel.stdout;
  part2 input |> printf "Part 2: %d\n";
  print_endline "";
  printf "Actual:\n";
  let input = In_channel.read_lines "../input.txt" |> process_input in
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n"
;;
