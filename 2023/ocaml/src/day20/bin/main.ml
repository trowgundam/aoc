open Core

module PulseType = struct
  module T = struct
    type t =
      | Low
      | High

    let int_of_t t =
      match t with
      | Low -> 0
      | High -> 1
    ;;

    let t_of_int i =
      match i with
      | 0 -> Low
      | 1 -> High
      | _ -> failwith @@ sprintf "Invalid int: %d" i
    ;;

    let compare t1 t2 = Int.compare (int_of_t t1) (int_of_t t2)
    let sexp_of_t t = Int.sexp_of_t @@ int_of_t t
    let t_of_sexp s = Int.t_of_sexp s |> t_of_int
  end

  include T
  include Comparable.Make (T)

  let is_high = equal High
end

type module_type =
  | FlipFlop of bool ref
  | Conjunction of (string, PulseType.t) Hashtbl_intf.Hashtbl.t
  | Broadcaster

let module_type_of_char c =
  match c with
  | '%' -> FlipFlop (ref false)
  | '&' -> Conjunction (Hashtbl.create (module String))
  | 'b' -> Broadcaster
  | _ -> failwith @@ sprintf "Invalid Char: %c" c
;;

type puz_module =
  { name : string
  ; module_type : module_type
  ; destinations : string list
  }

let process_input input =
  let process_line line =
    let items =
      String.substr_replace_all line ~pattern:" -> " ~with_:";" |> String.split ~on:';'
    in
    let module_type = String.nget (List.nth_exn items 0) 0 |> module_type_of_char in
    let name =
      match module_type with
      | Broadcaster -> "broadcaster"
      | _ -> String.drop_prefix (List.nth_exn items 0) 1
    in
    let destinations =
      String.split (List.nth_exn items 1) ~on:',' |> List.map ~f:String.strip
    in
    { name; module_type; destinations }
  in
  let modules = String.split_lines input |> List.map ~f:process_line in
  List.iter modules ~f:(fun pm ->
    match pm.module_type with
    | Conjunction cm ->
      List.iter modules ~f:(fun m ->
        if List.mem m.destinations pm.name ~equal:String.equal
        then Hashtbl.set cm ~key:m.name ~data:Low
        else ())
    | _ -> ());
  match
    List.map modules ~f:(fun pm -> pm.name, pm)
    |> Hashtbl.of_alist ~growth_allowed:false ~size:(List.length modules) (module String)
  with
  | `Duplicate_key k -> failwith @@ sprintf "Multiple modules of type %s" k
  | `Ok m -> m
;;

let copy_input (modules : (string, puz_module) Hashtbl_intf.Hashtbl.t) =
  Hashtbl.to_alist modules
  |> List.map ~f:(fun (n, pm) ->
    let module_type =
      match pm.module_type with
      | Conjunction cm -> Conjunction (Hashtbl.copy cm)
      | FlipFlop s -> FlipFlop (ref !s)
      | mt -> mt
    in
    n, { name = pm.name; module_type; destinations = pm.destinations })
  |> Hashtbl.of_alist_exn
       ~growth_allowed:false
       ~size:(Hashtbl.length modules)
       (module String)
;;

let press_button (modules : (string, puz_module) Hashtbl_intf.Hashtbl.t) =
  let rec send queue lows highs =
    let send' pulse destinations sender =
      List.map destinations ~f:(fun d -> d, pulse, sender) |> Queue.enqueue_all queue;
      let lows =
        let open PulseType in
        match pulse with
        | Low -> lows + List.length destinations
        | _ -> lows
      in
      let highs =
        match pulse with
        | High -> highs + List.length destinations
        | _ -> highs
      in
      send queue lows highs
    in
    match Queue.dequeue queue with
    | None -> lows, highs
    | Some (name, pulse, sender) ->
      (match Hashtbl.find modules name with
       | None -> send queue lows highs
       | Some pm ->
         let open PulseType in
         (match pm.module_type, pulse with
          | Broadcaster, _ -> send' pulse pm.destinations name
          | FlipFlop _, High -> send queue lows highs
          | FlipFlop s, Low ->
            s := not !s;
            let sm = if !s then High else Low in
            send' sm pm.destinations name
          | Conjunction cm, _ ->
            Hashtbl.set cm ~key:sender ~data:pulse;
            let all_high = Hashtbl.for_all cm ~f:PulseType.is_high in
            let sm = if all_high then Low else High in
            send' sm pm.destinations name))
  in
  let queue = Queue.create () in
  let open PulseType in
  Queue.enqueue queue ("broadcaster", Low, "button");
  send queue 1 0
;;

let part1 input =
  let input = copy_input input in
  let rec press modules count results =
    if count = 0
    then results
    else press modules (count - 1) (results @ [ press_button modules ])
  in
  let results = press input 1000 [] in
  let lows, highs = List.unzip results in
  let lows, highs = List.fold lows ~init:0 ~f:( + ), List.fold highs ~init:0 ~f:( + ) in
  lows * highs
;;

let rec gcd a b = if b = 0 then a else gcd b (a mod b)
let lcm a b = a * b / gcd a b

let send pm pt from =
  let open PulseType in
  match pm.module_type, pt with
  | Broadcaster, _ -> Some (Low, pm.destinations)
  | FlipFlop _, High -> None
  | FlipFlop s, Low ->
    s := not !s;
    if !s then Some (High, pm.destinations) else Some (Low, pm.destinations)
  | Conjunction cm, _ ->
    Hashtbl.set cm ~key:from ~data:pt;
    if Hashtbl.for_all cm ~f:PulseType.is_high
    then Some (Low, pm.destinations)
    else Some (High, pm.destinations)
;;

let run2 modules target =
  let rec outer count =
    let count = count + 1 in
    let start = Hashtbl.find_exn modules "broadcaster" in
    let start = send start Low "" |> Option.value_exn in
    let queue = Queue.of_list [ "broadcaster", start ] in
    let rec inner () =
      match Queue.dequeue queue with
      | None -> outer count
      | Some (from, (pulse, names)) ->
        if String.equal from target && PulseType.is_high pulse
        then count
        else (
          List.iter names ~f:(fun name ->
            match Hashtbl.find modules name with
            | Some pm ->
              (match send pm pulse from with
               | Some o -> Queue.enqueue queue (name, o)
               | _ -> ())
            | None -> ());
          inner ())
    in
    inner ()
  in
  outer 0
;;

let part2 input =
  let input = copy_input input in
  let bb =
    match
      Hashtbl.to_alist input
      |> List.find ~f:(fun (_, pm) -> List.mem pm.destinations "rx" ~equal:String.equal)
    with
    | Some (_, pm) -> pm
    | None -> failwith "Can't find rx"
  in
  Hashtbl.filter input ~f:(fun pm -> List.mem pm.destinations bb.name ~equal:String.equal)
  |> Hashtbl.to_alist
  |> List.unzip
  |> snd
  |> List.map ~f:(fun pm ->
    let modules = copy_input input in
    run2 modules pm.name)
  |> List.fold ~init:1 ~f:lcm
;;

let () =
  print_endline "Sample 1:";
  let input = In_channel.read_all "../input-sample.txt" |> process_input in
  part1 input |> sprintf "Part 1: %d" |> print_endline;
  print_endline "";
  print_endline "Sample 2:";
  let input = In_channel.read_all "../input-sample2.txt" |> process_input in
  part1 input |> sprintf "Part 1: %d" |> print_endline;
  print_endline "";
  print_endline "Actual:";
  let input = In_channel.read_all "../input.txt" |> process_input in
  part1 input |> sprintf "Part 1: %d" |> print_endline;
  part2 input |> sprintf "Part 1: %d" |> print_endline
;;
