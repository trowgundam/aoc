open Core

(* let range start len = List.range ~stride:1 start (start + len) *)

let rec split_list (on : string -> bool) (data : string list) =
  let delim f = not @@ on f in
  let curr = List.take_while data ~f:delim in
  if List.length data = List.length curr
  then [ curr ]
  else (
    let remainder = List.slice data (List.length curr + 1) (List.length data) in
    [ curr ] @ split_list on remainder)
;;

let part1_seeds s = List.sort s ~compare:Int.compare |> Sequence.of_list

let part2_seeds s =
  let rec aux s =
    match s with
    | st :: len :: tail -> [ st, len ] @ aux tail
    | _ -> []
  in
  aux s
;;

let get_seeds fs s =
  match List.nth s 0 with
  | Some s ->
    String.slice s 7 (String.length s)
    |> String.split_on_chars ~on:[ ' ' ]
    |> List.map ~f:int_of_string
    |> fs
  | None -> failwith "Invalid Seeds"
;;

let get_map_values line =
  let range_descriptor =
    String.split_on_chars line ~on:[ ' ' ] |> List.map ~f:int_of_string
  in
  match range_descriptor with
  | dest :: source :: len :: _ -> dest, source, len
  | _ -> failwith "Invalid Map Value"
;;

(* let print_sequence s = printf "%d, %d\n" *)

let get_map s =
  match s with
  | _ :: data -> List.map data ~f:get_map_values
  | _ -> failwith "Invalid Invalid Map Section"
;;

let get_processor seed_func input =
  let sections = split_list String.is_empty input in
  match sections with
  | s :: ss :: sf :: fw :: wl :: lt :: th :: hl :: _ ->
    ( get_seeds seed_func s
    , get_map ss
    , get_map sf
    , get_map fw
    , get_map wl
    , get_map lt
    , get_map th
    , get_map hl )
  | _ -> failwith "Invalid Input"
;;

let get_location
  (seeds, seed_soil, soil_fert, fert_water, water_light, light_temp, temp_hum, hum_loc)
  =
  let translate v ~map:(mp : (int * int * int) list) =
    let in_set (_, s, l) = Int.between v ~low:s ~high:(s + l - 1) in
    match List.find mp ~f:in_set with
    | None -> v
    | Some (d, s, _) -> v - s + d
  in
  let translate_seed s =
    translate s ~map:seed_soil
    |> translate ~map:soil_fert
    |> translate ~map:fert_water
    |> translate ~map:water_light
    |> translate ~map:light_temp
    |> translate ~map:temp_hum
    |> translate ~map:hum_loc
  in
  let locs = Sequence.map seeds ~f:translate_seed in
  match Sequence.min_elt locs ~compare:Int.compare with
  | None -> failwith "Impossible"
  | Some l -> l
;;

let part1 proc = get_location proc

let part2 proc =
  let seeds, _, _, _, _, _, _, _ = proc in
  let expand_seeds (sr, sl) =
    let _, seed_soil, soil_fert, fert_water, water_light, light_temp, temp_hum, hum_loc =
      proc
    in
    ( Sequence.range sr (sr + sl - 1)
    , seed_soil
    , soil_fert
    , fert_water
    , water_light
    , light_temp
    , temp_hum
    , hum_loc )
  in
  match
    List.map seeds ~f:expand_seeds
    |> List.map ~f:part1
    |> List.min_elt ~compare:Int.compare
  with
  | None -> failwith "Impossible"
  | Some l -> l
;;

(* let print_map (hd, values) = *)
(*     printf "Map %s:\n" hd *)
(*     ; List.iter values ~f:(fun (i, o, l) -> printf "%d, %d -> %d, %d\n" i l o l);; *)
(* let print_processor (s, ss, sf, fw, wl, lt, th, hl) = *)
(*     printf "Seeds:\n" *)
(*     ; List.iter s ~f:(fun (st, ln) -> printf "%d, %d\n" st ln) *)
(*     ; print_map ss *)
(*     ; print_map sf *)
(*     ; print_map fw *)
(*     ; print_map wl *)
(*     ; print_map lt *)
(*     ; print_map th *)
(*     ; print_map hl;; *)

let process_file f =
  let lines = In_channel.read_lines f in
  get_processor part1_seeds lines |> part1 |> printf "Part1: %d\n";
  get_processor part2_seeds lines |> part2 |> printf "Part2: %d\n"
;;

let () =
  process_file "../input-sample.txt";
  process_file "../input.txt"
;;
