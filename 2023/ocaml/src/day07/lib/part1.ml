open Core
open Common

let get_hand_type (h : hand) =
  match
    List.sort h.cards ~compare:Int.compare
    |> List.group ~break:( <> )
    |> List.map ~f:List.length
    |> List.sort ~compare:Int.compare
    |> List.rev
  with
  | 5 :: _ -> FiveKind
  | 4 :: _ -> FourKind
  | 3 :: 2 :: _ -> FullHouse
  | 3 :: _ -> ThreeKind
  | 2 :: 2 :: _ -> TwoPair
  | 2 :: _ -> OnePair
  | _ -> HighCard
;;

let compare_hand (h1 : hand) (h2 : hand) =
  let h1_type, h2_type = get_hand_type h1, get_hand_type h2 in
  match compare_hand_type h1_type h2_type with
  | 0 ->
    let rec compare_cards c1 c2 =
      match c1, c2 with
      | hd1 :: tl1, hd2 :: tl2 ->
        if hd1 = hd2 then compare_cards tl1 tl2 else Int.compare hd1 hd2
      | [], [] -> 0
      | [], _ -> -1
      | _, [] -> 1
    in
    compare_cards h1.cards h2.cards
  | c -> c
;;

let process_cards cards =
  let get_card c =
    match c with
    | '2' .. '9' -> String.of_char c |> int_of_string
    | 'T' -> 10
    | 'J' -> 11
    | 'Q' -> 12
    | 'K' -> 13
    | 'A' -> 14
    | _ -> failwith "Invalid Card"
  in
  String.to_list cards |> List.map ~f:get_card
;;

let process_line line =
  match String.split_on_chars line ~on:[ ' ' ] with
  | [ c; b ] -> { cards = process_cards c; bid = int_of_string b }
  | _ -> failwith "Invalid Hand"
;;

let part1 input =
  List.map input ~f:process_line
  |> List.sort ~compare:compare_hand
  |> List.zip_exn (List.range ~stop:`inclusive 1 @@ List.length input)
  |> List.map ~f:(fun (r, h) -> r * h.bid)
  |> List.fold ~init:0 ~f:( + )
;;
