type hand_type =
  | FiveKind
  | FourKind
  | FullHouse
  | ThreeKind
  | TwoPair
  | OnePair
  | HighCard

let get_hand_power (h : hand_type) =
  match h with
  | FiveKind -> 6
  | FourKind -> 5
  | FullHouse -> 4
  | ThreeKind -> 3
  | TwoPair -> 2
  | OnePair -> 1
  | HighCard -> 0
;;

let compare_hand_type (h1 : hand_type) (h2 : hand_type) =
  Int.compare (get_hand_power h1) (get_hand_power h2)
;;

type hand =
  { cards : int list
  ; bid : int
  }
