type hand_type =
  | FiveKind
  | FourKind
  | FullHouse
  | ThreeKind
  | TwoPair
  | OnePair
  | HighCard

val get_hand_power : hand_type -> int
val compare_hand_type : hand_type -> hand_type -> int

type hand =
  { cards : int list
  ; bid : int
  }
