open Core
open Day07.Part1
open Day07.Part2

let () =
  printf "Sample:\n";
  let lines = In_channel.read_lines "../input-sample.txt" in
  part1 lines |> printf "Part1: %d\n";
  part2 lines |> printf "Part2: %d\n";
  printf "\nActual:\n";
  let lines = In_channel.read_lines "../input.txt" in
  part1 lines |> printf "Part1: %d\n";
  part2 lines |> printf "Part2: %d\n"
;;
