open Core

type map =
  { nodes : (string, int) Hashtbl.t
  ; directions : int array
  ; map : int array array
  }

let parse_input lines =
  let parse_node line =
    let name = String.slice line 0 3 in
    let left = String.slice line 7 10 in
    let right = String.slice line 12 15 in
    name, (left, right)
  in
  let translate_dir c =
    match Char.uppercase c with
    | 'L' -> 0
    | 'R' -> 1
    | _ -> failwith @@ "Invalid Direction " ^ String.of_char c
  in
  let directions =
    List.hd_exn lines |> String.to_list |> List.map ~f:translate_dir |> Array.of_list
  in
  let nodes = List.tl_exn lines |> List.tl_exn |> List.map ~f:parse_node in
  let node_key =
    List.map nodes ~f:(fun (n, _) -> n)
    |> List.zip_exn (List.range 0 (List.length nodes))
    |> List.map ~f:(fun (i, n) -> n, i)
    |> Hashtbl.of_alist_exn
         ~growth_allowed:false
         ~size:(List.length nodes)
         (module String)
  in
  let translate_node_name n = Hashtbl.find_exn node_key n in
  let nodes_map =
    List.map nodes ~f:(fun (n, (l, r)) ->
      translate_node_name n, [| translate_node_name l; translate_node_name r |])
    |> List.map ~f:(fun (_, d) -> d)
    |> Array.of_list
  in
  { nodes = node_key; directions; map = nodes_map }
;;

let walk_node start (is_end : int -> bool) (input : map) =
  let get_direction n d = Array.get (Array.get input.map n) d in
  let rec walk node d =
    if is_end node
    then d
    else (
      let dir = Array.get input.directions (d mod Array.length input.directions) in
      walk (get_direction node dir) (d + 1))
  in
  walk start 0
;;

let rec prime_factors n =
  let rec find_divisor n candidate =
    if candidate * candidate > n
    then n
    else if n mod candidate = 0
    then candidate
    else find_divisor n (candidate + 1)
  in
  if n < 2
  then []
  else (
    let divisor = find_divisor n 2 in
    if divisor = n then [ n ] (* n is prime *) else divisor :: prime_factors (n / divisor))
;;

let collect_powers (factors : int list) =
  List.sort factors ~compare:Int.compare
  |> List.group ~break:( <> )
  |> List.map ~f:(fun p -> List.hd_exn p, List.length p)
;;

let combine_base (factor : (int * int) list) =
  let b, _ = List.hd_exn factor in
  let e =
    match List.max_elt factor ~compare:(fun (_, e1) (_, e2) -> Int.compare e1 e2) with
    | None -> failwith "Impossible"
    | Some (_, e) -> e
  in
  Int.pow b e
;;

let part1 (input : map) =
  let get_index n = Hashtbl.find_exn input.nodes n in
  let start = get_index "AAA" in
  let end_index = get_index "ZZZ" in
  let is_end e = end_index = e in
  walk_node start is_end input
;;

let part2 (input : map) =
  let get_index n = Hashtbl.find_exn input.nodes n in
  let ends =
    let is_end n = Char.compare (String.nget n 2) 'Z' = 0 in
    Hashtbl.keys input.nodes |> List.filter ~f:is_end |> List.map ~f:get_index
  in
  let starts =
    let is_start n = Char.compare (String.nget n 2) 'A' = 0 in
    Hashtbl.keys input.nodes
    |> List.filter ~f:is_start
    |> List.map ~f:get_index
    |> Array.of_list
  in
  let is_end n = Option.is_some @@ List.find ends ~f:(fun e -> e = n) in
  let lengths = Array.map starts ~f:(fun s -> walk_node s is_end input) in
  let prime_factors = Array.map lengths ~f:prime_factors |> List.of_array in
  List.map prime_factors ~f:collect_powers
  |> List.join
  |> List.sort ~compare:(fun (p1, _) (p2, _) -> Int.compare p1 p2)
  |> List.group ~break:(fun (p1, _) (p2, _) -> p1 <> p2)
  |> List.map ~f:combine_base
  |> List.fold ~init:1 ~f:( * )
;;

let () =
  let part1_sample =
    In_channel.read_lines "../input-sample.txt" |> parse_input |> part1
  in
  let part2_sample =
    In_channel.read_lines "../input-sample2.txt" |> parse_input |> part2
  in
  let input = In_channel.read_lines "../input.txt" |> parse_input in
  let part1_actual = part1 input in
  let part2_actual = part2 input in
  printf
    "Sample:\nPart1: %d\nPart2: %d\n\nActual:\nPart1: %d\nPart2: %d\n"
    part1_sample
    part2_sample
    part1_actual
    part2_actual
;;
