open Core

type map_empties =
  { rows : int list
  ; columns : int list
  }

let is_char (t : char) (c : char) = 0 = compare_char t c

let find_empties (lines : string list) =
  let is_empty (line : char list) =
    match List.find line ~f:(is_char '#') with
    | None -> true
    | Some _ -> false
  in
  let process_row (index : int) (line : char list) =
    if is_empty line then Some index else None
  in
  let separated = List.map lines ~f:String.to_list in
  let empty_rows = List.filter_mapi separated ~f:process_row in
  let empty_columns = List.transpose_exn separated |> List.filter_mapi ~f:process_row in
  { rows = empty_rows; columns = empty_columns }
;;

let find_galaxies (lines : string list) =
  let find_galaxies (y : int) (line : string) =
    let find_galaxies (x : int) (c : char) =
      if is_char '#' c then Some (x, y) else None
    in
    String.to_list line |> List.filter_mapi ~f:find_galaxies
  in
  List.mapi lines ~f:find_galaxies |> List.join
;;

let expand_galaxies (galaxies : (int * int) list) ~(empties : map_empties) ~(factor : int)
  =
  let expand_galaxy ((x, y) : int * int) =
    let dx = List.filter empties.columns ~f:(fun c -> x > c) |> List.length in
    let dy = List.filter empties.rows ~f:(fun r -> y > r) |> List.length in
    x + (dx * factor) - dx, y + (dy * factor) - dy
  in
  List.map galaxies ~f:expand_galaxy
;;

let pair_galaxies (galaxies : (int * int) list) =
  let pair_galaxy (index : int) (galaxy : int * int) =
    if index + 1 = List.length galaxies
    then []
    else
      List.range (index + 1) @@ List.length galaxies
      |> List.map ~f:(fun i -> galaxy, List.nth_exn galaxies i)
  in
  List.mapi galaxies ~f:pair_galaxy |> List.join
;;

let process (input : string list) ~(factor : int) =
  let galaxies =
    find_galaxies input |> expand_galaxies ~empties:(find_empties input) ~factor
  in
  let pairs = pair_galaxies galaxies in
  List.map pairs ~f:(fun ((x1, y1), (x2, y2)) -> abs (x1 - x2) + abs (y1 - y2))
  |> List.fold ~init:0 ~f:( + )
;;

let part1 = process ~factor:2
let part2_sample = process ~factor:10
let part2 = process ~factor:1000000

let () =
  let input = In_channel.read_lines "../input-sample.txt" in
  printf "Sample:\n";
  part1 input |> printf "Part 1: %d\n";
  part2_sample input |> printf "Part 2: %d\n";
  print_endline "";
  let input = In_channel.read_lines "../input.txt" in
  printf "Actual:\n";
  part1 input |> printf "Part 1: %d\n";
  part2 input |> printf "Part 2: %d\n"
;;
