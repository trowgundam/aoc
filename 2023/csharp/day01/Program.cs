﻿// See https://aka.ms/new-console-template for more information
const string file_name = "../files/day01.txt";
int part1_sum = 0;
int part2_sum = 0;
await foreach (string line in File.ReadLinesAsync(file_name))
{
    IEnumerable<char> numbers = line.Where(char.IsNumber);
    part1_sum +=
        (int.Parse(numbers.First().ToString()) * 10) + int.Parse(numbers.Last().ToString());
    char? first = null,
        last = null;
    for (int i = 0; i < line.Length; i++)
    {
        first = get_int(line[i..]);
        if (first is not null)
        {
            break;
        }
    }
    for (int i = line.Length - 1; i >= 0; i--)
    {
        last = get_int(line[i..]);
        if (last is not null)
        {
            break;
        }
    }
    if (first is null || last is null)
    {
        return -1;
    }
    part2_sum += (int.Parse(first.Value.ToString()) * 10) + int.Parse(last.Value.ToString());
}
Console.WriteLine($"Part1: {part1_sum}");
Console.WriteLine($"Part2: {part2_sum}");
return 0;

static char? get_int(string value)
{
    if (char.IsDigit(value.First()))
    {
        return value.First();
    }

    if (value.StartsWith("one", StringComparison.OrdinalIgnoreCase))
    {
        return '1';
    }

    if (value.StartsWith("two", StringComparison.OrdinalIgnoreCase))
    {
        return '2';
    }

    if (value.StartsWith("three", StringComparison.OrdinalIgnoreCase))
    {
        return '3';
    }

    if (value.StartsWith("four", StringComparison.OrdinalIgnoreCase))
    {
        return '4';
    }

    if (value.StartsWith("five", StringComparison.OrdinalIgnoreCase))
    {
        return '5';
    }

    if (value.StartsWith("six", StringComparison.OrdinalIgnoreCase))
    {
        return '6';
    }

    if (value.StartsWith("seven", StringComparison.OrdinalIgnoreCase))
    {
        return '7';
    }

    if (value.StartsWith("eight", StringComparison.OrdinalIgnoreCase))
    {
        return '8';
    }

    return value.StartsWith("nine", StringComparison.OrdinalIgnoreCase) ? '9' : null;
}
