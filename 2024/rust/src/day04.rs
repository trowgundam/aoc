fn main() {
    let input = parse(include_str!("../../files/day04.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<Vec<char>> {
    input
        .lines()
        .map(|l| l.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>()
}

fn part1(input: &[Vec<char>]) -> usize {
    test(input, 'X', xmas_count)
}

fn part2(input: &[Vec<char>]) -> usize {
    test(input, 'A', mas_count)
}

fn test<F>(input: &[Vec<char>], trigger: char, test: F) -> usize
where
    F: Fn(i32, i32, &[Vec<char>]) -> usize,
{
    (0..input.len())
        .map(|y| {
            (0..input.first().unwrap().len())
                .map(|x| {
                    if input[y][x] == trigger {
                        test(x as i32, y as i32, input)
                    } else {
                        0
                    }
                })
                .sum::<usize>()
        })
        .sum()
}

fn xmas_count(x: i32, y: i32, input: &[Vec<char>]) -> usize {
    let coords = vec![
        vec![
            ('X', x, y),
            ('M', x + 1, y),
            ('A', x + 2, y),
            ('S', x + 3, y),
        ],
        vec![
            ('X', x, y),
            ('M', x + 1, y + 1),
            ('A', x + 2, y + 2),
            ('S', x + 3, y + 3),
        ],
        vec![
            ('X', x, y),
            ('M', x, y + 1),
            ('A', x, y + 2),
            ('S', x, y + 3),
        ],
        vec![
            ('X', x, y),
            ('M', x - 1, y + 1),
            ('A', x - 2, y + 2),
            ('S', x - 3, y + 3),
        ],
        vec![
            ('X', x, y),
            ('M', x - 1, y),
            ('A', x - 2, y),
            ('S', x - 3, y),
        ],
        vec![
            ('X', x, y),
            ('M', x - 1, y - 1),
            ('A', x - 2, y - 2),
            ('S', x - 3, y - 3),
        ],
        vec![
            ('X', x, y),
            ('M', x, y - 1),
            ('A', x, y - 2),
            ('S', x, y - 3),
        ],
        vec![
            ('X', x, y),
            ('M', x + 1, y - 1),
            ('A', x + 2, y - 2),
            ('S', x + 3, y - 3),
        ],
    ];
    count(input, coords)
}

fn mas_count(x: i32, y: i32, input: &[Vec<char>]) -> usize {
    let coords = vec![
        // M.S
        // .A.
        // M.S
        vec![
            ('M', x - 1, y + 1),
            ('A', x, y),
            ('S', x + 1, y - 1),
            ('M', x - 1, y - 1),
            // ('A', x, y),
            ('S', x + 1, y + 1),
        ],
        // M.M
        // .A.
        // S.S
        vec![
            ('M', x - 1, y - 1),
            ('A', x, y),
            ('S', x + 1, y + 1),
            ('M', x + 1, y - 1),
            // ('A', x, y),
            ('S', x - 1, y + 1),
        ],
        // S.M
        // .A.
        // S.M
        vec![
            ('M', x + 1, y - 1),
            ('A', x, y),
            ('S', x - 1, y + 1),
            ('M', x + 1, y + 1),
            // ('A', x, y),
            ('S', x - 1, y - 1),
        ],
        // S.S
        // .A.
        // M.M
        vec![
            ('M', x + 1, y + 1),
            ('A', x, y),
            ('S', x - 1, y - 1),
            ('M', x - 1, y + 1),
            // ('A', x, y),
            ('S', x + 1, y - 1),
        ],
    ];
    count(input, coords)
}

fn count(input: &[Vec<char>], coords: Vec<Vec<(char, i32, i32)>>) -> usize {
    coords
        .iter()
        .filter(|set| {
            !set.iter().any(|&(_, x, y)| {
                x < 0
                    || x >= input.first().unwrap().len() as i32
                    || y < 0
                    || y >= input.len() as i32
            })
        })
        .filter(|coord| {
            coord
                .iter()
                .all(|&(c, x, y)| c == input[y as usize][x as usize])
        })
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample04.txt"));
        let part1 = part1(&input);
        assert_eq!(part1, 18);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample04.txt"));
        let part2 = part2(&input);
        assert_eq!(part2, 9);
    }
}
