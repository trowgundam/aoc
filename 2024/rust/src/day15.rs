use itertools::Itertools;

fn main() {
    let input = parse(include_str!("../../files/day15.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

#[allow(dead_code)]
fn print_map(map: &[Vec<Option<MapItem>>], rx: &usize, ry: &usize) {
    // Keeping this for if I want to do a Visualization or something, although I doubt it.
    for (y, r) in map.iter().enumerate() {
        println!(
            "{}",
            r.iter()
                .enumerate()
                .map(|(x, mi)| if x == *rx && y == *ry {
                    '@'
                } else {
                    match mi {
                        Some(MapItem::Wall) => '#',
                        Some(MapItem::Box) => 'O',
                        Some(MapItem::BoxLeft) => '[',
                        Some(MapItem::BoxRight) => ']',
                        None => ' ',
                    }
                })
                .join("")
        );
    }
}

#[derive(Debug)]
enum Direction {
    Left,
    Up,
    Right,
    Down,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum MapItem {
    Wall,
    Box,
    BoxLeft,
    BoxRight,
}

struct Input {
    pub map: Vec<Vec<Option<MapItem>>>,
    pub moves: Vec<Direction>,
    pub robot_starting_pos: (usize, usize),
}

fn parse(input: &str) -> Input {
    let map = input
        .lines()
        .take_while(|l| l.starts_with('#'))
        .map(|l| l.chars().collect_vec())
        .collect_vec();
    let moves = input
        .lines()
        .skip_while(|l| l.starts_with('#'))
        .filter(|l| !l.is_empty())
        .join("");

    let (robot_y, row) = map
        .iter()
        .enumerate()
        .find(|&(_, r)| r.contains(&'@'))
        .unwrap();
    let (robot_x, _) = row.iter().enumerate().find(|&(_, c)| *c == '@').unwrap();

    Input {
        map: map
            .iter()
            .map(|r| {
                r.iter()
                    .map(|c| match c {
                        '#' => Some(MapItem::Wall),
                        'O' => Some(MapItem::Box),
                        '@' => None,
                        '.' => None,
                        c => panic!("Unknown map character: {c}"),
                    })
                    .collect_vec()
            })
            .collect_vec(),
        moves: moves
            .chars()
            .filter_map(|c| match c {
                '<' => Some(Direction::Left),
                '^' => Some(Direction::Up),
                '>' => Some(Direction::Right),
                'v' => Some(Direction::Down),
                _ => None,
            })
            .collect_vec(),
        robot_starting_pos: (robot_x, robot_y),
    }
}

fn part1(input: &Input) -> usize {
    let mut map = input.map.clone();
    let mut robot_x = input.robot_starting_pos.0;
    let mut robot_y = input.robot_starting_pos.1;

    for mv in input.moves.iter() {
        let new_x = match mv {
            Direction::Left => robot_x - 1,
            Direction::Right => robot_x + 1,
            _ => robot_x,
        };
        let new_y = match mv {
            Direction::Up => robot_y - 1,
            Direction::Down => robot_y + 1,
            _ => robot_y,
        };

        if Some(MapItem::Wall) == map[new_y][new_x] {
            continue;
        } else {
            move_box(&mut map, new_x, new_y, mv);
            if map[new_y][new_x].is_none() {
                robot_x = new_x;
                robot_y = new_y;
            }
        }
    }

    map.into_iter()
        .enumerate()
        .map(|(y, row)| {
            row.into_iter()
                .enumerate()
                .map(|(x, item)| match item {
                    Some(MapItem::Box) => (y * 100) + x,
                    _ => 0,
                })
                .sum::<usize>()
        })
        .sum()
}

fn can_move(
    map: &mut Vec<Vec<Option<MapItem>>>,
    x: usize,
    y: usize,
    direction: &Direction,
) -> bool {
    let x = match map[y][x] {
        Some(MapItem::BoxRight) => x - 1,
        _ => x,
    };
    let new_x = match direction {
        Direction::Left => x - 1,
        Direction::Right => x + 1,
        _ => x,
    };
    let new_y = match direction {
        Direction::Up => y - 1,
        Direction::Down => y + 1,
        _ => y,
    };

    if Some(MapItem::Wall) == map[new_y][new_x] || Some(MapItem::Wall) == map[new_y][new_x + 1] {
        false
    } else {
        (map[new_y][new_x].is_none() || can_move(map, new_x, new_y, direction))
            && (map[new_y][new_x + 1].is_none() || can_move(map, new_x + 1, new_y, direction))
    }
}

fn move_box(map: &mut Vec<Vec<Option<MapItem>>>, x: usize, y: usize, direction: &Direction) {
    if map[y][x].is_none() || Some(MapItem::Wall) == map[y][x] {
        return;
    }

    let norm_box = Some(MapItem::Box) == map[y][x];
    let x = match map[y][x] {
        Some(MapItem::BoxRight) => x - 1,
        _ => x,
    };
    let new_x = match direction {
        Direction::Left => x - 1,
        Direction::Right => x + 1,
        _ => x,
    };
    let new_y = match direction {
        Direction::Up => y - 1,
        Direction::Down => y + 1,
        _ => y,
    };

    if norm_box {
        move_box(map, new_x, new_y, direction);
    } else {
        match direction {
            Direction::Left => {
                move_box(map, new_x, new_y, direction);
            }
            Direction::Right => {
                move_box(map, new_x + 1, new_y, direction);
            }
            _ => {
                if can_move(map, x, y, direction) {
                    move_box(map, new_x, new_y, direction);
                    move_box(map, new_x + 1, new_y, direction);
                }
            }
        };
    }

    let can_move = if norm_box {
        map[new_y][new_x].is_none()
    } else {
        match direction {
            Direction::Left => map[new_y][new_x].is_none(),
            Direction::Right => map[new_y][new_x + 1].is_none(),
            _ => map[new_y][new_x].is_none() && map[new_y][new_x + 1].is_none(),
        }
    };

    if can_move {
        if norm_box {
            map[y][x] = None;
            map[new_y][new_x] = Some(MapItem::Box);
        } else {
            map[y][x] = None;
            map[y][x + 1] = None;
            map[new_y][new_x] = Some(MapItem::BoxLeft);
            map[new_y][new_x + 1] = Some(MapItem::BoxRight);
        }
    }
}

fn part2(input: &Input) -> usize {
    let mut map = input
        .map
        .iter()
        .map(|row| {
            row.iter()
                .flat_map(|mi| match mi {
                    None => vec![None, None],
                    Some(MapItem::Wall) => vec![Some(MapItem::Wall), Some(MapItem::Wall)],
                    Some(MapItem::Box) => vec![Some(MapItem::BoxLeft), Some(MapItem::BoxRight)],
                    _ => vec![],
                })
                .collect_vec()
        })
        .collect_vec();

    let mut robot_x = input.robot_starting_pos.0 * 2;
    let mut robot_y = input.robot_starting_pos.1;

    for mv in input.moves.iter() {
        let new_x = match mv {
            Direction::Left => robot_x - 1,
            Direction::Right => robot_x + 1,
            _ => robot_x,
        };
        let new_y = match mv {
            Direction::Up => robot_y - 1,
            Direction::Down => robot_y + 1,
            _ => robot_y,
        };

        if Some(MapItem::Wall) == map[new_y][new_x] {
            continue;
        } else {
            move_box(&mut map, new_x, new_y, mv);
            if map[new_y][new_x].is_none() {
                robot_x = new_x;
                robot_y = new_y;
            }
        }
        let lefts = map
            .iter()
            .flatten()
            .filter(|&&mi| mi == Some(MapItem::BoxLeft))
            .count();
        let rights = map
            .iter()
            .flatten()
            .filter(|&&mi| mi == Some(MapItem::BoxRight))
            .count();

        assert_eq!(lefts, rights, "Mismatched Box Lefts and Right!");
    }

    let lefts = map
        .iter()
        .flatten()
        .filter(|&&mi| mi == Some(MapItem::BoxLeft))
        .count();
    let rights = map
        .iter()
        .flatten()
        .filter(|&&mi| mi == Some(MapItem::BoxRight))
        .count();

    assert_eq!(lefts, rights, "Mismatched Box Lefts and Right!");

    map.into_iter()
        .enumerate()
        .map(|(y, row)| {
            row.into_iter()
                .enumerate()
                .map(|(x, item)| match item {
                    Some(MapItem::BoxLeft) => (y * 100) + x,
                    _ => 0,
                })
                .sum::<usize>()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample15.txt"));

        let result = part1(&input);
        assert_eq!(10092, result);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample15.txt"));

        let result = part2(&input);
        assert_eq!(9021, result);
    }
}
