use itertools::Itertools;
use rayon::prelude::*;

fn main() {
    let input = parse(include_str!("../../files/day10.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|l| l.chars().filter_map(|c| c.to_digit(10)).collect())
        .collect()
}

fn part1(input: &[Vec<u32>]) -> usize {
    let height = input.len();
    let width = input.first().unwrap().len();

    // (x, y)
    let trailheads = input
        .iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .filter(|(_, a)| **a == 0)
                .map(move |(x, _)| (x, y))
        })
        .collect::<Vec<_>>();

    let get_possibilities = |(x, y)| {
        let mut new_pos = vec![];
        if x > 0 {
            new_pos.push((x - 1, y));
        }
        new_pos.push((x + 1, y));
        if y > 0 {
            new_pos.push((x, y - 1));
        }
        new_pos.push((x, y + 1));
        new_pos
    };

    trailheads
        .into_par_iter()
        .map(|trailhead| {
            get_possibilities(trailhead)
                .into_iter()
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 1)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 2)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 3)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 4)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 5)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 6)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 7)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 8)
                .unique()
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 9)
                .unique()
                .count()
        })
        .sum()
}

fn part2(input: &[Vec<u32>]) -> usize {
    let height = input.len();
    let width = input.first().unwrap().len();

    // (x, y)
    let trailheads = input
        .iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .filter(|(_, a)| **a == 0)
                .map(move |(x, _)| (x, y))
        })
        .collect::<Vec<_>>();

    let get_possibilities = |(x, y)| {
        let mut new_pos = vec![];
        if x > 0 {
            new_pos.push((x - 1, y));
        }
        new_pos.push((x + 1, y));
        if y > 0 {
            new_pos.push((x, y - 1));
        }
        new_pos.push((x, y + 1));
        new_pos
    };

    trailheads
        .into_par_iter()
        .map(|trailhead| {
            get_possibilities(trailhead)
                .into_par_iter()
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 1)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 2)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 3)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 4)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 5)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 6)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 7)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 8)
                .flat_map(&get_possibilities)
                .filter(|(x, y)| *x < width && *y < height)
                .filter(|(x, y)| input[*y][*x] == 9)
                .count()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample10.txt"));
        let result = part1(&input);
        assert_eq!(36, result);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample10.txt"));
        let result = part2(&input);
        assert_eq!(81, result);
    }
}
