use itertools::Itertools;
use std::collections::VecDeque;

fn main() {
    let input = parse(include_str!("../../files/day09.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<Option<usize>> {
    input
        .chars()
        .filter_map(|c| c.to_string().parse::<u32>().ok())
        .enumerate()
        .flat_map(|(idx, count)| {
            if idx % 2 == 0 {
                vec![Some(idx / 2); count as usize]
            } else {
                vec![None; count as usize]
            }
        })
        .collect()
}
fn part1(input: &[Option<usize>]) -> usize {
    let mut my_input = input.iter().collect::<VecDeque<_>>();

    let mut output = vec![];

    while let Some(v) = my_input.pop_front() {
        if v.is_some() {
            output.push(v);
        } else {
            while let Some(last) = my_input.pop_back() {
                if last.is_some() {
                    output.push(last);
                    break;
                }
            }
        }
    }

    output
        .into_iter()
        .enumerate()
        .filter_map(|(i, v)| v.map(|id| i * id))
        .sum()
}

fn part2(input: &[Option<usize>]) -> usize {
    let mut my_input = vec![None; input.len()];
    my_input.clone_from_slice(input);

    // (value, indices)
    let mut map = input
        .iter()
        .enumerate()
        .filter(|(_, v)| v.is_some())
        .chunk_by(|(_, v)| **v)
        .into_iter()
        .map(|(v, g)| (v, g.map(|(i, _)| i).collect::<Vec<_>>()))
        .collect_vec();
    map.reverse();

    for (_, indices) in map {
        let new_indices = my_input
            .iter()
            .enumerate()
            .chunk_by(|(_, v)| **v)
            .into_iter()
            .filter(|(v, _)| v.is_none())
            .map(|(_, g)| g.map(|(i, _)| i).collect::<Vec<_>>())
            .find(|i| {
                i.len() >= indices.len() && i.iter().min().unwrap() < indices.iter().min().unwrap()
            });

        if let Some(new_indices) = new_indices {
            for (o, n) in indices.iter().zip(new_indices.iter()) {
                my_input.swap(*o, *n);
            }
        }
    }

    my_input
        .into_iter()
        .enumerate()
        .filter_map(|(i, v)| v.map(|id| i * id))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse("2333133121414131402");
        let part1 = part1(&input);
        assert_eq!(1928, part1);
    }

    #[test]
    fn sample_part2() {
        let input = parse("2333133121414131402");
        let part2 = part2(&input);
        assert_eq!(2858, part2);
    }
}
