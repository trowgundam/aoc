use rayon::prelude::*;
use std::collections::HashSet;

fn main() {
    let input = parse(include_str!("../../files/day06.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

struct Input {
    pub start_x: usize,
    pub start_y: usize,
    pub map: Vec<Vec<Option<()>>>,
}

#[derive(Eq, PartialEq, Hash, Clone)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

fn parse(input: &str) -> Input {
    let start = input
        .lines()
        .enumerate()
        .find_map(|(y, l)| {
            l.chars()
                .enumerate()
                .find_map(|(x, c)| if c == '^' { Some((x, y)) } else { None })
        })
        .unwrap();

    Input {
        start_x: start.0,
        start_y: start.1,
        map: input
            .lines()
            .map(|l| {
                l.chars()
                    .map(|c| match c {
                        '#' => Some(()),
                        _ => None,
                    })
                    .collect()
            })
            .collect(),
    }
}

fn part1(input: &Input) -> usize {
    part1_walk(input).len()
}

fn part1_walk(input: &Input) -> Vec<(usize, usize)> {
    let height = input.map.len();
    let width = input.map.first().unwrap().len();

    let mut visited: Vec<Vec<Option<()>>> = (0..height)
        .map(|_| (0..width).map(|_| None).collect())
        .collect();

    visited[input.start_y][input.start_x] = Some(());
    let mut curr_x = input.start_x;
    let mut curr_y = input.start_y;
    let mut curr_direction = Direction::Up;

    loop {
        match curr_direction {
            Direction::Up => {
                if curr_y == 0 {
                    break;
                } else if input.map[curr_y - 1][curr_x] == Some(()) {
                    curr_direction = Direction::Right;
                    continue;
                }
                curr_y -= 1;
                visited[curr_y][curr_x] = Some(());
            }
            Direction::Right => {
                if curr_x == (width - 1) {
                    break;
                } else if input.map[curr_y][curr_x + 1] == Some(()) {
                    curr_direction = Direction::Down;
                    continue;
                }
                curr_x += 1;
                visited[curr_y][curr_x] = Some(());
            }
            Direction::Down => {
                if curr_y == (height - 1) {
                    break;
                } else if input.map[curr_y + 1][curr_x] == Some(()) {
                    curr_direction = Direction::Left;
                    continue;
                }
                curr_y += 1;
                visited[curr_y][curr_x] = Some(());
            }
            Direction::Left => {
                if curr_x == 0 {
                    break;
                } else if input.map[curr_y][curr_x - 1] == Some(()) {
                    curr_direction = Direction::Up;
                    continue;
                }
                curr_x -= 1;
                visited[curr_y][curr_x] = Some(());
            }
        }
    }

    visited
        .iter()
        .enumerate()
        .flat_map(|(y, v)| {
            v.iter()
                .enumerate()
                .filter_map(move |(x, v)| v.map(|_| (x, y)))
        })
        .collect()
}

fn part2(input: &Input) -> usize {
    let height = input.map.len();
    let width = input.map.first().unwrap().len();

    part1_walk(input)
        .par_iter()
        .filter(|&(x, y)| !(*x == input.start_x && *y == input.start_y))
        .filter(|&(x, y)| {
            // let's try to avoid unncessary allocations here
            // let mut new_map = input.map.clone();
            // map[y][x] = Some(()); // Modify our copy to test
            part2_walk(
                &input.map,
                &input.start_x,
                &input.start_y,
                x,
                y,
                &width,
                &height,
            )
            // map[y][x] = None; // Undo the modification
        })
        .count()
}

fn part2_walk(
    map: &[Vec<Option<()>>],
    start_x: &usize,
    start_y: &usize,
    extra_block_x: &usize,
    extra_block_y: &usize,
    width: &usize,
    height: &usize,
) -> bool {
    let mut curr_x = *start_x;
    let mut curr_y = *start_y;
    let mut curr_direction = Direction::Up;
    let mut visited = HashSet::new();
    visited.insert((curr_x, curr_y, curr_direction.clone()));

    loop {
        match curr_direction {
            Direction::Up => {
                if curr_y == 0 {
                    break;
                }
                let next_y = curr_y - 1;
                if (next_y == *extra_block_y && curr_x == *extra_block_x)
                    || map[curr_y - 1][curr_x] == Some(())
                {
                    curr_direction = Direction::Right;
                    continue;
                }
                curr_y -= 1;
                if !visited.insert((curr_x, curr_y, curr_direction.clone())) {
                    return true;
                }
            }
            Direction::Right => {
                let next_x = curr_x + 1;
                if next_x == *width {
                    break;
                } else if (curr_y == *extra_block_y && next_x == *extra_block_x)
                    || map[curr_y][next_x] == Some(())
                {
                    curr_direction = Direction::Down;
                    continue;
                }
                curr_x += 1;
                if !visited.insert((curr_x, curr_y, curr_direction.clone())) {
                    return true;
                }
            }
            Direction::Down => {
                let next_y = curr_y + 1;
                if next_y == *height {
                    break;
                } else if (next_y == *extra_block_y && curr_x == *extra_block_x)
                    || map[next_y][curr_x] == Some(())
                {
                    curr_direction = Direction::Left;
                    continue;
                }
                curr_y += 1;
                if !visited.insert((curr_x, curr_y, curr_direction.clone())) {
                    return true;
                }
            }
            Direction::Left => {
                if curr_x == 0 {
                    break;
                }
                let next_x = curr_x - 1;
                if (curr_y == *extra_block_y && next_x == *extra_block_x)
                    || map[curr_y][next_x] == Some(())
                {
                    curr_direction = Direction::Up;
                    continue;
                }
                curr_x -= 1;
                if !visited.insert((curr_x, curr_y, curr_direction.clone())) {
                    return true;
                }
            }
        }
    }

    false
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample06.txt"));
        let part1 = part1(&input);
        assert_eq!(41, part1);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample06.txt"));
        let part2 = part2(&input);
        assert_eq!(6, part2);
    }
}
