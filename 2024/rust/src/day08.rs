use itertools::Itertools;

fn main() {
    let input = parse(include_str!("../../files/day08.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<Vec<Option<char>>> {
    input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| match c {
                    '.' => None,
                    _ => Some(c),
                })
                .collect()
        })
        .collect()
}

fn part1(input: &[Vec<Option<char>>]) -> usize {
    let height = input.len();
    let width = input.first().unwrap().len();

    let data = (0..height)
        .flat_map(|y| (0..width).filter_map(move |x| input[y][x].map(|c| (x, y, c))))
        .collect::<Vec<_>>();

    let height = height as i32;
    let width = width as i32;
    data.iter()
        .flat_map(|(x, y, c)| {
            data.iter()
                .filter(|(x2, y2, c2)| c == c2 && !(x == x2 && y == y2))
                .map(|(x2, y2, _)| ((*x as i32, *y as i32), (*x2 as i32, *y2 as i32)))
                .collect::<Vec<_>>()
        })
        .map(|((x1, y1), (x2, y2))| (x2 + (x2 - x1), y2 + (y2 - y1)))
        .filter(|&(x, y)| x >= 0 && x < width && y >= 0 && y < height)
        .unique()
        .count()
}

fn part2(input: &[Vec<Option<char>>]) -> usize {
    let height = input.len();
    let width = input.first().unwrap().len();

    let data = (0..height)
        .flat_map(|y| (0..width).filter_map(move |x| input[y][x].map(|c| (x, y, c))))
        .collect::<Vec<_>>();

    let height = height as i32;
    let width = width as i32;
    data.iter()
        .flat_map(|(x, y, c)| {
            data.iter()
                .filter(|(x2, y2, c2)| c == c2 && !(x == x2 && y == y2))
                .map(|(x2, y2, _)| ((*x as i32, *y as i32), (*x2 as i32, *y2 as i32)))
                .collect::<Vec<_>>()
        })
        .flat_map(|((x1, y1), (x2, y2))| {
            let dx = x2 - x1;
            let dy = y2 - y1;
            let mut antinodes = vec![];
            for f in 0.. {
                let nx = x2 + (f * dx);
                let ny = y2 + (f * dy);
                if !(nx >= 0 && nx < width && ny >= 0 && ny < height) {
                    break;
                }
                antinodes.push((nx, ny));
            }
            antinodes
        })
        .unique()
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample08.txt"));
        let part1 = part1(&input);
        assert_eq!(14, part1);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample08.txt"));
        let part2 = part2(&input);
        assert_eq!(34, part2);
    }
}
