use std::collections::HashMap;

fn main() {
    let input = parse(include_str!("../../files/day01.txt"));

    let part1 = part1(&input.0[..], &input.1[..]);
    let part2 = part2(&input.0[..], &input.1[..]);

    println!("Part 1: {}", part1);
    println!("Part 2: {}", part2);
}

fn parse(input: &str) -> (Vec<i32>, Vec<i32>) {
    input
        .lines()
        .map(|s| s.split("   ").collect::<Vec<_>>())
        .map(|i| (i[0].parse::<i32>().unwrap(), i[1].parse::<i32>().unwrap()))
        .unzip()
}

fn part1(left: &[i32], right: &[i32]) -> i32 {
    let mut left = left.to_vec();
    let mut right = right.to_vec();

    left.sort();
    right.sort();

    left.into_iter()
        .zip(right)
        .map(|i| i32::abs(i.0 - i.1))
        .sum::<i32>()
}

fn part2(left: &[i32], right: &[i32]) -> i32 {
    let mut cache = HashMap::new();
    let mut sum = 0;

    for l in left {
        if let Some(v) = cache.get(l) {
            sum += v;
        } else {
            let v = right.iter().filter(|rl| l == *rl).sum::<i32>();
            cache.insert(l, v);
            sum += v;
        }
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let sample = include_str!("../../files/sample01.txt");
        let input = parse(sample);

        let part1 = part1(&input.0[..], &input.1[..]);
        let part2 = part2(&input.0[..], &input.1[..]);

        assert_eq!(part1, 11);
        assert_eq!(part2, 31);
    }

    #[test]
    fn sample_part2() {
        let sample = include_str!("../../files/sample01.txt");
        let input = parse(sample);

        let part2 = part2(&input.0[..], &input.1[..]);

        assert_eq!(part2, 31);
    }
}
