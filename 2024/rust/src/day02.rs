fn main() {
    let input = parse(include_str!("../../files/day02.txt"));

    let part1 = part1(&input);
    println!("Part 1: {part1}");

    let part2 = part2(&input);
    println!("Part 2: {part2}");
}

fn parse(input: &str) -> Vec<Vec<i32>> {
    input
        .lines()
        .map(|l| {
            l.split_whitespace()
                .map(|i| str::parse::<i32>(i).unwrap())
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
}

fn part1(input: &[Vec<i32>]) -> usize {
    input.iter().filter(|r| validate_report(r)).count()
}

fn part2(input: &[Vec<i32>]) -> usize {
    input
        .iter()
        .map(|r| {
            (0..r.len() + 1)
                .rev()
                .map(|s| {
                    r.iter()
                        .enumerate()
                        .filter(|&(idx, _)| idx != s)
                        .map(|(_, item)| *item)
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>()
        })
        .filter(|r| r.iter().any(|r| validate_report(r)))
        .count()
}

fn validate_report(report: &[i32]) -> bool {
    let diffs = report
        .iter()
        .take(report.len() - 1)
        .zip(report.iter().skip(1))
        .map(|(p, n)| n - p)
        .collect::<Vec<_>>();
    !diffs.iter().any(|d| *d == 0 || d.abs() > 3)
        && (diffs.iter().all(|d| *d < 0) || diffs.iter().all(|d| *d > 0))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample02.txt"));

        let result = part1(&input);

        assert_eq!(
            result, 2,
            "Sample Part 1 Result is {} expected {}",
            result, 2
        );
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample02.txt"));

        let result = part2(&input);

        assert_eq!(
            result, 4,
            "Sample Part 2 Result is {} expected {}",
            result, 4
        );
    }
}
