use std::collections::HashSet;

use itertools::Itertools;
use rayon::prelude::*;

fn main() {
    let input = parse(include_str!("../../files/day16.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Coordinates {
    x: usize,
    y: usize,
}

struct Maze {
    maze: Vec<Vec<Option<()>>>,
    start: Coordinates,
    end: Coordinates,
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
enum Direction {
    West,
    North,
    East,
    South,
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
enum Movement {
    Turn(Direction),
    Move(Coordinates),
}

fn parse(input: &str) -> Maze {
    let start = input
        .lines()
        .enumerate()
        .filter_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter_map(|(x, c)| {
                    if c == 'S' {
                        Some(Coordinates { x, y })
                    } else {
                        None
                    }
                })
                .next()
        })
        .next()
        .unwrap();
    let end = input
        .lines()
        .enumerate()
        .filter_map(|(y, line)| {
            line.chars()
                .enumerate()
                .filter_map(|(x, c)| {
                    if c == 'E' {
                        Some(Coordinates { x, y })
                    } else {
                        None
                    }
                })
                .next()
        })
        .next()
        .unwrap();
    Maze {
        maze: input
            .lines()
            .map(|line| {
                line.chars()
                    .map(|c| match c {
                        '#' => Some(()),
                        _ => None,
                    })
                    .collect_vec()
            })
            .collect_vec(),
        start,
        end,
    }
}

fn score_path(path: &[Movement]) -> u32 {
    path.iter()
        .map(|mv| match mv {
            Movement::Turn(_) => 1000,
            Movement::Move(_) => 1,
        })
        .sum()
}

fn walk(maze: &Maze) -> u32 {
    let pf_score_path = |(path, _, last_pos): &(Vec<Movement>, Direction, Coordinates)| {
        let score = score_path(path);
        score
            + i32::abs_diff(maze.end.x as i32, last_pos.x as i32)
            + i32::abs_diff(maze.end.y as i32, last_pos.y as i32)
            + if maze.end.x != last_pos.x && maze.end.y != last_pos.y {
                1000
            } else {
                0
            }
    };

    let mut path = HashSet::new();
    path.insert(maze.start);
    let mut pathes = vec![(path, 0, Direction::East, maze.start)];

    let mut loops = 0;
    loop {
        loops += 1;
        println!("Loop {loops}");

        let best_path = pathes
            .iter()
            .enumerate()
            .sorted_by_key(|&(_, (_, score, _, _))| score)
            .map(|(idx, _)| idx)
            .next();

        if best_path.is_none() {
            return 0;
        }

        let (travelled, curr_score, curr_dir, curr_pos) = pathes.remove(best_path.unwrap());

        println!("Best Path: ({}, {}) - {curr_score}", curr_pos.x, curr_pos.y);

        // println!("Path: {:?}", best_path);

        if curr_pos == maze.end {
            return curr_score;
        }

        let next = vec![
            (
                Direction::West,
                Coordinates {
                    x: curr_pos.x - 1,
                    y: curr_pos.y,
                },
            ),
            (
                Direction::North,
                Coordinates {
                    x: curr_pos.x,
                    y: curr_pos.y - 1,
                },
            ),
            (
                Direction::East,
                Coordinates {
                    x: curr_pos.x + 1,
                    y: curr_pos.y,
                },
            ),
            (
                Direction::South,
                Coordinates {
                    x: curr_pos.x,
                    y: curr_pos.y + 1,
                },
            ),
        ]
        .into_iter()
        .filter(|(_, c)| maze.maze[c.y][c.x].is_none())
        .filter(|(_, c)| !travelled.contains(c))
        .collect_vec();

        if next.is_empty() {
            // deadend
            continue;
        }

        for (next_dir, next_pos) in next {
            let mut travelled = travelled.clone();
            travelled.insert(next_pos);
            pathes.push((
                travelled,
                curr_score + 1 + (if curr_dir == next_dir { 0 } else { 1000 }),
                next_dir,
                next_pos,
            ));
        }
    }
}

fn part1(input: &Maze) -> u32 {
    walk(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample16.txt"));

        let result = part1(&input);
        // assert_eq!(0, result);
        assert_eq!(7036, result);
    }

    #[test]
    fn sample_part2() {}
}
