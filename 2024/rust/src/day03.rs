use regex::Regex;

fn main() {
    let input = include_str!("../../files/day03.txt");

    let part1 = part1(input);
    println!("Part 1: {part1}");

    let part2 = part2(input);
    println!("Part 2: {part2}");
}

fn part1(input: &str) -> i32 {
    let pat = Regex::new(r"mul\((\d{1,3}),(\d{1,3})\)").unwrap();
    pat.captures_iter(input)
        .map(|m| m.extract())
        .map(|(_, [m1, m2])| {
            (
                str::parse::<i32>(m1).unwrap(),
                str::parse::<i32>(m2).unwrap(),
            )
        })
        .map(|(m1, m2)| m1 * m2)
        .sum()
}

fn part2(input: &str) -> i32 {
    let pat = Regex::new(r"(?:mul\((\d{1,3}),(\d{1,3})\))|(?:do\(\))|(?:don't\(\))").unwrap();
    pat.captures_iter(input)
        .fold((true, 0), |(enabled, sum), m| {
            match m.get(0).map(|m| m.as_str()) {
                Some("do()") => (true, sum),
                Some("don't()") => (false, sum),
                Some(_) => {
                    if enabled {
                        (
                            enabled,
                            sum + (str::parse::<i32>(m.get(1).unwrap().as_str()).unwrap()
                                * str::parse::<i32>(m.get(2).unwrap().as_str()).unwrap()),
                        )
                    } else {
                        (enabled, sum)
                    }
                }
                None => panic!("Invalid match!"),
            }
        })
        .1
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = include_str!("../../files/sample03_part1.txt");
        let result = part1(input);
        assert_eq!(result, 161)
    }

    #[test]
    fn sample_part2() {
        let input = include_str!("../../files/sample03_part2.txt");
        let result = part2(input);
        assert_eq!(result, 48)
    }
}
