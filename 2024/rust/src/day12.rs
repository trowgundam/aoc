use std::collections::HashSet;

use rayon::prelude::*;

fn main() {
    let input = parse(include_str!("../../files/day12.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<Vec<char>> {
    input.lines().map(|l| l.chars().collect()).collect()
}

fn walk(
    input: &[Vec<char>],
    item: char,
    x: usize,
    y: usize,
    width: usize,
    height: usize,
    walked: &mut HashSet<(usize, usize)>,
) {
    let this_item = input[y][x];
    if this_item != item {
        return;
    }
    if !walked.insert((x, y)) {
        return;
    }

    let mut next = vec![];
    if x > 0 {
        next.push((x - 1, y));
    }
    if y > 0 {
        next.push((x, y - 1));
    }
    if x + 1 < width {
        next.push((x + 1, y));
    }
    if y + 1 < height {
        next.push((x, y + 1));
    }
    next.into_iter()
        .for_each(|(x, y)| walk(input, item, x, y, width, height, walked));
}

fn part1(input: &[Vec<char>]) -> usize {
    let height = input.len();
    let width = input.first().unwrap().len();

    let mut visited = HashSet::new();
    let mut plots = vec![];
    for y in 0..height {
        for x in 0..width {
            if visited.contains(&(x, y)) {
                continue;
            }

            let mut stuff = HashSet::new();
            walk(input, input[y][x], x, y, width, height, &mut stuff);

            for c in stuff.iter() {
                visited.insert(*c);
            }

            plots.push((input[y][x], stuff));
        }
    }

    plots
        .into_par_iter()
        .map(|(crop, plot)| {
            let area = plot.len();

            let fences = plot
                .into_iter()
                .map(|(x, y)| {
                    let mut edge: usize = 0;
                    if y == 0 || input[y - 1][x] != crop {
                        edge += 1;
                    }
                    if x == 0 || input[y][x - 1] != crop {
                        edge += 1;
                    }
                    if y + 1 == height || input[y + 1][x] != crop {
                        edge += 1;
                    }
                    if x + 1 == width || input[y][x + 1] != crop {
                        edge += 1;
                    }
                    edge
                })
                .sum::<usize>();
            area * fences
        })
        .sum::<usize>()
}

fn part2(input: &[Vec<char>]) -> usize {
    let height = input.len();
    let width = input.first().unwrap().len();

    let mut visited = HashSet::new();
    let mut plots = vec![];
    for y in 0..height {
        for x in 0..width {
            if visited.contains(&(x, y)) {
                continue;
            }

            let mut stuff = HashSet::new();
            walk(input, input[y][x], x, y, width, height, &mut stuff);

            for c in stuff.iter() {
                visited.insert(*c);
            }

            plots.push((input[y][x], stuff));
        }
    }

    plots
        .into_par_iter()
        .map(|(crop, plot)| {
            let area = plot.len();

            // ((x, y), (Left, Top, Right, Bottom))
            let plot = plot
                .into_par_iter()
                .map(|(x, y)| {
                    (
                        (x, y),
                        (
                            x == 0 || input[y][x - 1] != crop,
                            y == 0 || input[y - 1][x] != crop,
                            x == width - 1 || input[y][x + 1] != crop,
                            y == height - 1 || input[y + 1][x] != crop,
                        ),
                    )
                })
                .collect::<Vec<_>>();
            // (x, y, d)
            // Values for d:
            // 0 = Left
            // 1 = Top
            // 2 = Right
            // 3 = Bottom
            let mut visited = HashSet::new();

            let mut fences: usize = 0;
            for &((x, y), (left, top, right, bottom)) in plot.iter() {
                // Left
                if visited.insert(((x, y), 0)) && left {
                    let edges = (0..y)
                        .rev()
                        .take_while(|y2| {
                            plot.iter()
                                .any(|((x3, y3), (left, _, _, _))| *x3 == x && y3 == y2 && *left)
                        })
                        .chain((y + 1..height).take_while(|y2| {
                            plot.iter()
                                .any(|((x3, y3), (left, _, _, _))| *x3 == x && y3 == y2 && *left)
                        }))
                        .collect::<Vec<_>>();

                    for y2 in edges {
                        visited.insert(((x, y2), 0));
                    }
                    fences += 1;
                }
                // Right
                if visited.insert(((x, y), 2)) && right {
                    let edges = (0..y)
                        .rev()
                        .take_while(|y2| {
                            plot.iter()
                                .any(|((x3, y3), (_, _, right, _))| *x3 == x && y3 == y2 && *right)
                        })
                        .chain((y + 1..height).take_while(|y2| {
                            plot.iter()
                                .any(|((x3, y3), (_, _, right, _))| *x3 == x && y3 == y2 && *right)
                        }))
                        .collect::<Vec<_>>();

                    for y2 in edges {
                        visited.insert(((x, y2), 2));
                    }
                    fences += 1;
                }
                // Top
                if visited.insert(((x, y), 1)) && top {
                    let edges = (0..x)
                        .rev()
                        .take_while(|x2| {
                            plot.iter()
                                .any(|((x3, y3), (_, top, _, _))| x3 == x2 && *y3 == y && *top)
                        })
                        .chain((x + 1..height).take_while(|x2| {
                            plot.iter()
                                .any(|((x3, y3), (_, top, _, _))| x3 == x2 && *y3 == y && *top)
                        }))
                        .collect::<Vec<_>>();

                    for x2 in edges {
                        visited.insert(((x2, y), 1));
                    }
                    fences += 1;
                }
                // Bottom
                if visited.insert(((x, y), 3)) && bottom {
                    let edges = (0..x)
                        .rev()
                        .take_while(|x2| {
                            plot.iter().any(|((x3, y3), (_, _, _, bottom))| {
                                x3 == x2 && *y3 == y && *bottom
                            })
                        })
                        .chain((x + 1..height).take_while(|x2| {
                            plot.iter().any(|((x3, y3), (_, _, _, bottom))| {
                                x3 == x2 && *y3 == y && *bottom
                            })
                        }))
                        .collect::<Vec<_>>();

                    for x2 in edges {
                        visited.insert(((x2, y), 3));
                    }
                    fences += 1;
                }
            }
            area * fences
        })
        .sum::<usize>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample12.txt"));
        let result = part1(&input);
        assert_eq!(1930, result);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample12.txt"));
        let result = part2(&input);
        assert_eq!(1206, result);
    }
}
