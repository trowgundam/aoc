use std::collections::HashMap;

fn main() {
    let input = parse(include_str!("../../files/day11.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<u64> {
    input
        .split_whitespace()
        .map(|i| i.parse::<u64>().unwrap())
        .collect()
}

fn rules(v: &u64) -> Vec<u64> {
    if *v == 0 {
        return vec![1];
    }

    let i = v.to_string();
    if i.len() % 2 == 0 {
        vec![
            i[..i.len() / 2].parse().unwrap(),
            i[i.len() / 2..].parse().unwrap(),
        ]
    } else {
        vec![*v * 2024]
    }
}

fn blink(
    value: u64,
    count: usize,
    result_cache: &mut HashMap<(u64, usize), usize>,
    rule_cache: &mut HashMap<u64, Vec<u64>>,
) -> usize {
    if count == 0 {
        return 1;
    }

    if let Some(r) = result_cache.get(&(value, count)) {
        return *r;
    }

    let items = rule_cache
        .entry(value)
        .or_insert_with(|| rules(&value))
        .clone();
    let result = items
        .into_iter()
        .map(|v| blink(v, count - 1, result_cache, rule_cache))
        .sum();

    result_cache.insert((value, count), result);
    result
}

fn part1(input: &[u64]) -> usize {
    let mut rule_cache = HashMap::new();
    let mut result_cache = HashMap::new();
    input
        .iter()
        .map(|v| blink(*v, 25, &mut result_cache, &mut rule_cache))
        .sum()
}

fn part2(input: &[u64]) -> usize {
    let mut rule_cache = HashMap::new();
    let mut result_cache = HashMap::new();
    input
        .iter()
        .map(|v| blink(*v, 75, &mut result_cache, &mut rule_cache))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse("125 17");

        let result = part1(&input);
        assert_eq!(55312, result);
    }

    #[test]
    fn sample_part2() {
        let input = parse("125 17");

        let result = part2(&input);
        // No actual result was provided, this is calculated
        // by my successful algorithm for the actual input
        assert_eq!(65601038650482, result);
    }
}
