use rayon::prelude::*;

fn main() {
    let input = parse(include_str!("../../files/day07.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Vec<(u64, Vec<u64>)> {
    input
        .lines()
        .map(|l| {
            let items = l.split(": ").collect::<Vec<_>>();
            (
                str::parse::<u64>(items[0]).unwrap(),
                items[1]
                    .split_whitespace()
                    .map(|i| str::parse::<u64>(i).unwrap())
                    .collect::<Vec<_>>(),
            )
        })
        .collect()
}

fn part1(input: &[(u64, Vec<u64>)]) -> u64 {
    calc(input, calc_part1)
}

fn part2(input: &[(u64, Vec<u64>)]) -> u64 {
    calc(input, calc_part2)
}

fn calc(input: &[(u64, Vec<u64>)], calc_fn: fn(u64, u64) -> Vec<u64>) -> u64 {
    input
        .par_iter()
        .filter(|(total, values)| {
            values
                .iter()
                .fold(vec![], |acc, v| {
                    if acc.is_empty() {
                        vec![*v]
                    } else {
                        acc.iter()
                            .flat_map(|i1| calc_fn(*i1, *v))
                            .filter(|v| v <= total)
                            .collect()
                    }
                })
                .contains(total)
        })
        .map(|(v, _)| v)
        .sum::<u64>()
}

fn calc_part1(item1: u64, item2: u64) -> Vec<u64> {
    vec![item1 + item2, item1 * item2]
}

fn calc_part2(item1: u64, item2: u64) -> Vec<u64> {
    vec![
        item1 + item2,
        item1 * item2,
        if item2 == 0 {
            item1 * 10
        } else {
            let num_digits = (item2 as f64).log10().floor() as u32 + 1; // Count digits in b
            item1 * 10u64.pow(num_digits) + item2
        },
    ]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample07.txt"));
        let part1 = part1(&input);
        assert_eq!(3749, part1);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample07.txt"));
        let part2 = part2(&input);
        assert_eq!(11387, part2);
    }
}
