use itertools::Itertools;
use rayon::iter::{IntoParallelIterator, IntoParallelRefIterator, ParallelIterator};
use regex::Regex;

fn main() {
    let input = parse(include_str!("../../files/day13.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2_ai(&input);
    println!("Part2: {part2}");
}

#[derive(Debug)]
struct Coord {
    pub x: usize,
    pub y: usize,
}
#[derive(Debug)]
struct PrizeMachine {
    pub button_a: Coord,
    pub button_b: Coord,
    pub prize: Coord,
}

fn parse(input: &str) -> Vec<PrizeMachine> {
    let btn_a = Regex::new(r"Button A: X\+(\d+), Y\+(\d+)").unwrap();
    let btn_b = Regex::new(r"Button B: X\+(\d+), Y\+(\d+)").unwrap();
    let prize = Regex::new(r"Prize: X=(\d+), Y=(\d+)").unwrap();

    let btn_a = btn_a
        .captures_iter(input)
        .map(|m| m.extract())
        .map(|(_, [x, y])| (x, y));
    let btn_b = btn_b
        .captures_iter(input)
        .map(|m| m.extract())
        .map(|(_, [x, y])| (x, y));
    let prize = prize
        .captures_iter(input)
        .map(|m| m.extract())
        .map(|(_, [x, y])| (x, y));

    btn_a
        .zip(btn_b)
        .zip(prize)
        .map(|(((ax, ay), (bx, by)), (px, py))| PrizeMachine {
            button_a: Coord {
                x: ax.parse().unwrap(),
                y: ay.parse().unwrap(),
            },
            button_b: Coord {
                x: bx.parse().unwrap(),
                y: by.parse().unwrap(),
            },
            prize: Coord {
                x: px.parse().unwrap(),
                y: py.parse().unwrap(),
            },
        })
        .collect_vec()
}

fn part1(input: &[PrizeMachine]) -> usize {
    input
        .par_iter()
        .filter_map(|prize_machine| {
            let presses = (0..100)
                .into_par_iter()
                .flat_map(|a: usize| {
                    let ax = prize_machine.button_a.x * a;
                    let ay = prize_machine.button_a.y * a;
                    (0..100).into_par_iter().map(move |b: usize| {
                        (
                            (a, b),
                            (
                                ax + (b * prize_machine.button_b.x),
                                ay + (b * prize_machine.button_b.y),
                            ),
                        )
                    })
                })
                .filter(|(_, (x, y))| prize_machine.prize.x == *x && prize_machine.prize.y == *y)
                .collect::<Vec<_>>();

            if presses.is_empty() {
                None
            } else {
                presses.into_iter().map(|((a, b), _)| (a * 3) + b).min()
            }
        })
        .sum()
}

fn get_b(a: usize, prize_machine: &PrizeMachine) -> Option<usize> {
    // if (prize_machine.button_a.x * a) > prize_machine.prize.x {
    //     return None;
    // }
    //
    let bx = prize_machine.prize.x - (prize_machine.button_a.x * a);

    if bx % prize_machine.button_b.x != 0 {
        None
    } else {
        let b = bx / prize_machine.button_b.x;
        if prize_machine.prize.y == (prize_machine.button_a.y * a) + (prize_machine.button_b.y * b)
        {
            Some(b)
        } else {
            None
        }
    }
}

// I'm too stupid. My solution will work... eventually. No clue how long it would take, but pretty
// long I'd guess. Even with it parallelized as much as it is. I was able to find an answer using
// linear algebra from ChatGPU, but while I sorta understand how it works. I'm just too shit at
// linear algebra to have ever come up with the solution myself.
#[allow(dead_code)]
fn part2(input: &[PrizeMachine]) -> usize {
    let input = input
        .iter()
        .map(|pm| PrizeMachine {
            button_a: Coord {
                x: pm.button_a.x,
                y: pm.button_a.y,
            },
            button_b: Coord {
                x: pm.button_b.x,
                y: pm.button_b.y,
            },
            prize: Coord {
                x: pm.prize.x + 10000000000000,
                y: pm.prize.y + 10000000000000,
            },
        })
        .collect_vec();

    let sum = input
        .par_iter()
        .filter_map(|prize_machine| {
            let max_ax = (prize_machine.prize.x / prize_machine.button_a.x) + 1;
            let max_ay = (prize_machine.prize.y / prize_machine.button_a.y) + 1;
            let max_a = if max_ax < max_ay { max_ax } else { max_ay };

            (0..max_a)
                .into_par_iter()
                .filter_map(|a| get_b(a, prize_machine).map(|b| (a, b)))
                .map(|(a, b)| (a * 3) + b)
                .min()
        })
        .sum();

    println!("Sum: {sum}");
    sum
}

// This algorithm was provided by ChatGPT, it's using some fancy linear algebra that I am far too
// stupid to ever have thought of myself.
fn solve_system(
    x_a: i128,
    y_a: i128,
    x_b: i128,
    y_b: i128,
    x_d: i128,
    y_d: i128,
) -> Option<(i128, i128)> {
    let d = x_a * y_b - x_b * y_a;
    if d == 0 {
        // Check for consistency
        if x_a * y_d == y_a * x_d {
            // Infinitely many solutions; we need to find non-negative solutions with minimal cost
            // This requires parameterization; however, without additional constraints, it's complex
            // For simplicity, return None here
            None
        } else {
            // No solution
            None
        }
    } else {
        // Calculate a and b
        let a_numerator = x_d * y_b - x_b * y_d;
        let b_numerator = x_a * y_d - y_a * x_d;

        // Check if d divides both numerators
        if a_numerator % d != 0 || b_numerator % d != 0 {
            return None;
        }

        let a = a_numerator / d;
        let b = b_numerator / d;

        if a >= 0 && b >= 0 {
            Some((a, b))
        } else {
            None
        }
    }
}

fn part2_ai(input: &[PrizeMachine]) -> i128 {
    input
        .iter()
        .map(|pm| {
            (
                (pm.button_a.x as i128, pm.button_a.y as i128),
                (pm.button_b.x as i128, pm.button_b.y as i128),
                (
                    pm.prize.x as i128 + 10000000000000,
                    pm.prize.y as i128 + 10000000000000,
                ),
            )
        })
        .filter_map(|((x_a, y_a), (x_b, y_b), (x_d, y_d))| {
            solve_system(x_a, y_a, x_b, y_b, x_d, y_d)
        })
        .map(|(a, b)| (a * 3) + b)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample13.txt"));
        let result = part1(&input);
        assert_eq!(480, result);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample13.txt"));
        let result = part2_ai(&input);
        assert_eq!(875318608908, result);
    }
}
