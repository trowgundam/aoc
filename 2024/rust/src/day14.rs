use itertools::Itertools;
use rayon::prelude::*;

fn main() {
    let input = parse(include_str!("../../files/day14.txt"));

    let part1 = part1(&input, 101, 103, 100);
    println!("Part1: {part1}");

    let part2 = part2(&input, 101, 103);
    println!("Part2: {part2}");
}

struct Vec2<T> {
    pub x: T,
    pub y: T,
}

struct Robot {
    position: Vec2<i32>,
    velocity: Vec2<i32>,
}
fn parse(input: &str) -> Vec<Robot> {
    input
        .lines()
        .filter_map(|line| line.split_once(' '))
        .map(|(pos, vel)| {
            let pos = pos[2..].split_once(',').unwrap();
            let vel = vel[2..].split_once(',').unwrap();
            Robot {
                position: Vec2 {
                    x: pos.0.parse().unwrap(),
                    y: pos.1.parse().unwrap(),
                },
                velocity: Vec2 {
                    x: vel.0.parse().unwrap(),
                    y: vel.1.parse().unwrap(),
                },
            }
        })
        .collect_vec()
}

fn part1(input: &[Robot], width: i32, height: i32, iterations: i32) -> usize {
    input
        .par_iter()
        .map(|robot| {
            (
                robot,
                robot.position.x + (iterations * robot.velocity.x),
                robot.position.y + (iterations * robot.velocity.y),
            )
        })
        .map(|(robot, x, y)| {
            (
                robot,
                if x >= width {
                    x % width
                } else if x < 0 {
                    ((x % width) + width) % width
                } else {
                    x
                },
                if y >= height {
                    y % height
                } else if y <= 0 {
                    ((y % height) + height) % height
                } else {
                    y
                },
            )
        })
        .collect::<Vec<_>>()
        .into_iter()
        .fold(vec![0, 0, 0, 0], |acc, (_, x, y)| {
            let mx = (width - 1) / 2;
            let my = (height - 1) / 2;
            if x < mx && y < my {
                vec![acc[0] + 1, acc[1], acc[2], acc[3]]
            } else if x > mx && y < my {
                vec![acc[0], acc[1] + 1, acc[2], acc[3]]
            } else if x < mx && y > my {
                vec![acc[0], acc[1], acc[2] + 1, acc[3]]
            } else if x > mx && y > my {
                vec![acc[0], acc[1], acc[2], acc[3] + 1]
            } else {
                acc
            }
        })
        .into_iter()
        .product()
}

fn part2(input: &[Robot], width: i32, height: i32) -> usize {
    (0..(101 * 104))
        .into_par_iter()
        .map(|iteration| (iteration, part1(input, width, height, iteration)))
        .min_by(|(i1, s1), (i2, s2)| match s1.cmp(s2) {
            std::cmp::Ordering::Equal => i1.cmp(i2),
            o => o,
        })
        .unwrap()
        .0 as usize
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample14.txt"));
        let result = part1(&input, 11, 7, 100);
        assert_eq!(12, result);
    }

    #[test]
    fn sample_part2() {}
}
