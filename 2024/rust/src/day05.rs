fn main() {
    let input = parse(include_str!("../../files/day05.txt"));

    let part1 = part1(&input);
    println!("Part1: {part1}");

    let part2 = part2(&input);
    println!("Part2: {part2}");
}

fn parse(input: &str) -> Data {
    let rules = input
        .lines()
        .take_while(|l| !l.is_empty())
        .map(|l| l.split('|').collect::<Vec<_>>())
        .map(|items| Rule {
            left: str::parse::<i32>(items[0]).unwrap(),
            right: str::parse::<i32>(items[1]).unwrap(),
        })
        .collect::<Vec<_>>();
    let updates: Vec<Vec<i32>> = input
        .lines()
        .skip_while(|l| !l.is_empty())
        .skip(1)
        .map(|l| l.split(','))
        .map(|l| l.map(|i| str::parse::<i32>(i).unwrap()).collect())
        .collect();
    Data { rules, updates }
}

struct Data {
    pub rules: Vec<Rule>,
    pub updates: Vec<Vec<i32>>,
}

struct Rule {
    pub left: i32,
    pub right: i32,
}

fn part1(input: &Data) -> i32 {
    input
        .updates
        .iter()
        .map(|up| {
            (
                up,
                input
                    .rules
                    .iter()
                    .filter(|rule| up.contains(&rule.left) && up.contains(&rule.right))
                    .collect::<Vec<_>>(),
            )
        })
        .filter(|(up, rules)| {
            rules.iter().all(|&r| {
                up.iter().position(|&i| i == r.left).unwrap()
                    < up.iter().position(|&i| i == r.right).unwrap()
            })
        })
        .map(|(up, _)| up[up.len() / 2])
        .sum::<i32>()
}

fn part2(input: &Data) -> i32 {
    input
        .updates
        .iter()
        .map(|up| {
            (
                up,
                input
                    .rules
                    .iter()
                    .filter(|rule| up.contains(&rule.left) && up.contains(&rule.right))
                    .collect::<Vec<_>>(),
            )
        })
        .filter(|(up, rules)| {
            !rules.iter().all(|&r| {
                up.iter().position(|&i| i == r.left).unwrap()
                    < up.iter().position(|&i| i == r.right).unwrap()
            })
        })
        .map(|(up, rules)| {
            let mut mine = up.clone();
            mine.sort_by(|i1, i2| {
                let rule = rules.iter().find(|r| {
                    (r.left == *i1 && r.right == *i2) || (r.left == *i2 && r.right == *i1)
                });
                if let Some(rule) = rule {
                    if rule.left == *i1 {
                        std::cmp::Ordering::Less
                    } else {
                        std::cmp::Ordering::Greater
                    }
                } else if rules.iter().any(|r| r.left == *i1 || r.right == *i1) {
                    std::cmp::Ordering::Less
                } else {
                    std::cmp::Ordering::Greater
                }
            });
            mine[mine.len() / 2]
        })
        .sum::<i32>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_part1() {
        let input = parse(include_str!("../../files/sample05.txt"));
        let part1 = part1(&input);
        assert_eq!(part1, 143);
    }

    #[test]
    fn sample_part2() {
        let input = parse(include_str!("../../files/sample05.txt"));
        let part2 = part2(&input);
        assert_eq!(part2, 123);
    }
}
