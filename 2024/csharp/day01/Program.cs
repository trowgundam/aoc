﻿if (!args.Any())
{
    await Console.Error.WriteLineAsync("Must provide input file");
    return -1;
}

var (left, right) = Parse(File.ReadAllLines(args.First()));

var part1 = Part1(left, right);
var part2 = Part2(left, right);

Console.WriteLine($"Part 1: {part1}");
Console.WriteLine($"Part 2: {part2}");

return 0;

int Part1(int[] left, int[] right) =>
    left.Order().Zip(right.Order()).Select(i => Math.Abs(i.First - i.Second)).Sum();

int Part2(int[] left, int[] right)
{
    Dictionary<int, int> cache = new();
    int sum = 0;
    foreach (var id in left)
    {
        if (cache.TryGetValue(id, out var val))
        {
            sum += val;
            continue;
        }

        val = id * right.Count(v => v == id);
        cache[id] = val;
        sum += val;
    }

    return sum;
}

(int[] Left, int[] Right) Parse(IEnumerable<string> input)
{
    var parsed = input.Select(ParseLine) ?? throw new InvalidOperationException();
    return (parsed.Select(i => i.Left).ToArray(), parsed.Select(i => i.Right).ToArray());
}

(int Left, int Right) ParseLine(string line)
{
    var items = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
    return (int.Parse(items.First()), int.Parse(items.Last()));
}
